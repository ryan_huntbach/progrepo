Each folder contains a detailed readme file.    

**Lab_03**    
Employee management program written for CS3260 (C# Software Engineering) during Spring of 2016.  
Code complies with the style guide for the course, which differs from the C# language standard.

**Treasure Hunter**    
Retro game written for CS 3680 (Mobile Development) during Spring of 2016.