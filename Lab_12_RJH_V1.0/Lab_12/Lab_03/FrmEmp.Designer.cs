﻿namespace Lab_03
{
    partial class FrmEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GBxBasicEmpInfo = new System.Windows.Forms.GroupBox();
            this.TxtDepartment = new System.Windows.Forms.TextBox();
            this.DTpStartDate = new System.Windows.Forms.DateTimePicker();
            this.LblStartDate = new System.Windows.Forms.Label();
            this.TxtTitle = new System.Windows.Forms.TextBox();
            this.LblTitle = new System.Windows.Forms.Label();
            this.LblDepartment = new System.Windows.Forms.Label();
            this.CBxMaritalStatus = new System.Windows.Forms.ComboBox();
            this.LblMaritalStatus = new System.Windows.Forms.Label();
            this.TxtMiddleInitial = new System.Windows.Forms.TextBox();
            this.LblMiddleInitial = new System.Windows.Forms.Label();
            this.TxtEmpID = new System.Windows.Forms.TextBox();
            this.TxtFirstName = new System.Windows.Forms.TextBox();
            this.TxtLastName = new System.Windows.Forms.TextBox();
            this.LblLastName = new System.Windows.Forms.Label();
            this.LblFirstName = new System.Windows.Forms.Label();
            this.LblEmpID = new System.Windows.Forms.Label();
            this.GBxCompensation = new System.Windows.Forms.GroupBox();
            this.TxtAgency = new System.Windows.Forms.TextBox();
            this.LblAgency = new System.Windows.Forms.Label();
            this.LblAcceptedCharacters = new System.Windows.Forms.Label();
            this.TxtContractWage = new System.Windows.Forms.TextBox();
            this.LblContractWage = new System.Windows.Forms.Label();
            this.TxtHourlyRate = new System.Windows.Forms.TextBox();
            this.LblHourlyRate = new System.Windows.Forms.Label();
            this.TxtMonthlySalary = new System.Windows.Forms.TextBox();
            this.LblMonthlySalary = new System.Windows.Forms.Label();
            this.GBxEmpType = new System.Windows.Forms.GroupBox();
            this.RBtnContractEmp = new System.Windows.Forms.RadioButton();
            this.RBtnHourlyEmp = new System.Windows.Forms.RadioButton();
            this.RBtnSalesEmp = new System.Windows.Forms.RadioButton();
            this.RBtnSalaryEmp = new System.Windows.Forms.RadioButton();
            this.BtnStoreEmpData = new System.Windows.Forms.Button();
            this.LblSuccessOrFailureMsg = new System.Windows.Forms.Label();
            this.GBxEmpFileOps = new System.Windows.Forms.GroupBox();
            this.BtnSaveAndClose = new System.Windows.Forms.Button();
            this.BtnLoadFromFile = new System.Windows.Forms.Button();
            this.TabsEmpForm = new System.Windows.Forms.TabControl();
            this.TabAddEmp = new System.Windows.Forms.TabPage();
            this.GBxFullPartTime = new System.Windows.Forms.GroupBox();
            this.GBxLookup = new System.Windows.Forms.GroupBox();
            this.BtnLookupEmployee = new System.Windows.Forms.Button();
            this.TabCourseInfo = new System.Windows.Forms.TabPage();
            this.GBxLookupCourses = new System.Windows.Forms.GroupBox();
            this.BtnLookupCourses = new System.Windows.Forms.Button();
            this.TxtLookupCoursesEmpId = new System.Windows.Forms.TextBox();
            this.LblLookupCourses = new System.Windows.Forms.Label();
            this.LBxLookupCourses = new System.Windows.Forms.ListBox();
            this.GBxAddCourse = new System.Windows.Forms.GroupBox();
            this.LblAddCrsEmpId = new System.Windows.Forms.Label();
            this.TxtAddCrsEmpId = new System.Windows.Forms.TextBox();
            this.CBxCredits = new System.Windows.Forms.ComboBox();
            this.CbxCourseApproved = new System.Windows.Forms.ComboBox();
            this.LblCourseApproved = new System.Windows.Forms.Label();
            this.DTpAppDate = new System.Windows.Forms.DateTimePicker();
            this.CBxCourseGrade = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnAddCourse = new System.Windows.Forms.Button();
            this.LblCourseCredits = new System.Windows.Forms.Label();
            this.LblApprovedDate = new System.Windows.Forms.Label();
            this.LblCourseGrade = new System.Windows.Forms.Label();
            this.LblCourseDesc = new System.Windows.Forms.Label();
            this.TxtCourseDesc = new System.Windows.Forms.TextBox();
            this.LblCourseID = new System.Windows.Forms.Label();
            this.TxtCourseID = new System.Windows.Forms.TextBox();
            this.BtnLookupEmployeeInCrs = new System.Windows.Forms.Button();
            this.ChBxFullTime = new System.Windows.Forms.CheckBox();
            this.ChBxPartTime = new System.Windows.Forms.CheckBox();
            this.LblCrsAddSuccessFailMsg = new System.Windows.Forms.Label();
            this.GBxBasicEmpInfo.SuspendLayout();
            this.GBxCompensation.SuspendLayout();
            this.GBxEmpType.SuspendLayout();
            this.GBxEmpFileOps.SuspendLayout();
            this.TabsEmpForm.SuspendLayout();
            this.TabAddEmp.SuspendLayout();
            this.GBxFullPartTime.SuspendLayout();
            this.GBxLookup.SuspendLayout();
            this.TabCourseInfo.SuspendLayout();
            this.GBxLookupCourses.SuspendLayout();
            this.GBxAddCourse.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBxBasicEmpInfo
            // 
            this.GBxBasicEmpInfo.Controls.Add(this.TxtDepartment);
            this.GBxBasicEmpInfo.Controls.Add(this.DTpStartDate);
            this.GBxBasicEmpInfo.Controls.Add(this.LblStartDate);
            this.GBxBasicEmpInfo.Controls.Add(this.TxtTitle);
            this.GBxBasicEmpInfo.Controls.Add(this.LblTitle);
            this.GBxBasicEmpInfo.Controls.Add(this.LblDepartment);
            this.GBxBasicEmpInfo.Controls.Add(this.CBxMaritalStatus);
            this.GBxBasicEmpInfo.Controls.Add(this.LblMaritalStatus);
            this.GBxBasicEmpInfo.Controls.Add(this.TxtMiddleInitial);
            this.GBxBasicEmpInfo.Controls.Add(this.LblMiddleInitial);
            this.GBxBasicEmpInfo.Controls.Add(this.TxtEmpID);
            this.GBxBasicEmpInfo.Controls.Add(this.TxtFirstName);
            this.GBxBasicEmpInfo.Controls.Add(this.TxtLastName);
            this.GBxBasicEmpInfo.Controls.Add(this.LblLastName);
            this.GBxBasicEmpInfo.Controls.Add(this.LblFirstName);
            this.GBxBasicEmpInfo.Controls.Add(this.LblEmpID);
            this.GBxBasicEmpInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxBasicEmpInfo.Location = new System.Drawing.Point(18, 144);
            this.GBxBasicEmpInfo.Name = "GBxBasicEmpInfo";
            this.GBxBasicEmpInfo.Size = new System.Drawing.Size(382, 264);
            this.GBxBasicEmpInfo.TabIndex = 11;
            this.GBxBasicEmpInfo.TabStop = false;
            this.GBxBasicEmpInfo.Text = "Basic Info";
            // 
            // TxtDepartment
            // 
            this.TxtDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDepartment.Location = new System.Drawing.Point(128, 166);
            this.TxtDepartment.Name = "TxtDepartment";
            this.TxtDepartment.Size = new System.Drawing.Size(240, 22);
            this.TxtDepartment.TabIndex = 6;
            // 
            // DTpStartDate
            // 
            this.DTpStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTpStartDate.Location = new System.Drawing.Point(128, 224);
            this.DTpStartDate.Name = "DTpStartDate";
            this.DTpStartDate.Size = new System.Drawing.Size(240, 22);
            this.DTpStartDate.TabIndex = 8;
            this.DTpStartDate.Value = new System.DateTime(2016, 3, 24, 0, 0, 0, 0);
            // 
            // LblStartDate
            // 
            this.LblStartDate.AutoSize = true;
            this.LblStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblStartDate.Location = new System.Drawing.Point(6, 229);
            this.LblStartDate.Name = "LblStartDate";
            this.LblStartDate.Size = new System.Drawing.Size(67, 16);
            this.LblStartDate.TabIndex = 0;
            this.LblStartDate.Text = "Start Date";
            // 
            // TxtTitle
            // 
            this.TxtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTitle.Location = new System.Drawing.Point(128, 196);
            this.TxtTitle.Name = "TxtTitle";
            this.TxtTitle.Size = new System.Drawing.Size(240, 22);
            this.TxtTitle.TabIndex = 7;
            // 
            // LblTitle
            // 
            this.LblTitle.AutoSize = true;
            this.LblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTitle.Location = new System.Drawing.Point(6, 199);
            this.LblTitle.Name = "LblTitle";
            this.LblTitle.Size = new System.Drawing.Size(34, 16);
            this.LblTitle.TabIndex = 0;
            this.LblTitle.Text = "Title";
            // 
            // LblDepartment
            // 
            this.LblDepartment.AutoSize = true;
            this.LblDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDepartment.Location = new System.Drawing.Point(6, 169);
            this.LblDepartment.Name = "LblDepartment";
            this.LblDepartment.Size = new System.Drawing.Size(78, 16);
            this.LblDepartment.TabIndex = 0;
            this.LblDepartment.Text = "Department";
            // 
            // CBxMaritalStatus
            // 
            this.CBxMaritalStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBxMaritalStatus.FormattingEnabled = true;
            this.CBxMaritalStatus.Items.AddRange(new object[] {
            "Single",
            "Married"});
            this.CBxMaritalStatus.Location = new System.Drawing.Point(128, 136);
            this.CBxMaritalStatus.Name = "CBxMaritalStatus";
            this.CBxMaritalStatus.Size = new System.Drawing.Size(240, 24);
            this.CBxMaritalStatus.TabIndex = 5;
            // 
            // LblMaritalStatus
            // 
            this.LblMaritalStatus.AutoSize = true;
            this.LblMaritalStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMaritalStatus.Location = new System.Drawing.Point(6, 139);
            this.LblMaritalStatus.Name = "LblMaritalStatus";
            this.LblMaritalStatus.Size = new System.Drawing.Size(88, 16);
            this.LblMaritalStatus.TabIndex = 0;
            this.LblMaritalStatus.Text = "Marital Status";
            // 
            // TxtMiddleInitial
            // 
            this.TxtMiddleInitial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMiddleInitial.Location = new System.Drawing.Point(128, 108);
            this.TxtMiddleInitial.Name = "TxtMiddleInitial";
            this.TxtMiddleInitial.Size = new System.Drawing.Size(240, 22);
            this.TxtMiddleInitial.TabIndex = 4;
            // 
            // LblMiddleInitial
            // 
            this.LblMiddleInitial.AutoSize = true;
            this.LblMiddleInitial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMiddleInitial.Location = new System.Drawing.Point(6, 111);
            this.LblMiddleInitial.Name = "LblMiddleInitial";
            this.LblMiddleInitial.Size = new System.Drawing.Size(82, 16);
            this.LblMiddleInitial.TabIndex = 0;
            this.LblMiddleInitial.Text = "Middle Initial";
            // 
            // TxtEmpID
            // 
            this.TxtEmpID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpID.Location = new System.Drawing.Point(128, 24);
            this.TxtEmpID.Name = "TxtEmpID";
            this.TxtEmpID.Size = new System.Drawing.Size(240, 22);
            this.TxtEmpID.TabIndex = 1;
            // 
            // TxtFirstName
            // 
            this.TxtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFirstName.Location = new System.Drawing.Point(128, 52);
            this.TxtFirstName.Name = "TxtFirstName";
            this.TxtFirstName.Size = new System.Drawing.Size(240, 22);
            this.TxtFirstName.TabIndex = 2;
            // 
            // TxtLastName
            // 
            this.TxtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLastName.Location = new System.Drawing.Point(128, 80);
            this.TxtLastName.Name = "TxtLastName";
            this.TxtLastName.Size = new System.Drawing.Size(240, 22);
            this.TxtLastName.TabIndex = 3;
            // 
            // LblLastName
            // 
            this.LblLastName.AutoSize = true;
            this.LblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLastName.Location = new System.Drawing.Point(6, 83);
            this.LblLastName.Name = "LblLastName";
            this.LblLastName.Size = new System.Drawing.Size(73, 16);
            this.LblLastName.TabIndex = 0;
            this.LblLastName.Text = "Last Name";
            // 
            // LblFirstName
            // 
            this.LblFirstName.AutoSize = true;
            this.LblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFirstName.Location = new System.Drawing.Point(6, 55);
            this.LblFirstName.Name = "LblFirstName";
            this.LblFirstName.Size = new System.Drawing.Size(70, 16);
            this.LblFirstName.TabIndex = 0;
            this.LblFirstName.Text = "FirstName";
            // 
            // LblEmpID
            // 
            this.LblEmpID.AutoSize = true;
            this.LblEmpID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEmpID.Location = new System.Drawing.Point(6, 27);
            this.LblEmpID.Name = "LblEmpID";
            this.LblEmpID.Size = new System.Drawing.Size(86, 16);
            this.LblEmpID.TabIndex = 0;
            this.LblEmpID.Text = "Employee ID";
            // 
            // GBxCompensation
            // 
            this.GBxCompensation.Controls.Add(this.TxtAgency);
            this.GBxCompensation.Controls.Add(this.LblAgency);
            this.GBxCompensation.Controls.Add(this.LblAcceptedCharacters);
            this.GBxCompensation.Controls.Add(this.TxtContractWage);
            this.GBxCompensation.Controls.Add(this.LblContractWage);
            this.GBxCompensation.Controls.Add(this.TxtHourlyRate);
            this.GBxCompensation.Controls.Add(this.LblHourlyRate);
            this.GBxCompensation.Controls.Add(this.TxtMonthlySalary);
            this.GBxCompensation.Controls.Add(this.LblMonthlySalary);
            this.GBxCompensation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxCompensation.Location = new System.Drawing.Point(430, 8);
            this.GBxCompensation.Name = "GBxCompensation";
            this.GBxCompensation.Size = new System.Drawing.Size(399, 182);
            this.GBxCompensation.TabIndex = 12;
            this.GBxCompensation.TabStop = false;
            this.GBxCompensation.Text = "Compensation";
            // 
            // TxtAgency
            // 
            this.TxtAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAgency.Location = new System.Drawing.Point(165, 142);
            this.TxtAgency.Name = "TxtAgency";
            this.TxtAgency.Size = new System.Drawing.Size(212, 22);
            this.TxtAgency.TabIndex = 12;
            this.TxtAgency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LblAgency
            // 
            this.LblAgency.AutoSize = true;
            this.LblAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAgency.Location = new System.Drawing.Point(15, 144);
            this.LblAgency.Name = "LblAgency";
            this.LblAgency.Size = new System.Drawing.Size(144, 16);
            this.LblAgency.TabIndex = 0;
            this.LblAgency.Text = "Agency (Contract Only)";
            // 
            // LblAcceptedCharacters
            // 
            this.LblAcceptedCharacters.AutoSize = true;
            this.LblAcceptedCharacters.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcceptedCharacters.Location = new System.Drawing.Point(200, 114);
            this.LblAcceptedCharacters.Name = "LblAcceptedCharacters";
            this.LblAcceptedCharacters.Size = new System.Drawing.Size(177, 13);
            this.LblAcceptedCharacters.TabIndex = 25;
            this.LblAcceptedCharacters.Text = "Enter as 12,080.19.  Do not use \"$\"";
            // 
            // TxtContractWage
            // 
            this.TxtContractWage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractWage.Location = new System.Drawing.Point(137, 83);
            this.TxtContractWage.Name = "TxtContractWage";
            this.TxtContractWage.Size = new System.Drawing.Size(240, 22);
            this.TxtContractWage.TabIndex = 11;
            this.TxtContractWage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LblContractWage
            // 
            this.LblContractWage.AutoSize = true;
            this.LblContractWage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblContractWage.Location = new System.Drawing.Point(15, 85);
            this.LblContractWage.Name = "LblContractWage";
            this.LblContractWage.Size = new System.Drawing.Size(97, 16);
            this.LblContractWage.TabIndex = 0;
            this.LblContractWage.Text = "Contract Wage";
            // 
            // TxtHourlyRate
            // 
            this.TxtHourlyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHourlyRate.Location = new System.Drawing.Point(137, 55);
            this.TxtHourlyRate.Name = "TxtHourlyRate";
            this.TxtHourlyRate.Size = new System.Drawing.Size(240, 22);
            this.TxtHourlyRate.TabIndex = 10;
            this.TxtHourlyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LblHourlyRate
            // 
            this.LblHourlyRate.AutoSize = true;
            this.LblHourlyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblHourlyRate.Location = new System.Drawing.Point(15, 57);
            this.LblHourlyRate.Name = "LblHourlyRate";
            this.LblHourlyRate.Size = new System.Drawing.Size(79, 16);
            this.LblHourlyRate.TabIndex = 0;
            this.LblHourlyRate.Text = "Hourly Rate";
            // 
            // TxtMonthlySalary
            // 
            this.TxtMonthlySalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMonthlySalary.Location = new System.Drawing.Point(137, 27);
            this.TxtMonthlySalary.Name = "TxtMonthlySalary";
            this.TxtMonthlySalary.Size = new System.Drawing.Size(240, 22);
            this.TxtMonthlySalary.TabIndex = 9;
            this.TxtMonthlySalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LblMonthlySalary
            // 
            this.LblMonthlySalary.AutoSize = true;
            this.LblMonthlySalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMonthlySalary.Location = new System.Drawing.Point(15, 29);
            this.LblMonthlySalary.Name = "LblMonthlySalary";
            this.LblMonthlySalary.Size = new System.Drawing.Size(96, 16);
            this.LblMonthlySalary.TabIndex = 0;
            this.LblMonthlySalary.Text = "Monthly Salary";
            // 
            // GBxEmpType
            // 
            this.GBxEmpType.Controls.Add(this.RBtnContractEmp);
            this.GBxEmpType.Controls.Add(this.RBtnHourlyEmp);
            this.GBxEmpType.Controls.Add(this.RBtnSalesEmp);
            this.GBxEmpType.Controls.Add(this.RBtnSalaryEmp);
            this.GBxEmpType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEmpType.Location = new System.Drawing.Point(18, 6);
            this.GBxEmpType.Name = "GBxEmpType";
            this.GBxEmpType.Size = new System.Drawing.Size(382, 132);
            this.GBxEmpType.TabIndex = 13;
            this.GBxEmpType.TabStop = false;
            this.GBxEmpType.Text = "Employee Type";
            // 
            // RBtnContractEmp
            // 
            this.RBtnContractEmp.AutoSize = true;
            this.RBtnContractEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnContractEmp.Location = new System.Drawing.Point(9, 99);
            this.RBtnContractEmp.Name = "RBtnContractEmp";
            this.RBtnContractEmp.Size = new System.Drawing.Size(75, 20);
            this.RBtnContractEmp.TabIndex = 0;
            this.RBtnContractEmp.TabStop = true;
            this.RBtnContractEmp.Text = "Contract";
            this.RBtnContractEmp.UseVisualStyleBackColor = true;
            this.RBtnContractEmp.CheckedChanged += new System.EventHandler(this.RBtnEmpType_CheckedChanged);
            // 
            // RBtnHourlyEmp
            // 
            this.RBtnHourlyEmp.AutoSize = true;
            this.RBtnHourlyEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnHourlyEmp.Location = new System.Drawing.Point(9, 73);
            this.RBtnHourlyEmp.Name = "RBtnHourlyEmp";
            this.RBtnHourlyEmp.Size = new System.Drawing.Size(65, 20);
            this.RBtnHourlyEmp.TabIndex = 0;
            this.RBtnHourlyEmp.TabStop = true;
            this.RBtnHourlyEmp.Text = "Hourly";
            this.RBtnHourlyEmp.UseVisualStyleBackColor = true;
            this.RBtnHourlyEmp.CheckedChanged += new System.EventHandler(this.RBtnEmpType_CheckedChanged);
            // 
            // RBtnSalesEmp
            // 
            this.RBtnSalesEmp.AutoSize = true;
            this.RBtnSalesEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnSalesEmp.Location = new System.Drawing.Point(9, 47);
            this.RBtnSalesEmp.Name = "RBtnSalesEmp";
            this.RBtnSalesEmp.Size = new System.Drawing.Size(61, 20);
            this.RBtnSalesEmp.TabIndex = 0;
            this.RBtnSalesEmp.TabStop = true;
            this.RBtnSalesEmp.Text = "Sales";
            this.RBtnSalesEmp.UseVisualStyleBackColor = true;
            this.RBtnSalesEmp.CheckedChanged += new System.EventHandler(this.RBtnEmpType_CheckedChanged);
            // 
            // RBtnSalaryEmp
            // 
            this.RBtnSalaryEmp.AutoSize = true;
            this.RBtnSalaryEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnSalaryEmp.Location = new System.Drawing.Point(9, 21);
            this.RBtnSalaryEmp.Name = "RBtnSalaryEmp";
            this.RBtnSalaryEmp.Size = new System.Drawing.Size(65, 20);
            this.RBtnSalaryEmp.TabIndex = 0;
            this.RBtnSalaryEmp.TabStop = true;
            this.RBtnSalaryEmp.Text = "Salary";
            this.RBtnSalaryEmp.UseVisualStyleBackColor = true;
            this.RBtnSalaryEmp.CheckedChanged += new System.EventHandler(this.RBtnEmpType_CheckedChanged);
            // 
            // BtnStoreEmpData
            // 
            this.BtnStoreEmpData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStoreEmpData.Location = new System.Drawing.Point(324, 436);
            this.BtnStoreEmpData.Name = "BtnStoreEmpData";
            this.BtnStoreEmpData.Size = new System.Drawing.Size(200, 24);
            this.BtnStoreEmpData.TabIndex = 18;
            this.BtnStoreEmpData.Text = "Store Employee Data";
            this.BtnStoreEmpData.UseVisualStyleBackColor = true;
            this.BtnStoreEmpData.Click += new System.EventHandler(this.BtnStoreEmpData_Click);
            // 
            // LblSuccessOrFailureMsg
            // 
            this.LblSuccessOrFailureMsg.AutoSize = true;
            this.LblSuccessOrFailureMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSuccessOrFailureMsg.ForeColor = System.Drawing.Color.Black;
            this.LblSuccessOrFailureMsg.Location = new System.Drawing.Point(15, 441);
            this.LblSuccessOrFailureMsg.Name = "LblSuccessOrFailureMsg";
            this.LblSuccessOrFailureMsg.Size = new System.Drawing.Size(0, 13);
            this.LblSuccessOrFailureMsg.TabIndex = 16;
            // 
            // GBxEmpFileOps
            // 
            this.GBxEmpFileOps.Controls.Add(this.BtnSaveAndClose);
            this.GBxEmpFileOps.Controls.Add(this.BtnLoadFromFile);
            this.GBxEmpFileOps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEmpFileOps.Location = new System.Drawing.Point(430, 347);
            this.GBxEmpFileOps.Name = "GBxEmpFileOps";
            this.GBxEmpFileOps.Size = new System.Drawing.Size(399, 61);
            this.GBxEmpFileOps.TabIndex = 17;
            this.GBxEmpFileOps.TabStop = false;
            this.GBxEmpFileOps.Text = "File Options";
            // 
            // BtnSaveAndClose
            // 
            this.BtnSaveAndClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSaveAndClose.Location = new System.Drawing.Point(214, 21);
            this.BtnSaveAndClose.Name = "BtnSaveAndClose";
            this.BtnSaveAndClose.Size = new System.Drawing.Size(164, 23);
            this.BtnSaveAndClose.TabIndex = 17;
            this.BtnSaveAndClose.Text = "Save to File";
            this.BtnSaveAndClose.UseVisualStyleBackColor = true;
            this.BtnSaveAndClose.Click += new System.EventHandler(this.BtnSaveToFile_Click);
            // 
            // BtnLoadFromFile
            // 
            this.BtnLoadFromFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLoadFromFile.Location = new System.Drawing.Point(22, 21);
            this.BtnLoadFromFile.Name = "BtnLoadFromFile";
            this.BtnLoadFromFile.Size = new System.Drawing.Size(164, 23);
            this.BtnLoadFromFile.TabIndex = 16;
            this.BtnLoadFromFile.Text = "Load From File";
            this.BtnLoadFromFile.UseVisualStyleBackColor = true;
            this.BtnLoadFromFile.Click += new System.EventHandler(this.BtnLoadFromFile_Click);
            // 
            // TabsEmpForm
            // 
            this.TabsEmpForm.Controls.Add(this.TabAddEmp);
            this.TabsEmpForm.Controls.Add(this.TabCourseInfo);
            this.TabsEmpForm.Location = new System.Drawing.Point(0, -3);
            this.TabsEmpForm.Name = "TabsEmpForm";
            this.TabsEmpForm.SelectedIndex = 0;
            this.TabsEmpForm.Size = new System.Drawing.Size(879, 501);
            this.TabsEmpForm.TabIndex = 4;
            this.TabsEmpForm.Tag = "A";
            // 
            // TabAddEmp
            // 
            this.TabAddEmp.Controls.Add(this.GBxFullPartTime);
            this.TabAddEmp.Controls.Add(this.LblSuccessOrFailureMsg);
            this.TabAddEmp.Controls.Add(this.GBxLookup);
            this.TabAddEmp.Controls.Add(this.GBxEmpType);
            this.TabAddEmp.Controls.Add(this.GBxEmpFileOps);
            this.TabAddEmp.Controls.Add(this.GBxBasicEmpInfo);
            this.TabAddEmp.Controls.Add(this.GBxCompensation);
            this.TabAddEmp.Controls.Add(this.BtnStoreEmpData);
            this.TabAddEmp.Location = new System.Drawing.Point(4, 22);
            this.TabAddEmp.Name = "TabAddEmp";
            this.TabAddEmp.Padding = new System.Windows.Forms.Padding(3);
            this.TabAddEmp.Size = new System.Drawing.Size(871, 475);
            this.TabAddEmp.TabIndex = 0;
            this.TabAddEmp.Text = "Add Employee";
            this.TabAddEmp.UseVisualStyleBackColor = true;
            // 
            // GBxFullPartTime
            // 
            this.GBxFullPartTime.Controls.Add(this.ChBxPartTime);
            this.GBxFullPartTime.Controls.Add(this.ChBxFullTime);
            this.GBxFullPartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxFullPartTime.Location = new System.Drawing.Point(430, 192);
            this.GBxFullPartTime.Name = "GBxFullPartTime";
            this.GBxFullPartTime.Size = new System.Drawing.Size(399, 70);
            this.GBxFullPartTime.TabIndex = 19;
            this.GBxFullPartTime.TabStop = false;
            this.GBxFullPartTime.Text = "Full Time / Part Time";
            // 
            // GBxLookup
            // 
            this.GBxLookup.Controls.Add(this.BtnLookupEmployee);
            this.GBxLookup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxLookup.Location = new System.Drawing.Point(430, 262);
            this.GBxLookup.Name = "GBxLookup";
            this.GBxLookup.Size = new System.Drawing.Size(399, 84);
            this.GBxLookup.TabIndex = 18;
            this.GBxLookup.TabStop = false;
            this.GBxLookup.Text = "Lookup";
            // 
            // BtnLookupEmployee
            // 
            this.BtnLookupEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLookupEmployee.Location = new System.Drawing.Point(64, 30);
            this.BtnLookupEmployee.Name = "BtnLookupEmployee";
            this.BtnLookupEmployee.Size = new System.Drawing.Size(276, 23);
            this.BtnLookupEmployee.TabIndex = 15;
            this.BtnLookupEmployee.Text = "Lookup Employee";
            this.BtnLookupEmployee.UseVisualStyleBackColor = true;
            this.BtnLookupEmployee.Click += new System.EventHandler(this.BtnLookupEmployee_Click);
            // 
            // TabCourseInfo
            // 
            this.TabCourseInfo.Controls.Add(this.GBxLookupCourses);
            this.TabCourseInfo.Controls.Add(this.GBxAddCourse);
            this.TabCourseInfo.Location = new System.Drawing.Point(4, 22);
            this.TabCourseInfo.Name = "TabCourseInfo";
            this.TabCourseInfo.Padding = new System.Windows.Forms.Padding(3);
            this.TabCourseInfo.Size = new System.Drawing.Size(871, 475);
            this.TabCourseInfo.TabIndex = 1;
            this.TabCourseInfo.Text = "Courses";
            this.TabCourseInfo.UseVisualStyleBackColor = true;
            // 
            // GBxLookupCourses
            // 
            this.GBxLookupCourses.Controls.Add(this.BtnLookupCourses);
            this.GBxLookupCourses.Controls.Add(this.TxtLookupCoursesEmpId);
            this.GBxLookupCourses.Controls.Add(this.LblLookupCourses);
            this.GBxLookupCourses.Controls.Add(this.LBxLookupCourses);
            this.GBxLookupCourses.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxLookupCourses.Location = new System.Drawing.Point(391, 6);
            this.GBxLookupCourses.Name = "GBxLookupCourses";
            this.GBxLookupCourses.Size = new System.Drawing.Size(469, 455);
            this.GBxLookupCourses.TabIndex = 0;
            this.GBxLookupCourses.TabStop = false;
            this.GBxLookupCourses.Text = "Lookup Courses";
            // 
            // BtnLookupCourses
            // 
            this.BtnLookupCourses.Location = new System.Drawing.Point(279, 28);
            this.BtnLookupCourses.Name = "BtnLookupCourses";
            this.BtnLookupCourses.Size = new System.Drawing.Size(101, 23);
            this.BtnLookupCourses.TabIndex = 11;
            this.BtnLookupCourses.Text = "Courses";
            this.BtnLookupCourses.UseVisualStyleBackColor = true;
            this.BtnLookupCourses.Click += new System.EventHandler(this.BtnLookupCourses_Click);
            // 
            // TxtLookupCoursesEmpId
            // 
            this.TxtLookupCoursesEmpId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLookupCoursesEmpId.Location = new System.Drawing.Point(98, 28);
            this.TxtLookupCoursesEmpId.Name = "TxtLookupCoursesEmpId";
            this.TxtLookupCoursesEmpId.Size = new System.Drawing.Size(175, 22);
            this.TxtLookupCoursesEmpId.TabIndex = 10;
            // 
            // LblLookupCourses
            // 
            this.LblLookupCourses.AutoSize = true;
            this.LblLookupCourses.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLookupCourses.Location = new System.Drawing.Point(6, 31);
            this.LblLookupCourses.Name = "LblLookupCourses";
            this.LblLookupCourses.Size = new System.Drawing.Size(86, 16);
            this.LblLookupCourses.TabIndex = 0;
            this.LblLookupCourses.Text = "Employee ID";
            // 
            // LBxLookupCourses
            // 
            this.LBxLookupCourses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBxLookupCourses.FormattingEnabled = true;
            this.LBxLookupCourses.Location = new System.Drawing.Point(6, 66);
            this.LBxLookupCourses.Name = "LBxLookupCourses";
            this.LBxLookupCourses.Size = new System.Drawing.Size(457, 368);
            this.LBxLookupCourses.TabIndex = 12;
            // 
            // GBxAddCourse
            // 
            this.GBxAddCourse.Controls.Add(this.LblCrsAddSuccessFailMsg);
            this.GBxAddCourse.Controls.Add(this.LblAddCrsEmpId);
            this.GBxAddCourse.Controls.Add(this.TxtAddCrsEmpId);
            this.GBxAddCourse.Controls.Add(this.CBxCredits);
            this.GBxAddCourse.Controls.Add(this.CbxCourseApproved);
            this.GBxAddCourse.Controls.Add(this.LblCourseApproved);
            this.GBxAddCourse.Controls.Add(this.DTpAppDate);
            this.GBxAddCourse.Controls.Add(this.CBxCourseGrade);
            this.GBxAddCourse.Controls.Add(this.label1);
            this.GBxAddCourse.Controls.Add(this.BtnAddCourse);
            this.GBxAddCourse.Controls.Add(this.LblCourseCredits);
            this.GBxAddCourse.Controls.Add(this.LblApprovedDate);
            this.GBxAddCourse.Controls.Add(this.LblCourseGrade);
            this.GBxAddCourse.Controls.Add(this.LblCourseDesc);
            this.GBxAddCourse.Controls.Add(this.TxtCourseDesc);
            this.GBxAddCourse.Controls.Add(this.LblCourseID);
            this.GBxAddCourse.Controls.Add(this.TxtCourseID);
            this.GBxAddCourse.Controls.Add(this.BtnLookupEmployeeInCrs);
            this.GBxAddCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxAddCourse.Location = new System.Drawing.Point(8, 6);
            this.GBxAddCourse.Name = "GBxAddCourse";
            this.GBxAddCourse.Size = new System.Drawing.Size(377, 455);
            this.GBxAddCourse.TabIndex = 0;
            this.GBxAddCourse.TabStop = false;
            this.GBxAddCourse.Text = "Add Course";
            // 
            // LblAddCrsEmpId
            // 
            this.LblAddCrsEmpId.AutoSize = true;
            this.LblAddCrsEmpId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAddCrsEmpId.Location = new System.Drawing.Point(6, 59);
            this.LblAddCrsEmpId.Name = "LblAddCrsEmpId";
            this.LblAddCrsEmpId.Size = new System.Drawing.Size(86, 16);
            this.LblAddCrsEmpId.TabIndex = 0;
            this.LblAddCrsEmpId.Text = "Employee ID";
            // 
            // TxtAddCrsEmpId
            // 
            this.TxtAddCrsEmpId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAddCrsEmpId.Location = new System.Drawing.Point(142, 58);
            this.TxtAddCrsEmpId.Name = "TxtAddCrsEmpId";
            this.TxtAddCrsEmpId.Size = new System.Drawing.Size(223, 22);
            this.TxtAddCrsEmpId.TabIndex = 1;
            // 
            // CBxCredits
            // 
            this.CBxCredits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBxCredits.FormattingEnabled = true;
            this.CBxCredits.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.CBxCredits.Location = new System.Drawing.Point(142, 245);
            this.CBxCredits.Name = "CBxCredits";
            this.CBxCredits.Size = new System.Drawing.Size(223, 24);
            this.CBxCredits.TabIndex = 7;
            // 
            // CbxCourseApproved
            // 
            this.CbxCourseApproved.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbxCourseApproved.FormattingEnabled = true;
            this.CbxCourseApproved.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.CbxCourseApproved.Location = new System.Drawing.Point(142, 182);
            this.CbxCourseApproved.Name = "CbxCourseApproved";
            this.CbxCourseApproved.Size = new System.Drawing.Size(223, 24);
            this.CbxCourseApproved.TabIndex = 5;
            // 
            // LblCourseApproved
            // 
            this.LblCourseApproved.AutoSize = true;
            this.LblCourseApproved.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCourseApproved.Location = new System.Drawing.Point(6, 183);
            this.LblCourseApproved.Name = "LblCourseApproved";
            this.LblCourseApproved.Size = new System.Drawing.Size(68, 16);
            this.LblCourseApproved.TabIndex = 0;
            this.LblCourseApproved.Text = "Approved";
            // 
            // DTpAppDate
            // 
            this.DTpAppDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTpAppDate.Location = new System.Drawing.Point(142, 214);
            this.DTpAppDate.Name = "DTpAppDate";
            this.DTpAppDate.Size = new System.Drawing.Size(223, 21);
            this.DTpAppDate.TabIndex = 6;
            this.DTpAppDate.Value = new System.DateTime(2016, 2, 25, 0, 0, 0, 0);
            // 
            // CBxCourseGrade
            // 
            this.CBxCourseGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBxCourseGrade.FormattingEnabled = true;
            this.CBxCourseGrade.Items.AddRange(new object[] {
            "A",
            "A-",
            "B+",
            "B",
            "B-",
            "C+",
            "C",
            "C-",
            "D+",
            "D",
            "D-",
            "E",
            "In Progress"});
            this.CBxCourseGrade.Location = new System.Drawing.Point(142, 150);
            this.CBxCourseGrade.Name = "CBxCourseGrade";
            this.CBxCourseGrade.Size = new System.Drawing.Size(223, 24);
            this.CBxCourseGrade.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Utah Valley University Course Information";
            // 
            // BtnAddCourse
            // 
            this.BtnAddCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddCourse.Location = new System.Drawing.Point(80, 302);
            this.BtnAddCourse.Name = "BtnAddCourse";
            this.BtnAddCourse.Size = new System.Drawing.Size(216, 23);
            this.BtnAddCourse.TabIndex = 8;
            this.BtnAddCourse.Text = "Add Course";
            this.BtnAddCourse.UseVisualStyleBackColor = true;
            this.BtnAddCourse.Click += new System.EventHandler(this.BtnAddCourse_Click);
            // 
            // LblCourseCredits
            // 
            this.LblCourseCredits.AutoSize = true;
            this.LblCourseCredits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCourseCredits.Location = new System.Drawing.Point(6, 246);
            this.LblCourseCredits.Name = "LblCourseCredits";
            this.LblCourseCredits.Size = new System.Drawing.Size(50, 16);
            this.LblCourseCredits.TabIndex = 0;
            this.LblCourseCredits.Text = "Credits";
            // 
            // LblApprovedDate
            // 
            this.LblApprovedDate.AutoSize = true;
            this.LblApprovedDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblApprovedDate.Location = new System.Drawing.Point(7, 214);
            this.LblApprovedDate.Name = "LblApprovedDate";
            this.LblApprovedDate.Size = new System.Drawing.Size(100, 16);
            this.LblApprovedDate.TabIndex = 0;
            this.LblApprovedDate.Text = "Approved Date";
            // 
            // LblCourseGrade
            // 
            this.LblCourseGrade.AutoSize = true;
            this.LblCourseGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCourseGrade.Location = new System.Drawing.Point(6, 151);
            this.LblCourseGrade.Name = "LblCourseGrade";
            this.LblCourseGrade.Size = new System.Drawing.Size(46, 16);
            this.LblCourseGrade.TabIndex = 0;
            this.LblCourseGrade.Text = "Grade";
            // 
            // LblCourseDesc
            // 
            this.LblCourseDesc.AutoSize = true;
            this.LblCourseDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCourseDesc.Location = new System.Drawing.Point(6, 120);
            this.LblCourseDesc.Name = "LblCourseDesc";
            this.LblCourseDesc.Size = new System.Drawing.Size(122, 16);
            this.LblCourseDesc.TabIndex = 0;
            this.LblCourseDesc.Text = "Course Description";
            // 
            // TxtCourseDesc
            // 
            this.TxtCourseDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCourseDesc.Location = new System.Drawing.Point(142, 119);
            this.TxtCourseDesc.Name = "TxtCourseDesc";
            this.TxtCourseDesc.Size = new System.Drawing.Size(223, 22);
            this.TxtCourseDesc.TabIndex = 3;
            // 
            // LblCourseID
            // 
            this.LblCourseID.AutoSize = true;
            this.LblCourseID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCourseID.Location = new System.Drawing.Point(6, 87);
            this.LblCourseID.Name = "LblCourseID";
            this.LblCourseID.Size = new System.Drawing.Size(67, 16);
            this.LblCourseID.TabIndex = 0;
            this.LblCourseID.Text = "Course ID";
            // 
            // TxtCourseID
            // 
            this.TxtCourseID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCourseID.Location = new System.Drawing.Point(142, 86);
            this.TxtCourseID.Name = "TxtCourseID";
            this.TxtCourseID.Size = new System.Drawing.Size(223, 22);
            this.TxtCourseID.TabIndex = 2;
            // 
            // BtnLookupEmployeeInCrs
            // 
            this.BtnLookupEmployeeInCrs.Location = new System.Drawing.Point(6, 422);
            this.BtnLookupEmployeeInCrs.Name = "BtnLookupEmployeeInCrs";
            this.BtnLookupEmployeeInCrs.Size = new System.Drawing.Size(238, 23);
            this.BtnLookupEmployeeInCrs.TabIndex = 9;
            this.BtnLookupEmployeeInCrs.Text = "Lookup Employee";
            this.BtnLookupEmployeeInCrs.UseVisualStyleBackColor = true;
            this.BtnLookupEmployeeInCrs.Click += new System.EventHandler(this.BtnLookupEmployee_Click);
            // 
            // ChBxFullTime
            // 
            this.ChBxFullTime.AutoSize = true;
            this.ChBxFullTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChBxFullTime.Location = new System.Drawing.Point(41, 31);
            this.ChBxFullTime.Name = "ChBxFullTime";
            this.ChBxFullTime.Size = new System.Drawing.Size(82, 20);
            this.ChBxFullTime.TabIndex = 0;
            this.ChBxFullTime.Text = "Full Time";
            this.ChBxFullTime.UseVisualStyleBackColor = true;
            this.ChBxFullTime.CheckedChanged += new System.EventHandler(this.ChBxFullTime_CheckedChanged);
            // 
            // ChBxPartTime
            // 
            this.ChBxPartTime.AutoSize = true;
            this.ChBxPartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChBxPartTime.Location = new System.Drawing.Point(247, 31);
            this.ChBxPartTime.Name = "ChBxPartTime";
            this.ChBxPartTime.Size = new System.Drawing.Size(85, 20);
            this.ChBxPartTime.TabIndex = 1;
            this.ChBxPartTime.Text = "Part Time";
            this.ChBxPartTime.UseVisualStyleBackColor = true;
            this.ChBxPartTime.CheckedChanged += new System.EventHandler(this.ChBxPartTime_CheckedChanged);
            // 
            // LblCrsAddSuccessFailMsg
            // 
            this.LblCrsAddSuccessFailMsg.AutoSize = true;
            this.LblCrsAddSuccessFailMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCrsAddSuccessFailMsg.Location = new System.Drawing.Point(7, 345);
            this.LblCrsAddSuccessFailMsg.Name = "LblCrsAddSuccessFailMsg";
            this.LblCrsAddSuccessFailMsg.Size = new System.Drawing.Size(0, 13);
            this.LblCrsAddSuccessFailMsg.TabIndex = 10;
            // 
            // FrmEmp
            // 
            this.AcceptButton = this.BtnStoreEmpData;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 497);
            this.Controls.Add(this.TabsEmpForm);
            this.Name = "FrmEmp";
            this.Text = "Employee";
            this.GBxBasicEmpInfo.ResumeLayout(false);
            this.GBxBasicEmpInfo.PerformLayout();
            this.GBxCompensation.ResumeLayout(false);
            this.GBxCompensation.PerformLayout();
            this.GBxEmpType.ResumeLayout(false);
            this.GBxEmpType.PerformLayout();
            this.GBxEmpFileOps.ResumeLayout(false);
            this.TabsEmpForm.ResumeLayout(false);
            this.TabAddEmp.ResumeLayout(false);
            this.TabAddEmp.PerformLayout();
            this.GBxFullPartTime.ResumeLayout(false);
            this.GBxFullPartTime.PerformLayout();
            this.GBxLookup.ResumeLayout(false);
            this.TabCourseInfo.ResumeLayout(false);
            this.GBxLookupCourses.ResumeLayout(false);
            this.GBxLookupCourses.PerformLayout();
            this.GBxAddCourse.ResumeLayout(false);
            this.GBxAddCourse.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox GBxBasicEmpInfo;
        private System.Windows.Forms.TextBox TxtEmpID;
        private System.Windows.Forms.TextBox TxtFirstName;
        private System.Windows.Forms.TextBox TxtLastName;
        private System.Windows.Forms.Label LblLastName;
        private System.Windows.Forms.Label LblFirstName;
        private System.Windows.Forms.Label LblEmpID;
        private System.Windows.Forms.GroupBox GBxCompensation;
        private System.Windows.Forms.TextBox TxtMonthlySalary;
        private System.Windows.Forms.Label LblMonthlySalary;
        private System.Windows.Forms.TextBox TxtHourlyRate;
        private System.Windows.Forms.Label LblHourlyRate;
        private System.Windows.Forms.TextBox TxtContractWage;
        private System.Windows.Forms.Label LblContractWage;
        private System.Windows.Forms.GroupBox GBxEmpType;
        private System.Windows.Forms.RadioButton RBtnContractEmp;
        private System.Windows.Forms.RadioButton RBtnHourlyEmp;
        private System.Windows.Forms.RadioButton RBtnSalesEmp;
        private System.Windows.Forms.RadioButton RBtnSalaryEmp;
        private System.Windows.Forms.Button BtnStoreEmpData;
        private System.Windows.Forms.Label LblSuccessOrFailureMsg;
        private System.Windows.Forms.Label LblAcceptedCharacters;
        private System.Windows.Forms.GroupBox GBxEmpFileOps;
        private System.Windows.Forms.Button BtnSaveAndClose;
        private System.Windows.Forms.Button BtnLoadFromFile;
        private System.Windows.Forms.TabControl TabsEmpForm;
        private System.Windows.Forms.TabPage TabAddEmp;
        private System.Windows.Forms.TabPage TabCourseInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnAddCourse;
        private System.Windows.Forms.Label LblCourseCredits;
        private System.Windows.Forms.Label LblApprovedDate;
        private System.Windows.Forms.Label LblCourseGrade;
        private System.Windows.Forms.Label LblCourseDesc;
        private System.Windows.Forms.TextBox TxtCourseDesc;
        private System.Windows.Forms.Label LblCourseID;
        private System.Windows.Forms.TextBox TxtCourseID;
        private System.Windows.Forms.Button BtnLookupEmployeeInCrs;
        private System.Windows.Forms.GroupBox GBxAddCourse;
        private System.Windows.Forms.ComboBox CBxCourseGrade;
        private System.Windows.Forms.DateTimePicker DTpAppDate;
        private System.Windows.Forms.ComboBox CbxCourseApproved;
        private System.Windows.Forms.Label LblCourseApproved;
        private System.Windows.Forms.ComboBox CBxCredits;
        private System.Windows.Forms.TextBox TxtMiddleInitial;
        private System.Windows.Forms.Label LblMiddleInitial;
        private System.Windows.Forms.Label LblMaritalStatus;
        private System.Windows.Forms.ComboBox CBxMaritalStatus;
        private System.Windows.Forms.Label LblDepartment;
        private System.Windows.Forms.DateTimePicker DTpStartDate;
        private System.Windows.Forms.Label LblStartDate;
        private System.Windows.Forms.TextBox TxtTitle;
        private System.Windows.Forms.Label LblTitle;
        private System.Windows.Forms.TextBox TxtAgency;
        private System.Windows.Forms.Label LblAgency;
        private System.Windows.Forms.GroupBox GBxLookupCourses;
        private System.Windows.Forms.GroupBox GBxLookup;
        private System.Windows.Forms.Button BtnLookupEmployee;
        private System.Windows.Forms.ListBox LBxLookupCourses;
        private System.Windows.Forms.TextBox TxtDepartment;
        private System.Windows.Forms.Label LblAddCrsEmpId;
        private System.Windows.Forms.TextBox TxtAddCrsEmpId;
        private System.Windows.Forms.Button BtnLookupCourses;
        private System.Windows.Forms.TextBox TxtLookupCoursesEmpId;
        private System.Windows.Forms.Label LblLookupCourses;
        private System.Windows.Forms.GroupBox GBxFullPartTime;
        private System.Windows.Forms.CheckBox ChBxPartTime;
        private System.Windows.Forms.CheckBox ChBxFullTime;
        private System.Windows.Forms.Label LblCrsAddSuccessFailMsg;
    }
}

