﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections.Generic;

namespace Lab_03
{
    /// <summary>
    /// Factory class for creating employee objects
    /// </summary>
    public static class EmpObjFactory
    {
        //Constants
        public const string NO_EMP_TYPE_ERROR = "NO_EMP_TYPE_ERROR";

        //Locals
        public static uint EmpID { get; set; } = 0;
        public static EType EmpType { get; set; } = EType.NOT_SET_BAD;
        public static string FirstName { get; set; } = String.Empty;
        public static string LastName { get; set; } = String.Empty;
        public static decimal MonthlySalary { get; set; } = 0;
        public static decimal HourlyRate { get; set; } = 0;
        public static decimal ContractWage { get; set; } = 0;
        public static string MiddleInitial { get; set; } = String.Empty;
        public static ConstantsClass.MarritalStatus IsMarried { get; set; } = ConstantsClass.MarritalStatus.NOT_SET;
        public static string Department { get; set; } = String.Empty;
        public static string Title { get; set; } = String.Empty;
        public static string Agency { get; set; } = String.Empty;
        public static DateTime? StartDate { get; set; } = null;
        public static ConstantsClass.EmpStatus EmployeeStatus { get; set; } = ConstantsClass.EmpStatus.NOT_SET;

        private static List<String> missingData = new List<String>();


        /// <summary>
        /// Verifies all required data is present for building the specified Employee object type
        /// </summary>
        /// <returns>A List(type String) containing missing data elements required to build the Employee sub-class 
        /// object.  If all elements are present, the list is returned empty. </returns>
        public static List<String> VerifyHaveReqData()
        {
            //Basic info
            if (EmpID <= 0)
            {
                missingData.Add(EmpDataNames.ID.ToString());
            }

            if (FirstName == String.Empty)
            {
                missingData.Add(EmpDataNames.FIRST_NAME.ToString());
            }

            if (LastName == String.Empty)
            {
                missingData.Add(EmpDataNames.LAST_NAME.ToString());
            }

            if (MiddleInitial == String.Empty)
            {
                missingData.Add(EmpDataNames.MIDDLE_INITIAL.ToString());
            }

            if (IsMarried == ConstantsClass.MarritalStatus.NOT_SET)
            {
                missingData.Add(EmpDataNames.IS_MARRIED.ToString());
            }

            if (Department == String.Empty)
            {
                missingData.Add(EmpDataNames.DEPARTMENT.ToString());
            }

            if (Title == String.Empty)
            {
                missingData.Add(EmpDataNames.TITLE.ToString());
            }

            if (StartDate == null)
            {
                missingData.Add(EmpDataNames.START_DATE.ToString());
            }

            //Additional info and employee type
            switch (EmpType)
            {
                case (EType.SALARY):
                    if (MonthlySalary <= 0)
                    {
                        missingData.Add(EmpDataNames.MONTHLY_SALARY.ToString());
                    }

                    if (EmployeeStatus == ConstantsClass.EmpStatus.NOT_SET)
                    {
                        missingData.Add(EmpDataNames.EMPLOYEE_STATUS.ToString());
                    }

                    break;

                case (EType.SALES):
                    if (MonthlySalary <= 0)
                    {
                        missingData.Add(EmpDataNames.MONTHLY_SALARY.ToString());
                    }

                    if (EmployeeStatus == ConstantsClass.EmpStatus.NOT_SET)
                    {
                        missingData.Add(EmpDataNames.EMPLOYEE_STATUS.ToString());
                    }

                    break;

                case (EType.HOURLY):
                    if (HourlyRate <= 0)
                    {
                        missingData.Add(EmpDataNames.HOURLY_RATE.ToString());
                    }

                    if (EmployeeStatus == ConstantsClass.EmpStatus.NOT_SET)
                    {
                        missingData.Add(EmpDataNames.EMPLOYEE_STATUS.ToString());
                    }

                    break;

                case (EType.CONTRACT):
                    if (ContractWage <= 0)
                    {
                        missingData.Add(EmpDataNames.CONTRACT_WAGE.ToString());
                    }
                    
                    if (Agency == String.Empty)
                    {
                        missingData.Add(EmpDataNames.AGENCY.ToString());
                    }

                    break;

                default:
                    missingData.Add(NO_EMP_TYPE_ERROR);
                    return missingData;
            }

            return missingData;

        }//end VerifyHaveReqData()

        /// <summary>
        /// Builds the Employee sub-class object
        /// </summary>
        /// <remarks>This method does not verify Employee Data values (EmpID, FirstName, LastName, ...) are initialized. </remarks>
        /// <returns>The Employee class object, or null if EType containes an invalid value</returns>
        public static Employee BuildEmpObj()
        {
            switch (EmpType)
            {
                case (EType.SALARY):
                    return new Salary(EmpID, FirstName, LastName, MiddleInitial, EmpType, IsMarried, Department, Title, 
                        EmployeeStatus, StartDate, MonthlySalary);

                case (EType.SALES):
                    return new Sales(EmpID, FirstName, LastName, MiddleInitial, EmpType, IsMarried, Department, Title,
                        EmployeeStatus, StartDate, MonthlySalary);

                case (EType.HOURLY):
                    return new Hourly(EmpID, FirstName, LastName, MiddleInitial, EmpType, IsMarried, Department, Title,
                        EmployeeStatus, StartDate, HourlyRate);

                case (EType.CONTRACT):
                    return new Contract(EmpID, FirstName, LastName, MiddleInitial, EmpType, IsMarried, Department, Title, 
                        EmployeeStatus, StartDate, ContractWage, Agency);

                default:
                    return null;
            }
        }//end BuildEmpObj()

        /// <summary>
        /// Resets properties to default values
        /// <remarks>Does not reset (EmpObjFactory.EmpType) property. </remarks>
        /// </summary>
        public static void ResetAllButEmpType()
        {
            EmpID = 0;
            FirstName = String.Empty;
            LastName = String.Empty;
            MonthlySalary = 0;
            HourlyRate = 0;
            ContractWage = 0;
            MiddleInitial = String.Empty;
            IsMarried = ConstantsClass.MarritalStatus.NOT_SET;
            Department = String.Empty;
            Title = String.Empty;
            Agency = String.Empty;
            StartDate = null;
            EmployeeStatus = ConstantsClass.EmpStatus.NOT_SET;
            missingData = new List<String>();

        }//end ResetAllButEmpType()

    }//end EmpObjFactory class

}//end Lab_03 namespace
