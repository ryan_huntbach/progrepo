﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_08
// Date: 1/25/2016 13:10:22 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------



using System;
using System.Runtime.Serialization;

namespace Lab_03
{
    /// <summary>
    /// Models the information for a Utah Valley University course
    /// </summary>
    [Serializable]
    public class Course : ISerializable
    {
        //Members
        public String courseID = null;
        public String courseDesc = null;
        public String courseGrade = null;
        public DateTime? approvedDate = DateTime.MinValue;
        public uint courseCredits = 0;

        /// <summary>
        /// The Course constructor
        /// </summary>
        /// <param name="id">any valid string</param>
        /// <param name="desc">andy valid string</param>
        /// <param name="grade">any Grades enum element</param>
        /// <param name="apprDate">any valid DateTime</param>
        /// <param name="credits">any valid uint</param>
        public Course(String id, String desc, String grade, DateTime? apprDate, uint credits)
        {
            courseID = id;
            courseDesc = desc;
            courseGrade = grade;
            courseCredits = credits;

            if (apprDate == null)
            {
                approvedDate = DateTime.MinValue;
            }
            else
            {
                approvedDate = apprDate;
            }

        }//end Course(...)


        /// <summary>
        /// Maps ISerializable BinaryFormatter deserialization to Course class object
        /// </summary>
        /// <param name="info">info about serialization.</param>
        /// <param name="context">context of the serialization.</param>
        public Course(SerializationInfo info, StreamingContext context) 
        {
            courseID = info.GetString(ConstantsClass.COURSE_ID_STR);
            courseDesc = info.GetString(ConstantsClass.COURSE_DESCRIPTION_STR);
            courseGrade = info.GetString(ConstantsClass.COURSE_GRADE_STR);
            approvedDate = info.GetDateTime(ConstantsClass.APPROVED_DATE_STR);
            courseCredits = info.GetUInt32(ConstantsClass.COURSE_CREDITS_STR);

        }//end Course(...)


        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(ConstantsClass.COURSE_ID_STR, courseID);
            info.AddValue(ConstantsClass.COURSE_DESCRIPTION_STR, courseDesc);
            info.AddValue(ConstantsClass.COURSE_GRADE_STR, courseGrade);
            info.AddValue(ConstantsClass.APPROVED_DATE_STR, approvedDate);
            info.AddValue(ConstantsClass.COURSE_CREDITS_STR, courseCredits);
        }//end GetObjectData(...)

    }//end Course class

}//end Lab_03 namespace
