﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_07
// Date: 2/18/2016 12:41:59 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace Lab_03
{
    /// <summary>
    /// Handles serialized file IO operations.  Extends Object and implements IFileAccess.
    /// </summary>
    public sealed class FileIO : Object, IFileAccess
    {
        //Constant members
        const int MAX_BYTE_STREAM_READ_LENGTH = 10;

        //Member properties
        public SortedDictionary DB
        {
            get { return DB; }

            set { DB = SortedDictionary.GetInstance(); }
        }

        //Members
        private List<Employee> empList = new List<Employee>();
        private BusinessRules businessRules = BusinessRules.GetInstance();
        private String writePath = String.Empty;
        private String readPath = String.Empty;
        private BinaryFormatter binForm = null;


        /// <summary>
        /// Opens and/or creates file to write DB to, and writes information contained in DB to the file
        /// </summary>
        public void CloseDB()
        {
            SaveFileDialog saveFileDialogWriteFile = new SaveFileDialog();
            String writeErrorMsg = "Error: File was not written.";
            String saveDialogFilterStr = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            saveFileDialogWriteFile.Filter = saveDialogFilterStr;
            saveFileDialogWriteFile.FilterIndex = 2;
            saveFileDialogWriteFile.RestoreDirectory = true;

            if (saveFileDialogWriteFile.ShowDialog() == DialogResult.OK)
            {
                writePath = saveFileDialogWriteFile.FileName;
                WriteDB();
            }
            else
            {
                MessageBox.Show(writeErrorMsg);
            }

        }//end CloseDB()


        /// <summary>
        /// Opens file and initializes readStream, if file can be read.
        /// </summary>
        public void OpenDB()
        {
            OpenFileDialog openFileDialogReadDB = new OpenFileDialog();
            String fileOpenErrorMsg = "Error: Could not locate file.";
            String openDialogFilterStr = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            openFileDialogReadDB.InitialDirectory = "c:\\";
            openFileDialogReadDB.Filter = openDialogFilterStr;
            openFileDialogReadDB.FilterIndex = 2;
            openFileDialogReadDB.RestoreDirectory = true;

            if (openFileDialogReadDB.ShowDialog() == DialogResult.OK)
            {
                readPath = openFileDialogReadDB.FileName;
                ReadDB();                
            }
            else
            {
                MessageBox.Show(fileOpenErrorMsg);
            }

        }//end OpenDB()


        /// <summary>
        /// Reads file, deserializes it, and converts to employee objects;
        /// </summary>
        public void ReadDB()
        {
            FileStream readStream = null;
            binForm = new BinaryFormatter();
            Employee tempEmp = null;
            int streamOrigLen = 0;
            int bytesToRead = 0;
            int curPos = 0;
            empList = new List<Employee>();
            String invalidFileErrorMsg = "Invalid file type.\r\nPlease try again.";
            String nullStreamErrorMsg = "Error: Could not open file.\r\n";
            String unknownFileReadErrorMsg = "Error: Unknown file read error.\r\nIs the file type correct?\r\n.";

            try
            {
                readStream = new FileStream(readPath, FileMode.Open);
                streamOrigLen = (int)readStream.Length;
                bytesToRead = (int)readStream.Length;
                curPos = (int)readStream.Position;

                while (bytesToRead > 0)
                {
                    tempEmp = (Employee)binForm.Deserialize(readStream);

                    if (tempEmp != null)
                    {
                        empList.Add(tempEmp);
                        businessRules.empDictionary.AddValue(tempEmp.EmpID, ref tempEmp);
                    }

                    curPos = (int)readStream.Position;
                    bytesToRead = streamOrigLen - curPos;
                }
            }
            catch (SerializationException e)
            {
                MessageBox.Show(invalidFileErrorMsg);
                return;
            }
            catch (Exception e) when (readStream == null)
            {
                MessageBox.Show(nullStreamErrorMsg + e.Message);
                return;
            }
            catch (Exception e)
            {
                MessageBox.Show(unknownFileReadErrorMsg + e.Message);
                return;
            }

            DisplayEmpList();

            if (readStream != null)
            {
                readStream.Close();
            }

        }//end ReadDB()


        /// <summary>
        /// Serializes employee objects contained in BusinessRules storage and writes it to client specified file
        /// <remarks>Clears and overwrites file, if it existed previously.</remarks>
        /// </summary>
        public void WriteDB()
        {
            FileStream writeStream = null;
            binForm = new BinaryFormatter();
            String savedMsg = "Successfully saved file:\r\n" + writePath;
            String fileNotWrittenErrorMsg = "Error: File was not written.";

            if (File.Exists(writePath))
            {
                File.Delete(writePath);
            }

            using (writeStream = new FileStream(writePath, FileMode.OpenOrCreate))
            {
                if (writeStream == null)
                {
                    MessageBox.Show(fileNotWrittenErrorMsg);
                    return;
                }

                foreach (KeyValuePair<uint, Employee> entry in businessRules.empDictionary)
                {
                    binForm.Serialize(writeStream, entry.Value);                    
                }
            }

            MessageBox.Show(savedMsg);

            if (writeStream != null)
            {
                writeStream.Close();
            }           

        }//end WriteDB()

        
        /// <summary>
        /// Displays a MessageBox showing all employees in empList
        /// </summary>
        private void DisplayEmpList()
        {
            StringBuilder sb = new StringBuilder();
            String empsReadHeader = "Employees Loaded From File:\r\n\r\n";
            String idHeader = "  (ID: ", empTypeHeader = ", EmployeeType: ", contractHeader = ", Contract Wage: ", 
                hourlyHeader = ", Hourly Rate: ", salaryAndSalesHeader = ", Monthly Salary: ", closingParen = ")", blankSpacesBtwnEmps = "\r\n";

            sb.Append(empsReadHeader);

            foreach (Employee emp in empList)
            {
                sb.Append(emp.FirstName + " " + emp.LastName + idHeader + emp.EmpID + empTypeHeader + emp.EmpType.ToString());

                switch (emp.EmpType)
                {
                    case (EType.CONTRACT):
                        sb.Append(contractHeader + (emp as Contract).ContractWage.ToString() + closingParen);
                        break;
                    case (EType.HOURLY):
                        sb.Append(hourlyHeader + (emp as Hourly).HourlyRate.ToString() + closingParen);
                        break;
                    case (EType.SALARY):
                        sb.Append(salaryAndSalesHeader + (emp as Salary).MonthlySalary.ToString() + closingParen);
                        break;
                    case (EType.SALES):
                        sb.Append(salaryAndSalesHeader + (emp as Sales).MonthlySalary.ToString() + closingParen);
                        break;
                    default:
                        break;
                }

                sb.Append(blankSpacesBtwnEmps);
            }

            MessageBox.Show(sb.ToString());

        }//end DisplayEmpsList()
        
    }//end FileIO class

}//end Lab_03 namespace
