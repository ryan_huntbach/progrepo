﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lab_03
{
    /// <summary>
    /// This is the base class for all Employee objects
    /// </summary>
    [Serializable()]
    public abstract class Employee : ISerializable
    {
        //Members
        public SortedDictionary<string, Course> employeeCourses;

        //Member properties
        public uint EmpID { get; set; }
        public EType EmpType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public ConstantsClass.MarritalStatus IsMarried { get; set; }
        public string Department { get; set; }
        public string Title { get; set; }
        public DateTime? StartDate { get; set; }
        public bool IsCurrentEmp { get; set; }
        public ConstantsClass.EmpStatus EmployeeStatus { get; set; }


        /// <summary>
        /// Employee class constructor
        /// </summary>
        /// <param name="id">any valid uint</param>
        /// <param name="firstName">and valid string </param>
        /// <param name="lastName">any valid string </param>
        /// <param name="middleInitial">any valid string </param>
        /// <param name="empType">An EType (enum) indicating the employee's compensation status (Salary, Sales, Hourly, Contract) </param>
        /// <param name="isMarried">any valid MarritalStatus (enum) type </param>
        /// <param name="department">any valid string </param>
        /// <param name="title">any valid string </param>
        /// <param name="startDate">any valid DateTime </param>
        /// <param name="employeeStatus">any valid EmployeeStatus (enum) type </param>
        /// <param name="isCurrentEmp">any valid bool (optional) </param>
        /// <param name="courses">any valid SortedDictionary(string, Course) (optional) </param>
        public Employee(uint id, string firstName, string lastName, string middleInitial, EType empType, 
            ConstantsClass.MarritalStatus isMarried, string department, string title, ConstantsClass.EmpStatus employeeStatus,
            DateTime? startDate, bool isCurrentEmp = true, SortedDictionary<string, Course> courses = null)
        {
            this.EmpID = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.MiddleInitial = middleInitial;
            this.EmpType = empType;
            this.IsMarried = isMarried;
            this.Department = department;
            this.Title = title;
            this.StartDate = startDate;
            this.IsCurrentEmp = isCurrentEmp;
            this.EmployeeStatus = employeeStatus;

            if (courses == null)
            {
                this.employeeCourses = new SortedDictionary<string, Course>();
            }
            else
            {
                this.employeeCourses = courses;
            }

        }//end Employee(...)        


        /// <summary>
        /// Provides the data ISerializable needs for BinaryFormatter serialization
        /// </summary>
        /// <param name="info">info about serialization</param>
        /// <param name="context">context of the serialization to be performed</param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(ConstantsClass.EMP_ID_STR, EmpID);
            info.AddValue(ConstantsClass.FIRST_NAME_STR, FirstName);
            info.AddValue(ConstantsClass.LAST_NAME_STR, LastName);
            info.AddValue(ConstantsClass.MIDDLE_INITIAL_STR, MiddleInitial);
            info.AddValue(ConstantsClass.EMP_TYPE_STR, EmpType);
            info.AddValue(ConstantsClass.IS_MARRIED_STR, IsMarried);
            info.AddValue(ConstantsClass.DEPARTMENT_STR, Department);
            info.AddValue(ConstantsClass.TITLE_STR, Title);
            info.AddValue(ConstantsClass.EMPLOYEE_STATUS_STR, EmployeeStatus);
            info.AddValue(ConstantsClass.START_DATE_STR, StartDate);
            info.AddValue(ConstantsClass.IS_CURRENT_EMP_STR, IsCurrentEmp);
            info.AddValue(ConstantsClass.EMPLOYEE_COURSES_STR, employeeCourses);

        }//end GetObjectData(...)

    }//end Employee class

}//end Lab_03 namespace
