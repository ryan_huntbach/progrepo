﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_06
// Date: 2/09/2016 12:21:19 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Lab_03
{
    /// <summary>
    /// This is a Singleton class, for a SortedDictionary that holds a uint EmployeeID (key) and an Employee (value)
    /// </summary>
    [Serializable]
    public sealed class SortedDictionary : IEnumerable
    {
        //Members
        private SortedDictionary<uint, Employee> sDictionary = new SortedDictionary<uint, Employee>();
        private static volatile SortedDictionary instance = null;
        private static Object syncLock = new object();


        /// <summary>
        /// Non-Parameterized Constructor.  SortedDictionary is a Singleton, so the constructor is inaccessable from outside. 
        /// </summary>
        private SortedDictionary() { }//end SortedDictionary()


        /// <summary>
        /// Adds employee id and employee to sorted dictionary
        /// <remarks>Will not throw exception if Employee emp param is null.</remarks>
        /// </summary>
        public void AddValue(uint id, ref Employee emp)
        {
            if (emp != null)
            {
                sDictionary[id] = emp;
            }

        }//end AddValue(...)


        /// <summary>
        /// Removes the specified value, if contained in the dictionary
        /// </summary>
        /// <param name="id">any valid uint</param>
        public void RemoveValue(uint id)
        {
            if (sDictionary.Keys.Contains(id))
            {
                sDictionary.Remove(id);
            }
        }//end RemoveValue(...)


        /// <summary>
        /// Determines whether or not the dictionary contains the given key
        /// </summary>
        /// <param name="id">any valid uint</param>
        /// <returns>true, if the dictionary contains the key (parameter), else returns false.</returns>
        public bool Contains(uint id)
        {
            foreach (KeyValuePair<uint, Employee> entry in sDictionary)
            {
                if (entry.Key == id)
                {
                    return true;
                }
            }

            return false;
        }//end Contains(...)


        /// <summary>
        /// Returns the number of entries in the dictionary
        /// </summary>
        /// <returns>int, the number of entries</returns>
        public int Count()
        {
            return sDictionary.Count;

        }//end Count()


        /// <summary>
        /// Retrieves the instance of SortedDictionary
        /// <remarks>
        /// - This method implements the Singleton pattern.  
        /// - Method is thread safe. 
        /// </remarks>
        /// </summary>
        /// <returns>If no instance of SortedDictionary has been created, a new instance is returned, else the 
        /// pre-existing single instance of SortedDictionary is returned. </returns>
        public static SortedDictionary GetInstance()
        {
            if (instance == null)
            {
                lock (syncLock)
                {
                    if (instance == null)
                    {
                        instance = new SortedDictionary();
                    }
                }
            }

            return instance;

        }//end GetInstance()


        /// <summary>
        /// Indexer for SortedDictionary
        /// </summary>
        /// <param name="index">Any valid uint </param>
        /// <returns>The Employee value at id(key) </returns>
        public Employee this[uint id]
        {
            get
            {
                if (sDictionary.ContainsKey(id))
                {
                    return sDictionary[id];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (value != null)
                {
                    sDictionary[id] = value;
                }                
            }
        }//end Indexer definition


        /// <summary>
        /// Resets the SortedDictionary to a new, empty SortedDictionary
        /// </summary>
        public void Reset()
        {
            sDictionary = null;
            sDictionary = new SortedDictionary<uint, Employee>();
        }//end Reset()


        /// <summary>
        /// Returns the value associated with the specified key
        /// <remarks>Performs basic validation before attempting to get the value.</remarks>
        /// </summary>
        /// <param name="index">any valid uint</param>
        /// <returns>The employee object associated with the specified key, if the key has a valid employee object, else returns null. </returns>
        public Employee GetValue(uint id)
        {
            if (sDictionary.ContainsKey(id))
            {
                return sDictionary[id];
            }
            else
            {
                return null;
            }
        }//end GetValue(...)

        /// <summary>
        /// Returns an enumerator for the sDictionary
        /// </summary>
        /// <returns>IEnumertor object for sDictionary.</returns>
        public IEnumerator GetEnumerator()
        {
            return sDictionary.GetEnumerator();
            
        }//end GetEnumerator()
        
    }//end SortedDictionary class

}//end Lab_03 namespace
