﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 2/04/2016 14:09:10 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------



using System.Collections.Generic;

namespace Lab_03
{
    /// <summary>
    /// This class performs unit and end to end tests for FrmAddEmp, and all its related classes.  
    /// The Tests class depends on data from the TestData class and the display in the FrmTestOutput class.
    /// </summary>
    public class Tests
    {
        //Constant Members
        private string INITIALIZED_MSG = "UNIT AND END TO END TESTS INITIALIZED.\r\n(Tip: Use mouse for 'NEXT' and 'Enter' to save records)\r\n\r\n";

        //Members
        private FrmTestOutput testOutputFrm;
        private FrmEmp testAddEmpFrm;
        private BusinessRules testBusinessRules;
        private int currentTest = 1;
                

        /// <summary>
        /// The Tests class parameterized constructor
        /// </summary>
        public Tests()
        {
            testOutputFrm = new FrmTestOutput();
            testAddEmpFrm = Program.addEmp;
            testBusinessRules = BusinessRules.GetInstance();
            testOutputFrm.Show();

            if (InitializeTestData())
            {
                testOutputFrm.TxtTestData_Append(INITIALIZED_MSG);
                testOutputFrm.SetTestsObj(this);
            }          
        }//end Tests()


        /// <summary>
        /// This method is the single access point for the Test class.  It makes the method calls to run all tests. 
        /// </summary>
        public void RunNextTest()
        {
            const int PROCESS_RECS_MIN = 1, PROCESS_RECS_MAX = 11;
            const int DISP_EMPS_TEST = 12;
            const int INTEXING_TEST = 13;
            const int ADD_NULL_EMP_TEST = 14;
            const int REMOVE_EMP_TEST = 15;
            const int RESET_DICT_TEST = 16;
            string endToEndTestMsg = "---------------------------------------\r\nEND TO END TESTING\r\n---------------------------------------\r\n";
            string processingTestDataRecs = "ADDING EMPLOYEE RECORDS FROM FILE.\r\n(**ATTEMPT TO STORE EACH EMPLOYEE**)\r\n\r\n";
            string unitTestMsg = "---------------------\r\nUNIT TESTS\r\n---------------------\r\n";
            string allTestsCompleteMsg = "PHASE 1 TESTS SUCCESSFULLY COMPLETED!\r\n\r\n" 
                + "USE 'LOAD FROM FILE' AND 'SAVE TO FILE' BUTTONS TO TEST FileIO AND IFileAcess CLASSES\r\n\r\n"
                + "USE ADD COURSE TAB TO TEST LOOKUP AND COURSE FUNCTIONALITY";

            if (currentTest >= PROCESS_RECS_MIN && currentTest <= PROCESS_RECS_MAX)
            {
                if (currentTest == PROCESS_RECS_MIN)
                {
                    testOutputFrm.TxtTestData_Append(endToEndTestMsg);
                    testOutputFrm.TxtTestData_Append(processingTestDataRecs);
                }

                ProcessNextTDRec();

                if (currentTest == PROCESS_RECS_MAX) {testOutputFrm.TxtTestData_Append("\r\n\r\n"); }
            }

            switch (currentTest)
            {
                case (DISP_EMPS_TEST):
                    testOutputFrm.TxtTestData_Append(unitTestMsg);
                    DisplayStoredEmps();
                    break;

                case (INTEXING_TEST):
                    IndexerTests();
                    break;

                case (ADD_NULL_EMP_TEST):
                    AddNullEmpTest();
                    break;

                case (REMOVE_EMP_TEST):
                    RemoveEmpTest();
                    break;

                case (RESET_DICT_TEST):
                    ResetDictTest();
                    testOutputFrm.TxtTestData_Append(allTestsCompleteMsg);
                    testOutputFrm.BtnNext_Disable();
                    break;

                default:
                    break;
            }          

            currentTest++;

        }//end RunTests()


        /// <summary>
        /// Initialized the TestData class
        /// </summary>
        private bool InitializeTestData()
        {
            string testDataLoadFailed = "Error: Could not load testData.txt.\nIs testData.txt in your current working directory?";

            if (TestData.LoadTestData() == false)
            {
                testOutputFrm.TxtTestData_Append(testDataLoadFailed);
                testOutputFrm.BtnNext_Disable();
                //testAddEmpFrm.BtnTestData_Disable();
                return false;
            }
            else
            {
                //testAddEmpFrm.BtnTestData_Disable();
                return true;
            }

        }//end InitializeTestData()


        /// <summary>
        /// Runs all the records in the TestData class through the employee class.  
        /// </summary>
        private void ProcessNextTDRec()
        {
            TestData testObj = TestData.GetNextTestDataObj();
            
            testOutputFrm.TxtTestData_Append(testObj.TestInfo + "\r\n");
            //testAddEmpFrm.UpdateFields(testObj);
            testAddEmpFrm.Activate();

        }//end ProcessTestDataRecords()


        /// <summary>
        /// Displays all Employees contained in the BusinessRules dictionary
        /// </summary>
        private void DisplayStoredEmps()
        {
            string displayEmpRecords = "BUSINESS RULES CONTAINS:\r\n";
            string emptyMsg = "(Empty)";

            testOutputFrm.TxtTestData_Append(displayEmpRecords);

            if (testBusinessRules.empDictionary.Count() == 0)
            {
                testOutputFrm.TxtTestData_Append(emptyMsg + "\r\n\r\n");
                return;
            }
            
            foreach (KeyValuePair<uint, Employee> entry in testBusinessRules.empDictionary)
            {
                testOutputFrm.TxtTestData_Append(entry.Value.FirstName + " " + entry.Value.LastName + 
                    "(ID: " + entry.Value.EmpID + ")" + "\r\n");
            }

            testOutputFrm.TxtTestData_Append("\r\n\r\n");

        }//end DisplayStoredEmps()


        /// <summary>
        /// Tests the get and set indexer method in the BusinessRules class
        /// </summary>
        private void IndexerTests()
        {
            
            uint idForGetTest = 12345687;
            string addMidInitToFirstName = "Ryan J";
            string indexerTestMsg = "INDEXER GET AND SET OPERATIONS\r\n";
            string setTestMsg = "Setting BusinessRules.dictionary[" + idForGetTest + "].FirstName = Ryan J\r\n";
            string getTestMsg = "BusinessRules.dictionary[" + idForGetTest + "] returned: ";

            testOutputFrm.TxtTestData_Append(indexerTestMsg);
            testOutputFrm.TxtTestData_Append(getTestMsg);
            testOutputFrm.TxtTestData_Append(testBusinessRules.empDictionary[idForGetTest].FirstName + 
                " " + testBusinessRules.empDictionary[idForGetTest].LastName + "\r\n");
            testOutputFrm.TxtTestData_Append(setTestMsg);
            testBusinessRules.empDictionary[idForGetTest].FirstName = addMidInitToFirstName;
            testOutputFrm.TxtTestData_Append(getTestMsg);
            testOutputFrm.TxtTestData_Append(testBusinessRules.empDictionary[idForGetTest].FirstName +
                " " + testBusinessRules.empDictionary[idForGetTest].LastName + "\r\n");

            testOutputFrm.TxtTestData_Append("\r\n\r\n");

        }//end IndexerTests()


        /// <summary>
        /// Adds a valid employee ID with a null Employee Reference to the dictionary.  
        /// <remarks>The operations in this method should not cause an exception in BusinessRules, or any other class.</remarks>
        /// </summary>
        private void AddNullEmpTest()
        {
            uint addNullEmpTestID = 351;
            string addNullEmpTestMsg = "***ADD NULL EMPLOYEE TEST***\r\n";
            string addingNullEmpMsg = "Adding employee ID " + addNullEmpTestID + " with a null Employee reference\r\n";
            string noExceptionsThrownMsg = "No exceptions thrown.\r\n";
            Employee nullEmp = null;

            testOutputFrm.TxtTestData_Append(addNullEmpTestMsg);
            testOutputFrm.TxtTestData_Append(addingNullEmpMsg);
            testBusinessRules.empDictionary.AddValue(addNullEmpTestID, ref nullEmp);
            testOutputFrm.TxtTestData_Append(noExceptionsThrownMsg);

            testOutputFrm.TxtTestData_Append("\r\n\r\n");

        }//end AddNullEmpTest()


        /// <summary>
        /// Attempts to remove an invalid employee id, then a valid employee id
        /// </summary>
        private void RemoveEmpTest()
        {
            uint invalidEmpID = 123;
            uint validEmpID = 12345686;
            string removeEmpTestMsg = "REMOVE EMPLOYEE TEST\r\n";
            string invalidEmpIDMsg = "Attempting remove on ID 123 (invalid id)\r\n";
            string noExceptionMsg = "No exception thrown\r\n";
            string validEmpIDMsg = "Attempting remove on ID 12345686 (C. Hacker)\r\n";

            testOutputFrm.TxtTestData_Append(removeEmpTestMsg);
            testOutputFrm.TxtTestData_Append(invalidEmpIDMsg);
            testBusinessRules.empDictionary.RemoveValue(invalidEmpID);
            testOutputFrm.TxtTestData_Append(noExceptionMsg);
            testOutputFrm.TxtTestData_Append(validEmpIDMsg);
            testBusinessRules.empDictionary.RemoveValue(validEmpID);
            testOutputFrm.TxtTestData_Append("\r\n\r\n");
            DisplayStoredEmps();

        }//end RemoveEmpTest()


        /// <summary>
        /// Resets the dictionary and calls display
        /// </summary>
        private void ResetDictTest()
        {
            string resetDictTestMsg = "RESET DICTIONARY TEST\r\n";
            string afterResetMsg = "Dictionary has been reset\r\n";

            testOutputFrm.TxtTestData_Append(resetDictTestMsg);
            testBusinessRules.empDictionary.Reset();
            testOutputFrm.TxtTestData_Append(afterResetMsg);
            testOutputFrm.TxtTestData_Append("\r\n\r\n");
            DisplayStoredEmps();

        }//end ResetDictTest()

    }//end Tests class

}//end Lab_03 namespace
