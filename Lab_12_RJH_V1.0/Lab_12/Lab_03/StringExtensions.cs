﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Text.RegularExpressions;

namespace Lab_03
{
    /// <summary>
    /// This class contains String class extention methods
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// String class extension method.  Checks if string contains ONLY alpha characters.
        /// </summary>
        /// <param name="str">Any valid string object to check for digits</param>
        /// <returns>true, if string contains ONLY alpha character, else returns false</returns>
        public static bool ContainsOnlyAlpha(this String str)
        {
            string alphaOnly = @"^[A-Z]\w*";
            Regex reg = new Regex(alphaOnly);
            var matches = reg.Matches(str);
            string matchStr = String.Empty;

            str.Trim();

            foreach (Match m in matches)
            {
                matchStr += m;
            }

            if (matchStr.Length == str.Length && str.Length > 0)
            {
                return true;
            }

            return false;
        }//end ContainsOnlyAlpha(...)

        /// <summary>
        /// String class extension method.  Checks if string is in the valid form for a name.
        /// </summary>
        /// <param name="str">Any valid string object to check for digits</param>
        /// <returns>true, if string contains ONLY alpha character, else returns false</returns>
        public static bool IsValidName(this String str)
        {
            string namePattern = @"^[A-Z]\w*";
            Regex reg = new Regex(namePattern);

            str.Trim();

            return (reg.Match(str).Value.Length == str.Length);

        }//end ContainsOnlyAlpha(...)


        /// <summary>
        /// String class extension method.  Checks if string contains ONLY digit characters.
        /// </summary>
        /// <param name="str">Any valid string object to check for digits</param>
        /// <returns>true, if string contains at least ONLY digit characters, else returns false</returns>
        public static bool ContainsOnlyDigit(this String str)
        {
            string digitsOnly = @"^\d{1,3}(,{0,1}\d{3}){0,4}([.][0-9]{0,1}[0-9]){0,1}$|^[.][0-9]{0,1}[0-9]$";
            Regex reg = new Regex(digitsOnly);
            var matches = reg.Matches(str);
            string matchStr = String.Empty;

            str.Trim();

            foreach (Match m in matches)
            {
                matchStr += m;
            }

            if (matchStr.Length == str.Length && str.Length > 0)
            {
                return true;
            }

            return false;
        }//end ContainsOnlyDigit(...)


        /// <summary>
        /// Validates a course id string
        /// </summary>
        /// <param name="str">any valid string</param>
        /// <returns>true, if str is valid, else false</returns>
        public static bool ValidateCourseID(this String str)
        {
            string courseIdMatch = @"^[a-zA-Z]{2,4}\d{4}$";
            Regex reg = new Regex(courseIdMatch);
            var matches = reg.Matches(str);
            string matchStr = String.Empty;

            str.Trim();

            foreach (Match m in matches)
            {
                matchStr += m;
            }

            if (matchStr.Length == str.Length && str.Length > 0)
            {
                return true;
            }

            return false;

        }//end ValidateCourseID(...)


        /// <summary>
        /// Verifies initial length is either 0 or 1, and is an alpha character
        /// </summary>
        /// <param name="str">the string to check</param>
        /// <returns>true if string is a valid initial, else returns false</returns>
        public static bool IsValidInitial(this String str)
        {
            string midInitialPattern = @"^[A-Z]$";
            Regex reg = new Regex(midInitialPattern);
            string matchStr;

            str.Trim();
            matchStr = reg.Match(str).Value;
            return (matchStr.Length == str.Length);

        }//end IsValidInitial(...)


        public static bool IsValidDepartmentName(this String str)
        {
            string departmentPattern = "^([A-Z][a-z]*\\s*){1,}";
            Regex reg = new Regex(departmentPattern);

            str.Trim();

            return (reg.Match(str).Value.Length == str.Length);
        }

    }//end StringExtensions class

}//end Lab_03 namespace
