﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_08
// Date: 1/24/2016 19:40:25 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------



using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Lab_03
{
    /// <summary>
    /// This class manages a form for looking up employees
    /// </summary>
    public partial class FrmLookup : Form
    {
        //Members
        BusinessRules businessRules = BusinessRules.GetInstance();

        /// <summary>
        /// Initializes the form
        /// </summary>
        public FrmLookup()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Makes call to populate LBxLkupAllEmps on form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLookup_Load(object sender, EventArgs e)
        {
            LoadAllEmpsList();

        }//end FrmLookup_Load(...)


        /// <summary>
        /// Finds an employee record by ID
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void BtnFindByID_Click(object sender, EventArgs e)
        {
            string noMatchFoundMsg = "(No match found)";
            String badIDError = "Invalid ID";
            uint tempUint = 0;

            if (uint.TryParse(TxtFindByID.Text, out tempUint))
            {
                if (UpdateLookupInfo(businessRules.empDictionary.GetValue(tempUint)) == false)
                {
                    TxtFindByID.Text += noMatchFoundMsg;
                }
            }
            else
            {
                TxtFindByID.Text = badIDError;
            }

        }//end BtnFindbyID_Click(...)

        /// <summary>
        /// Finds all employees with a given last name
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void BtnFindByLastName_Click(object sender, EventArgs e)
        {
            string idStr = "ID: ";
            string commaStr = ", ";
            String badLastNameError = "Invalid Last Name";
            string noMatchFoundMsg = " (No match found)";
            uint matches = 0;

            LbxMatchingRecords.Items.Clear();

            if (TxtFindByLastName.Text == String.Empty)
            {
                TxtFindByLastName.Text = badLastNameError;
                return;
            }

            foreach (KeyValuePair<uint, Employee> entry in businessRules.empDictionary)
            {
                if (entry.Value.LastName.ToLower() == TxtFindByLastName.Text.ToLower())
                {
                    LbxMatchingRecords.Items.Add(idStr + entry.Value.EmpID + " " + commaStr + entry.Value.FirstName + 
                        " " + entry.Value.LastName);
                    matches++;
                }
            }

            if (matches <= 0)
            {
                TxtFindByLastName.Text += noMatchFoundMsg;
            }

        }//end BtnFindByLastName_Click(...)


        /// <summary>
        /// Handles item selections from within the list box
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void LbxMatchingRecords_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int listIndex = 0;
            string idStr = String.Empty;
            int idIdx = 1;
            uint tempId = 0;

            listIndex = LbxMatchingRecords.IndexFromPoint(e.Location);

            if (listIndex != ListBox.NoMatches)
            {
                idStr = LbxMatchingRecords.Items[listIndex].ToString().Split()[idIdx];

                if (uint.TryParse(idStr, out tempId))
                {
                    UpdateLookupInfo(businessRules.empDictionary[tempId]);
                }
            }

        }//end LVwMatches_SelectedIndexChanged(...)

        /// <summary>
        /// Populates the employee information
        /// </summary>
        /// <param name="emp">a valid Employee object or null</param>
        /// <returns>true, if employee info is in business rules dictionary, else returns false</returns>
        private bool UpdateLookupInfo(Employee emp)
        {
            const string notApplicable = "(N/A)";
            string dateTimeStr;
            const int salaryIdx = 0, salesIdx = 1, hourlyIdx = 2, contractIdx = 3;
            const int singleIdx = 0, marriedIdx = 1;
            const int isCurrentTrueIdx = 0, isCurrentFalseIdx = 1;
            const int fullTimeIdx = 0, partTimeIdx = 1;
            string[] dateTimeArr;

            if (emp == null) { return false; }

            dateTimeStr = emp.StartDate.Value.ToString();//emp must note be null to perform this initialization
            dateTimeArr = dateTimeStr.Split();//emp must note be null to perform this initialization

            //populate fields
            switch (emp.EmpType.ToString().ToLower())
            {
                case (ConstantsClass.SALARY):
                    CBxLkupEmpType.SelectedIndex = salaryIdx;

                    TxtLkupMonthlySalary.Text = (emp as Salary).MonthlySalary.ToString();
                    TxtLkupHourlyRate.Text = notApplicable;
                    TxtLkupContractWage.Text = notApplicable;

                    break;

                case (ConstantsClass.SALES):
                    CBxLkupEmpType.SelectedIndex = salesIdx;

                    TxtLkupMonthlySalary.Text = (emp as Sales).MonthlySalary.ToString();
                    TxtLkupHourlyRate.Text = notApplicable;
                    TxtLkupContractWage.Text = notApplicable;

                    break;

                case (ConstantsClass.HOURLY):
                    CBxLkupEmpType.SelectedIndex = hourlyIdx;

                    TxtLkupMonthlySalary.Text = notApplicable;
                    TxtLkupHourlyRate.Text = (emp as Hourly).HourlyRate.ToString();
                    TxtLkupContractWage.Text = notApplicable;

                    break;

                case (ConstantsClass.CONTRACT):
                    CBxLkupEmpType.SelectedIndex = contractIdx;

                    TxtLkupMonthlySalary.Text = notApplicable;
                    TxtLkupHourlyRate.Text = notApplicable;
                    TxtLkupContractWage.Text = (emp as Contract).ContractWage.ToString();

                    TxtLkupAgency.Text = (emp as Contract).Agency.ToString();

                    break;

                default:
                    break;      

            }

            TxtLkupEmpId.Text = emp.EmpID.ToString();
            TxtLkupFirstName.Text = emp.FirstName.ToString();
            TxtLkupLastName.Text = emp.LastName.ToString();
            TxtLkupMiddleInitial.Text = emp.MiddleInitial.ToString();

            switch (emp.IsMarried.ToString().ToLower())
            {
                case (ConstantsClass.SINGLE_LOWER_STR):
                    CBxLkupMarritalStatus.SelectedIndex = singleIdx;
                    break;
                case (ConstantsClass.MARRIED_LOWER_STR):
                    CBxLkupMarritalStatus.SelectedIndex = marriedIdx;
                    break;
                default:
                    break;
            }

            TxtLkupDepartment.Text = emp.Department.ToString();
            TxtLkupTitle.Text = emp.Title.ToString();
            DTpLkupStartDate.Value = emp.StartDate.Value;

            switch (emp.IsCurrentEmp.ToString().ToLower())
            {
                case (ConstantsClass.TRUE_LOWER_STR):
                    CBxLkupIsCurrent.SelectedIndex = isCurrentTrueIdx;
                    break;
                case (ConstantsClass.FALSE_LOWER_STR):
                    CBxLkupIsCurrent.SelectedIndex = isCurrentFalseIdx;
                    break;
                default:
                    break;
            }
            
            switch (emp.EmployeeStatus.ToString().ToLower())
            {
                case (ConstantsClass.FULL_TIME_LOWER_STR):
                    CBxLkupFullPartTime.SelectedIndex = fullTimeIdx;
                    break;
                case (ConstantsClass.PART_TIME_LOWER_STR):
                    CBxLkupFullPartTime.SelectedIndex = partTimeIdx;
                    break;
                default:
                    break;
            }

            return true;
        }//end UpdateLookupInfo(...)

        /// <summary>
        /// Populates LBxLkupAllEmps with all employees
        /// </summary>
        private void LoadAllEmpsList()
        {
            string idMsg = "ID: ";
            Employee tempEmp = null;

            LBxLkupAllEmps.Items.Clear();

            foreach (KeyValuePair<uint, Employee> entry in businessRules.empDictionary)
            {
                tempEmp = entry.Value;

                if (tempEmp.IsCurrentEmp == true)
                {
                    LBxLkupAllEmps.Items.Add(tempEmp.FirstName + " " + tempEmp.MiddleInitial + " " + tempEmp.LastName
                    + "   " + idMsg + tempEmp.EmpID);
                }                
            }

        }//end LoadAllEmpsList()


        /// <summary>
        /// Makes method call to update the list of all employees
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void BtnLkupRefresh_Click(object sender, EventArgs e)
        {
            LoadAllEmpsList();

        }//end BtnLkupRefresh_Click(...)
        

        /// <summary>
        /// Performs basic data validation on form data, and updates employee info, or creates a new employee if employee type changed.
        /// </summary>
        private void BtnLkupSaveChanges_Click(object sender, EventArgs e)
        {
            uint tempID = 0;
            Employee tempEmp = null;
            EType tempEmpType = EType.NOT_SET_BAD;
            string tempFirst = string.Empty;
            string tempLast = string.Empty;
            string tempMiddle = string.Empty;
            ConstantsClass.MarritalStatus tempMarritalStatus = ConstantsClass.MarritalStatus.NOT_SET;
            string tempDepartment = string.Empty;
            string tempTitle = string.Empty;
            DateTime? tempStartDate = null;
            bool tempIsCurrent = false;
            ConstantsClass.EmpStatus tempEmpStatus = ConstantsClass.EmpStatus.NOT_SET;
            string tempAgency = string.Empty;
            decimal tempMonthlySal = 0;
            decimal tempHourly = 0;
            decimal tempContractWage = 0;
            decimal tempDecimal = 0;
            bool invalidData = false;
            const string unknownInvalidInformation = "Unknown invalid information entered.  Record was not updated.";
            bool eTypeChanged = false;

            ResetFields();

            switch (CBxLkupEmpType.Items[CBxLkupEmpType.SelectedIndex].ToString().ToLower())
            {
                case (ConstantsClass.CONTRACT):
                    tempEmpType = EType.CONTRACT;
                    break;
                case (ConstantsClass.SALARY):
                    tempEmpType = EType.SALARY;
                    TxtLkupAgency.Text = string.Empty;
                    break;
                case (ConstantsClass.SALES):
                    tempEmpType = EType.SALES;
                    TxtLkupAgency.Text = string.Empty;
                    break;
                case (ConstantsClass.HOURLY):
                    tempEmpType = EType.HOURLY;
                    TxtLkupAgency.Text = string.Empty;
                    break;
                default:
                    tempEmpType = EType.NOT_SET_BAD;
                    LblLkupEmpType.BackColor = ConstantsClass.missingDataBackgroundColor;
                    invalidData = true;
                    break;
            }

            uint.TryParse(TxtLkupEmpId.Text, out tempID);//read only field.  this should always work;
            if (tempID == 0)
            {
                MessageBox.Show(unknownInvalidInformation);
                invalidData = true;
            }

            if (TxtLkupFirstName.Text.IsValidName())
            {
                tempFirst = TxtLkupFirstName.Text;
            }
            else
            {
                invalidData = true;
                TxtLkupFirstName.BackColor = ConstantsClass.missingDataBackgroundColor;
            }

            if (TxtLkupLastName.Text.IsValidName())
            {
                tempLast = TxtLkupLastName.Text;
            }
            else
            {
                invalidData = true;
                TxtLkupLastName.BackColor = ConstantsClass.missingDataBackgroundColor;
            }

            if (TxtLkupMiddleInitial.Text.IsValidInitial())
            {
                tempMiddle = TxtLkupMiddleInitial.Text;
            }
            else
            {
                invalidData = true;
                TxtLkupMiddleInitial.BackColor = ConstantsClass.missingDataBackgroundColor;
            }

            if (CBxLkupMarritalStatus.SelectedIndex >= 0)
            {
                if (CBxLkupMarritalStatus.Items[CBxLkupMarritalStatus.SelectedIndex].ToString().ToLower() ==
                    ConstantsClass.MarritalStatus.MARRIED.ToString().ToLower())
                {
                    tempMarritalStatus = ConstantsClass.MarritalStatus.MARRIED;
                }
                else if (CBxLkupMarritalStatus.Items[CBxLkupMarritalStatus.SelectedIndex].ToString().ToLower() ==
                    ConstantsClass.MarritalStatus.SINGLE.ToString().ToLower())
                {
                    tempMarritalStatus = ConstantsClass.MarritalStatus.SINGLE;
                }
                else
                {
                    LblLkupIsMarried.BackColor = ConstantsClass.missingDataBackgroundColor;
                    invalidData = true;
                }
            }

            if (TxtLkupDepartment.Text.ContainsOnlyAlpha())
            {
                tempDepartment = TxtLkupDepartment.Text;
            }
            else
            {
                TxtLkupDepartment.BackColor = ConstantsClass.missingDataBackgroundColor;
                invalidData = true;
            }

            if (TxtLkupTitle.Text.Length > 0)
            {
                tempTitle = TxtLkupTitle.Text;
            }
            else
            {
                TxtLkupTitle.BackColor = ConstantsClass.missingDataBackgroundColor;
                invalidData = true;
            }

            if (DTpLkupStartDate.Value.Date > ConstantsClass.ONE_WEEK_AGO)
            {
                tempStartDate = DTpLkupStartDate.Value.Date;
            }
            else
            {
                MessageBox.Show(ConstantsClass.INVALID_START_DATE_MSG);
                invalidData = true;
            }

            if (CBxLkupIsCurrent.Items[CBxLkupIsCurrent.SelectedIndex].ToString() ==ConstantsClass.TRUE_STR)
            {
                tempIsCurrent = true;
            }
            else if (CBxLkupIsCurrent.Items[CBxLkupIsCurrent.SelectedIndex].ToString() == ConstantsClass.FALSE_STR)
            {
                tempIsCurrent = false;
            }
            else
            {
                LblLkupIsCurrent.BackColor = ConstantsClass.missingDataBackgroundColor;
                invalidData = true;
            }

            if (CBxLkupFullPartTime.Items[CBxLkupFullPartTime.SelectedIndex].ToString().ToLower() == ConstantsClass.COMBOBOX_FULL_TIME_LOWER_STR)
            {
                tempEmpStatus = ConstantsClass.EmpStatus.FULL_TIME;
            }
            else if (CBxLkupFullPartTime.Items[CBxLkupFullPartTime.SelectedIndex].ToString().ToLower() == ConstantsClass.COMBOBOX_PART_TIME_LOWER_STR)
            {
                tempEmpStatus = ConstantsClass.EmpStatus.PART_TIME;
            }
            else
            {
                tempEmpStatus = ConstantsClass.EmpStatus.NOT_SET;
                LblLkupFullPartTime.BackColor = ConstantsClass.missingDataBackgroundColor;
                invalidData = true;
            }

            if (tempEmpType == EType.CONTRACT)
            {
                if (TxtLkupAgency.Text.Length > 0)
                {
                    tempAgency = TxtLkupAgency.Text;
                }
                else
                {
                    TxtLkupAgency.BackColor = ConstantsClass.missingDataBackgroundColor;
                    invalidData = true;
                }

                if (Decimal.TryParse(TxtLkupContractWage.Text, out tempDecimal) && TxtLkupContractWage.Text.ContainsOnlyDigit())
                {
                    tempContractWage = tempDecimal;
                    tempDecimal = 0;
                }
                else
                {
                    TxtLkupContractWage.BackColor = ConstantsClass.missingDataBackgroundColor;
                    invalidData = true;
                }
            }

            if (tempEmpType == EType.SALARY || tempEmpType == EType.SALES)
            {
                if (Decimal.TryParse(TxtLkupMonthlySalary.Text, out tempDecimal) && TxtLkupMonthlySalary.Text.ContainsOnlyDigit())
                {
                    tempMonthlySal = tempDecimal;
                    tempDecimal = 0;
                }
                else
                {
                    TxtLkupMonthlySalary.BackColor = ConstantsClass.missingDataBackgroundColor;
                    invalidData = true;
                }
            }

            if (tempEmpType == EType.HOURLY)
            {
                if (Decimal.TryParse(TxtLkupHourlyRate.Text, out tempDecimal) && TxtLkupHourlyRate.Text.ContainsOnlyDigit())
                {
                    tempHourly = tempDecimal;
                    tempDecimal = 0;
                }
                else
                {
                    TxtLkupHourlyRate.BackColor = ConstantsClass.missingDataBackgroundColor;
                    invalidData = true;
                }
            }

            if (invalidData == false)
            {
                tempEmp = businessRules.empDictionary.GetValue(tempID);

                if (tempEmp == null)
                {
                    MessageBox.Show(unknownInvalidInformation);
                    return;
                }

                if (tempEmp.EmpType != tempEmpType)
                {
                    eTypeChanged = true;
                }

                
                if (eTypeChanged == false)//employee type didn't change, so just update info.
                {
                    tempEmp.FirstName = tempFirst;
                    tempEmp.LastName = tempLast;
                    tempEmp.MiddleInitial = tempMiddle;
                    tempEmp.IsMarried = tempMarritalStatus;
                    tempEmp.Department = tempDepartment;
                    tempEmp.Title = tempTitle;
                    tempEmp.StartDate = tempStartDate;
                    tempEmp.IsCurrentEmp = tempIsCurrent;
                    tempEmp.EmployeeStatus = tempEmpStatus;

                    switch (tempEmp.EmpType.ToString().ToLower())
                    {
                        case (ConstantsClass.CONTRACT):
                            (tempEmp as Contract).ContractWage = tempContractWage;
                            (tempEmp as Contract).Agency = tempAgency;
                            break;
                        case (ConstantsClass.SALARY):
                            (tempEmp as Salary).MonthlySalary = tempMonthlySal;
                            break;
                        case (ConstantsClass.SALES):
                            (tempEmp as Sales).MonthlySalary = tempMonthlySal;
                            break;
                        case (ConstantsClass.HOURLY):
                            (tempEmp as Hourly).HourlyRate = tempHourly;
                            break;
                        default:
                            MessageBox.Show(unknownInvalidInformation);
                            break;
                    }

                }
                else//employee type changed, create new employee
                {
                    businessRules.empDictionary.RemoveValue(tempID);//remove record from dictionary.  this WON'T remove record from DB.

                    switch (tempEmpType.ToString().ToLower())
                    {
                        case (ConstantsClass.CONTRACT):
                            tempEmp = new Contract(tempID, tempFirst, tempLast, tempMiddle, tempEmpType, tempMarritalStatus,
                                tempDepartment, tempTitle, tempEmpStatus, tempStartDate, tempContractWage, tempAgency, tempIsCurrent);
                            businessRules.empDictionary.AddValue(tempEmp.EmpID, ref tempEmp);
                            break;
                        case (ConstantsClass.SALARY):
                            tempEmp = new Salary(tempID, tempFirst, tempLast, tempMiddle, tempEmpType, tempMarritalStatus,
                                tempDepartment, tempTitle, tempEmpStatus, tempStartDate, tempMonthlySal, tempIsCurrent);
                            businessRules.empDictionary.AddValue(tempEmp.EmpID, ref tempEmp);
                            break;
                        case (ConstantsClass.SALES):
                            tempEmp = new Sales(tempID, tempFirst, tempLast, tempMiddle, tempEmpType, tempMarritalStatus,
                                tempDepartment, tempTitle, tempEmpStatus, tempStartDate, tempMonthlySal, tempIsCurrent);
                            businessRules.empDictionary.AddValue(tempEmp.EmpID, ref tempEmp);
                            break;
                        case (ConstantsClass.HOURLY):
                            tempEmp = new Hourly(tempID, tempFirst, tempLast, tempMiddle, tempEmpType, tempMarritalStatus,
                                tempDepartment, tempTitle, tempEmpStatus, tempStartDate, tempHourly, tempIsCurrent);
                            businessRules.empDictionary.AddValue(tempEmp.EmpID, ref tempEmp);
                            break;
                        default:
                            MessageBox.Show(unknownInvalidInformation);
                            break;

                    }//end switch

                }//end else

                UpdateLookupInfo(tempEmp);

            }//end if (invalidData == false)

        }//end BtnLkupSaveChanges_Click(...)

        
        /// <summary>
        /// Resets the background colors for all the text boxes in this form to the default color
        /// </summary>
        private void ResetFields()
        {
            LblLkupEmpType.BackColor = DefaultBackColor;
            TxtLkupEmpId.BackColor = ConstantsClass.defaultBackColor;
            TxtLkupFirstName.BackColor = ConstantsClass.defaultBackColor;
            TxtLkupLastName.BackColor = ConstantsClass.defaultBackColor;
            TxtLkupMiddleInitial.BackColor = ConstantsClass.defaultBackColor;
            LblLkupIsMarried.BackColor = DefaultBackColor;
            TxtLkupDepartment.BackColor = ConstantsClass.defaultBackColor;
            TxtLkupTitle.BackColor = ConstantsClass.defaultBackColor;
            LblLkupStartDate.BackColor = DefaultBackColor;
            LblLkupIsCurrent.BackColor = DefaultBackColor;
            LblLkupFullPartTime.BackColor = DefaultBackColor;
            TxtLkupAgency.BackColor = ConstantsClass.defaultBackColor;
            TxtLkupMonthlySalary.BackColor = ConstantsClass.defaultBackColor;
            TxtLkupHourlyRate.BackColor = ConstantsClass.defaultBackColor;
            TxtLkupContractWage.BackColor = ConstantsClass.defaultBackColor;

        }//end ResetFields()
        

        /// <summary>
        /// Makes employee status "Non-Current"
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void BtnLkupDeleteEmp_Click(object sender, EventArgs e)
        {
            uint tempId = 0;
            const String couldNotDeleteMsg = "Invalid Employee ID.  Could not delete record.";
            Employee tempEmp = null;
            
            if (uint.TryParse(TxtLkupEmpId.Text, out tempId))
            {
                if (businessRules.empDictionary.Contains(tempId))
                {
                    tempEmp = businessRules.empDictionary.GetValue(tempId);
                    tempEmp.IsCurrentEmp = false;
                    return;
                }
            }

            MessageBox.Show(couldNotDeleteMsg);

        }//end BtnLkupDeleteEmp_Click(...)

    }//end FrmLookup class

}//end Lab_03 namespace
