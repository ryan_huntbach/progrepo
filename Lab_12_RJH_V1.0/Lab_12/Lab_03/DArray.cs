﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 2/04/2016 14:09:10 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections;

namespace Lab_03
{
    /// <summary>
    /// This is a Singleton class, for a dynamic array.  Implements IEnumerable and IEnumerator
    /// </summary>
    public sealed class DArray : IEnumerable, IEnumerator
    {
        //Constant Members
        const int INITIAL_CAPACITY = 10;
        const int DOUBLE_CAPACITY = 2;

        //Members
        private int _AIndex = 0;
        private Employee[] _RArray = new Employee[INITIAL_CAPACITY];
        private Employee[] _TempArray = null;
        private int _Capacity = INITIAL_CAPACITY;
        private int _Top = 0;

        private static volatile DArray instance = null;
        private static Object syncLock = new object();

        //Properties
        public object Current { get; set; } = null;



        /// <summary>
        /// Non-Parameterized Constructor.  DArray is a Singleton, so the constructor is inaccessable from outside. 
        /// </summary>
        private DArray() { }//end DArray()


        /// <summary>
        /// Adds the Employee reference to the array
        /// <remarks>If _RArray does not have the capacity to add another element, the array size is doubled prior to 
        /// adding the new element. </remarks>
        /// </summary>
        public void AddValue(ref Employee emp)
        {
            if (!MoveNext())
            {
                Resize(_Capacity * DOUBLE_CAPACITY);
            }
            
            _RArray[_Top] = emp;
            Current = _RArray[_Top];
            _Top++;
            
        }//end AddValue(...)


        /// <summary>
        /// Retrieves the instance of DArray.  
        /// <remarks>
        /// - This method implements the Singleton pattern.  
        /// - Method is thread safe. 
        /// </remarks>
        /// </summary>
        /// <returns>If no instance of DArray has been created, a new instance is returned, else the 
        /// pre-existing single instance of DArray is returned. </returns>
        public static DArray GetInstance()
        {
            if (instance == null)
            {
                lock (syncLock)
                {
                    if (instance == null)
                    {
                        instance = new DArray();
                    }
                }
            }

            return instance;

        }//end GetInstance()


        /// <summary>
        /// Indexer for _RArray
        /// <remarks>Indexer performs basic validation before getting/setting, to avoid exceptions. </remarks>
        /// </summary>
        /// <param name="index">Any valid integer </param>
        /// <returns>The Employee object at "index" </returns>
        public Employee this[int index]
        {
            get
            {
                if (index < _Capacity)
                {
                    return _RArray[index];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (index < _Capacity)
                {
                    _RArray[index] = value;
                }
            }
        }//end Indexer definition


        /// <summary>
        /// Resets the array to a new, empty, array of INITIAL_CAPACITY
        /// </summary>
        public void Reset()
        {
            _RArray = null;
            _RArray = new Employee[INITIAL_CAPACITY];

            _Capacity = INITIAL_CAPACITY;
            _Top = 0;
            Current = null;
        }//end Reset()


        /// <summary>
        /// Resizes the array.  
        /// <remarks>If the new size is less than the current size, elements after the new size are discarded.</remarks>
        /// </summary>
        /// <param name="newSize">any valid integer</param>
        public Employee[] Resize(int newSize)
        {
            _TempArray = new Employee[newSize];

            for (_AIndex = 0; _AIndex < newSize && _AIndex < _RArray.Length; _AIndex++)
            {
                _TempArray[_AIndex] = _RArray[_AIndex];
            }

            _Top = _AIndex;
            _Capacity = _TempArray.Length;
            Current = _TempArray[_Top - 1];

            _RArray = null;
            _RArray = _TempArray;

            return _RArray;

        }//end Resize(...)


        /// <summary>
        /// Returns the value at the specified index
        /// <remarks>Performs basic validation before attempting to get the value at the specified index, in 
        /// order to avoid exceptions.</remarks>
        /// </summary>
        /// <param name="index">any valid integer</param>
        /// <returns>The employee object at the specified index, if the index contains a valid employee object, else returns null. </returns>
        public Employee GetValue(int index)
        {
            if (index < _RArray.Length)
            {
                return _RArray[index];
            }
            else
            {
                return null;
            }
        }//end GetValue(...)


        /// <summary>
        /// Gets an IEnumberator for _RArray
        /// </summary>
        /// <returns>an IEnumerator object for _RArray.</returns>
        public IEnumerator GetEnumerator()
        {
            for(_AIndex = 0; _AIndex < _Top; _AIndex++)
            {
                yield return _RArray[_AIndex];
            }
        }//end GetEnumerator()


        /// <summary>
        /// 
        /// </summary>
        /// <returns>true, if the next index is valid (does not exceed array size), else returns false</returns>
        public bool MoveNext()
        {
            return (_Top < _RArray.Length);
        }//end MoveNext()

        /// <summary>
        /// Returns the number of initialized (non-null) elements in the array
        /// </summary>
        /// <returns>int, the number of initialized (non-null) elements in the array. </returns>
        public int Size()
        {
            return _Top;
        }//end Size()

    }//end DArray class

}//end Lab_03 namespace
