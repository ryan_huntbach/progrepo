﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------



using System;
using System.Drawing;

namespace Lab_03
{
    /// <summary>
    /// This class holds all constant, enums, and broadly-scoped variables for the Employee management system
    /// </summary>
    public static class ConstantsClass
    {
        /// <summary>
        /// Models marriage status options
        /// </summary>
        public enum MarritalStatus { MARRIED, SINGLE, NOT_SET };

        /// <summary>
        /// Models the status options for an employee (e.g. Full Time, Part Time, etc...)
        /// </summary>
        public enum EmpStatus { FULL_TIME, PART_TIME, NOT_SET };

        //Names of all the variables in the Employee class.  Used for serialization and de-serialization.
        public const String EMP_ID_STR = "EmpID",
            FIRST_NAME_STR = "FirstName",
            LAST_NAME_STR = "LastName",
            EMP_TYPE_STR = "EmpType",
            HOURLY_WAGE_STR = "HourlyWage",
            MONTHLY_SALARY_STR = "MonthlySalary",
            CONTRACT_WAGE_STR = "ContractWage",
            MIDDLE_INITIAL_STR = "MiddleInitial",
            IS_MARRIED_STR = "IsMarried",
            DEPARTMENT_STR = "Department",
            TITLE_STR = "Title",
            START_DATE_STR = "StartDate",
            BENEFITS_STR = "Benefits",
            COURSES_TAKEN_STR = "CoursesTaken",
            COURSES_APPROVED_STR = "CoursesApproved",
            AGENCY_STR = "Agency",
            HOURS_WORKED_STR = "HoursWorked",
            COMMISSION_STR = "Commission",
            GROSS_SALES_STR = "Sales",
            EMPLOYEE_COURSES_STR = "EmployeeCourses",
            COURSE_ID_STR = "CourseID",
            COURSE_DESCRIPTION_STR = "CourseDesc",
            COURSE_GRADE_STR = "CourseGrade",
            APPROVED_DATE_STR = "ApprovedDate",
            COURSE_CREDITS_STR = "CourseCredits",
            IS_CURRENT_EMP_STR = "IsCurrentEmp",
            EMPLOYEE_STATUS_STR = "EmployeeStatus";

        public const double SIX_DAYS = 6.0;
        public static DateTime ONE_WEEK_AGO = DateTime.Today.AddDays(-SIX_DAYS);
        public static String INVALID_START_DATE_MSG = "Start date must be after " + ONE_WEEK_AGO.Date.ToString();
        public const String NO_COURSES_MSG = "This employee is taking no courses";
        public const String CONT_EMP_EDU_MSG = "Note: Contract employees do not receive educational benefits.";
        public const String BAD_COURSE_APPROVAL_DATE_MSG = "Course approval date cannot be in the future.";
        public const string CONTRACT = "contract", SALARY = "salary", SALES = "sales", HOURLY = "hourly";
        public const string TRUE_STR = "TRUE";
        public const string FALSE_STR = "FALSE";
        public const string TRUE_LOWER_STR = "true";
        public const string FALSE_LOWER_STR = "false";
        public const string FULL_TIME_LOWER_STR = "full_time";
        public const string PART_TIME_LOWER_STR = "part_time";
        public const string COMBOBOX_FULL_TIME_LOWER_STR = "full time";
        public const string COMBOBOX_PART_TIME_LOWER_STR = "part time";
        public const string SINGLE_LOWER_STR = "single";
        public const string MARRIED_LOWER_STR = "married";

        public static Color missingDataBackgroundColor = Color.LightCoral;
        public static Color errorTextColor = Color.Red;
        public static Color defaultBackColor = Color.White;
        public static Color successColor = Color.Green;

    }//end ConstantsClass class

}//end Lab_03 namespace

