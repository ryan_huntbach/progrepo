﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace Lab_03
{
    /// <summary>
    /// Class contains event handling methods for FrmAddEmp
    /// </summary>
    public partial class FrmEmp : Form
    {
        //Constants
        private const string MISSING_FIELDS_MSG = "*Required information";
        private const string MISSING_EMP_TYPE_MSG = "*Select Employee Type";
        private const string SUCCESSFULLY_CREATED_EMP_MSG = "Successfully created record for: ";
        private const int ID_LEN = 5;

        //Locals
        
        private BusinessRules businessRules = BusinessRules.GetInstance();
        private FileIO fileObj = new FileIO();

        /// <summary>
        /// The FrmAddEmp constructor
        /// </summary>
        public FrmEmp()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles events for ALL Employee Type radio buttons in FrmAddEmp
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">contains event information</param>
        private void RBtnEmpType_CheckedChanged(object sender, EventArgs e)
        {
            var btnChecked = ((RadioButton)sender).Name;

            if (btnChecked == RBtnSalaryEmp.Name)
            {
                EmpObjFactory.EmpType = EType.SALARY;
            }
            else if(btnChecked == RBtnSalesEmp.Name)
            {
                EmpObjFactory.EmpType = EType.SALES;
            }
            else if (btnChecked == RBtnHourlyEmp.Name)
            {
                EmpObjFactory.EmpType = EType.HOURLY;
            }
            else if (btnChecked == RBtnContractEmp.Name)
            {
                EmpObjFactory.EmpType = EType.CONTRACT;
            }
            else { EmpObjFactory.EmpType = EType.NOT_SET_BAD; }
        }//end RBtnEmpType_CheckedChanged(...)

        /// <summary>
        /// Communicates with the factory class (EmpObjFactory), with the help of other methods in this class,
        /// to verify required data has been gathered, and to create the new employee object.  Notifies 
        /// user if required data is missing. 
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">contains event information</param>
        private void BtnStoreEmpData_Click(object sender, EventArgs e)
        {
            //Locals
            List<string> missingData = null;

            ResetTxtBackgroundColors();
            SendFrmDataToFactory();
            missingData = EmpObjFactory.VerifyHaveReqData();

            //Was required data missing?
            if (missingData.Count == 0)
            {
                //NO
                Employee newEmp = EmpObjFactory.BuildEmpObj();

                businessRules.empDictionary.AddValue(newEmp.EmpID, ref newEmp);

                LblSuccessOrFailureMsg.ForeColor = ConstantsClass.successColor;
                LblSuccessOrFailureMsg.Text = SUCCESSFULLY_CREATED_EMP_MSG + TxtFirstName.Text + " " + TxtLastName.Text;

                ClearFrm();
            }
            else if (EmpObjFactory.EmpType == EType.NOT_SET_BAD)
            {
                //Yes. Missing employee type.
                LblSuccessOrFailureMsg.ForeColor = ConstantsClass.errorTextColor;
                LblSuccessOrFailureMsg.Text = MISSING_EMP_TYPE_MSG;
            }
            else
            {
                //YES.  Missing employee data (ID, Name, etc...).

                if (missingData.Contains(EmpDataNames.ID.ToString()))
                {
                    TxtEmpID.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.FIRST_NAME.ToString()))
                {
                    TxtFirstName.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.LAST_NAME.ToString()))
                {
                    TxtLastName.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.MIDDLE_INITIAL.ToString()))
                {
                    TxtMiddleInitial.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.IS_MARRIED.ToString()))
                {
                    CBxMaritalStatus.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.DEPARTMENT.ToString()))
                {
                    TxtDepartment.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.TITLE.ToString()))
                {
                    TxtTitle.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.START_DATE.ToString()))
                {
                    LblStartDate.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.MONTHLY_SALARY.ToString()))
                {
                    TxtMonthlySalary.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.HOURLY_RATE.ToString()))
                {
                    TxtHourlyRate.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.CONTRACT_WAGE.ToString()))
                {
                    TxtContractWage.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.AGENCY.ToString()))
                {
                    TxtAgency.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                if (missingData.Contains(EmpDataNames.EMPLOYEE_STATUS.ToString()))
                {
                    ChBxFullTime.BackColor = ConstantsClass.missingDataBackgroundColor;
                    ChBxPartTime.BackColor = ConstantsClass.missingDataBackgroundColor;
                }

                LblSuccessOrFailureMsg.ForeColor = ConstantsClass.errorTextColor;
                LblSuccessOrFailureMsg.Text = MISSING_FIELDS_MSG;
            }

            EmpObjFactory.ResetAllButEmpType();//EmpObjFactory is static.  must reset for next run.

        }//end BtnStoreEmpData_Click(...)

        /// <summary>
        /// Performs basic data validation on form data, and sends the valid data to the factory
        /// </summary>
        private void SendFrmDataToFactory()
        {
            uint tempUInt = 0;
            decimal tempDecimal = 0;

            if (TxtEmpID.Text.ContainsOnlyDigit() && TxtEmpID.Text.Length == ID_LEN)
            {
                uint.TryParse(TxtEmpID.Text, out tempUInt);
                EmpObjFactory.EmpID = tempUInt;
                tempUInt = 0;
            }

            if (TxtFirstName.Text.IsValidName())
            {
                EmpObjFactory.FirstName = TxtFirstName.Text;
            }

            if (TxtLastName.Text.IsValidName())
            {
                EmpObjFactory.LastName = TxtLastName.Text;
            }            

            if (TxtMiddleInitial.Text.IsValidInitial())
            {
                EmpObjFactory.MiddleInitial = TxtMiddleInitial.Text;
            }

            if (CBxMaritalStatus.SelectedIndex >= 0)
            {
                if (CBxMaritalStatus.Items[CBxMaritalStatus.SelectedIndex].ToString().ToLower() == 
                    ConstantsClass.MarritalStatus.MARRIED.ToString().ToLower())
                {
                    EmpObjFactory.IsMarried = ConstantsClass.MarritalStatus.MARRIED;
                }
                else
                {
                    EmpObjFactory.IsMarried = ConstantsClass.MarritalStatus.SINGLE;
                }
            }

            if (TxtDepartment.Text.IsValidDepartmentName())
            {
                EmpObjFactory.Department = TxtDepartment.Text;
            }

            if (TxtTitle.Text.Length > 0)
            {
                EmpObjFactory.Title = TxtTitle.Text;
            }

            if (DTpStartDate.Value.Date > ConstantsClass.ONE_WEEK_AGO)
            {
                EmpObjFactory.StartDate = DTpStartDate.Value.Date;
            }
            else
            {
                MessageBox.Show(ConstantsClass.INVALID_START_DATE_MSG);
            }
            
            if (TxtMonthlySalary.Text.ContainsOnlyDigit())
            {
                Decimal.TryParse(TxtMonthlySalary.Text, out tempDecimal);
                EmpObjFactory.MonthlySalary = tempDecimal;
                tempDecimal = 0;
            }
            
            if (TxtHourlyRate.Text.ContainsOnlyDigit())
            {
                Decimal.TryParse(TxtHourlyRate.Text, out tempDecimal);
                EmpObjFactory.HourlyRate = tempDecimal;
                tempDecimal = 0;
            }

            if (TxtContractWage.Text.ContainsOnlyDigit())
            {
                Decimal.TryParse(TxtContractWage.Text, out tempDecimal);
                EmpObjFactory.ContractWage = tempDecimal;
                tempDecimal = 0;
            }

            if (TxtAgency.Text.Length > 0)
            {
                EmpObjFactory.Agency = TxtAgency.Text;
            }

            if (ChBxFullTime.Checked)
            {
                EmpObjFactory.EmployeeStatus = ConstantsClass.EmpStatus.FULL_TIME;
            }
            else if (ChBxPartTime.Checked)
            {
                EmpObjFactory.EmployeeStatus = ConstantsClass.EmpStatus.PART_TIME;
            }
            
        }//end SendFrmDataToFactory(...)


        /// <summary>
        /// Calls the FileIO class to read employee records from file
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event info</param>
        private void BtnLoadFromFile_Click(object sender, EventArgs e)
        {
            fileObj.OpenDB();

        }//end BtnLoadFromFile_Click(...)


        /// <summary>
        /// Calls the FileIO class to write employee records to file
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event info</param>
        private void BtnSaveToFile_Click(object sender, EventArgs e)
        {
            fileObj.CloseDB();

        }//end BtnWriteToFile_Click(...)


        /// <summary>
        /// Resets the background colors for all the text boxes in this form to the default color
        /// </summary>
        private void ResetTxtBackgroundColors()
        {
            TxtEmpID.BackColor = ConstantsClass.defaultBackColor;
            TxtFirstName.BackColor = ConstantsClass.defaultBackColor;
            TxtLastName.BackColor = ConstantsClass.defaultBackColor;
            TxtMiddleInitial.BackColor = ConstantsClass.defaultBackColor;
            CBxMaritalStatus.BackColor = ConstantsClass.defaultBackColor;
            TxtDepartment.BackColor = ConstantsClass.defaultBackColor;
            TxtTitle.BackColor = ConstantsClass.defaultBackColor;
            TxtMonthlySalary.BackColor = ConstantsClass.defaultBackColor;
            TxtHourlyRate.BackColor = ConstantsClass.defaultBackColor;
            LblStartDate.Text = String.Empty;
            TxtAgency.BackColor = ConstantsClass.defaultBackColor;
            TxtContractWage.BackColor = ConstantsClass.defaultBackColor;
            ChBxFullTime.BackColor = ConstantsClass.defaultBackColor;
            ChBxPartTime.BackColor = ConstantsClass.defaultBackColor;

        }//end ResetBackgrounds()


        /// <summary>
        /// Resets the background colors of all text boxes and combo boxes in the add course tab
        /// </summary>
        private void ResetCourseTxtBackColors()
        {
            TxtAddCrsEmpId.BackColor = ConstantsClass.defaultBackColor;
            TxtCourseID.BackColor = ConstantsClass.defaultBackColor;
            TxtCourseDesc.BackColor = ConstantsClass.defaultBackColor;
            CBxCourseGrade.BackColor = ConstantsClass.defaultBackColor;
            CbxCourseApproved.BackColor = ConstantsClass.defaultBackColor;
            CBxCredits.BackColor = ConstantsClass.defaultBackColor;

        }//end ResetCourseTxtBackg

        /// <summary>
        /// Clears any text contained in the Basic Info and Additional Info group boxes in the form.
        /// </summary>
        private void ClearFrm()
        {
            TxtEmpID.Text = String.Empty;
            TxtFirstName.Text = String.Empty;
            TxtLastName.Text = String.Empty;
            TxtMonthlySalary.Text = String.Empty;
            TxtHourlyRate.Text = String.Empty;
            TxtContractWage.Text = String.Empty;
            TxtMiddleInitial.Text = String.Empty;
            CBxMaritalStatus.SelectedIndex = 0;
            TxtDepartment.Text = String.Empty;
            TxtTitle.Text = String.Empty;
            DTpStartDate.Value = DateTime.Today;
            TxtAgency.Text = String.Empty;
            ChBxFullTime.Checked = false;
            ChBxPartTime.Checked = false;

        }//end ClearFrm()


        /// <summary>
        /// Makes call to show the form that allows client to lookup an employee
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void BtnLookupEmployee_Click(object sender, EventArgs e)
        {
            int pixlesFromTop = 75;
            FrmLookup lookupForm = new FrmLookup();

            lookupForm.Show();
            lookupForm.Top = pixlesFromTop;

        }//end BtnLookupEmployee_Click(...)


        /// <summary>
        /// Adds Course object to dictionary in Employee class
        /// <remarks>Calls function to validate inputs prior to creating course object</remarks>
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void BtnAddCourse_Click(object sender, EventArgs e)
        {
            uint tempEmpID = 0;
            String tempCourseID = "";
            String tempDesc = "";
            String tempGrade = "";
            DateTime? tempAppDate = null;
            uint tempCredits = 0;
            String approved = "YES";
            String notSelectedIdx = "-1";
            bool missingData = false;
            String invalidIdMsg = "Invalid ID";
            const String courseAddedMsg = "Successfully added course: ";
            String appendIfContractEmpMsg = String.Empty;

            ResetCourseTxtBackColors();

            //Validate input
            if (TxtAddCrsEmpId.Text.ContainsOnlyDigit()) { uint.TryParse(TxtAddCrsEmpId.Text, out tempEmpID); }

            if (TxtCourseID.Text.ValidateCourseID()) { tempCourseID = TxtCourseID.Text; }

            tempDesc = TxtCourseDesc.Text;

            if (CBxCourseGrade.SelectedIndex >= 0) { tempGrade = CBxCourseGrade.Items[CBxCourseGrade.SelectedIndex].ToString(); }

            if (CbxCourseApproved.SelectedIndex >= 0)
            {
                if (CbxCourseApproved.Items[CbxCourseApproved.SelectedIndex].ToString() == approved)
                {
                    tempAppDate = DTpAppDate.Value.Date;
                }
            }

            if (CBxCredits.SelectedIndex.ToString() != notSelectedIdx) { uint.TryParse(CBxCredits.Items[CBxCredits.SelectedIndex].ToString(), out tempCredits); }

            //Indicate missing or invalid data
            if (tempEmpID == 0)
            {
                TxtAddCrsEmpId.Text = invalidIdMsg;
                TxtAddCrsEmpId.BackColor = ConstantsClass.missingDataBackgroundColor;
                missingData = true;
            }

            if (tempCourseID == "") { TxtCourseID.BackColor = ConstantsClass.missingDataBackgroundColor; missingData = true; }

            if (tempDesc == "") { TxtCourseDesc.BackColor = ConstantsClass.missingDataBackgroundColor; missingData = true; }

            if (tempGrade == notSelectedIdx) { CBxCourseGrade.BackColor = ConstantsClass.missingDataBackgroundColor; missingData = true; }

            if (tempCredits == 0) { CBxCredits.BackColor = ConstantsClass.missingDataBackgroundColor; missingData = true; }

            if (tempAppDate > DateTime.Today.Date) { MessageBox.Show(ConstantsClass.BAD_COURSE_APPROVAL_DATE_MSG); missingData = true; }

            //Exit if missing data, or add course to dictionary
            if (missingData)
            {
                return;
            }
            else
            {
                Employee tempEmp = businessRules.empDictionary[tempEmpID];

                if (tempEmp != null)
                {
                    if (tempEmp.EmpType == EType.CONTRACT)
                    {
                        MessageBox.Show(ConstantsClass.CONT_EMP_EDU_MSG);
                    }
                    
                    tempEmp.employeeCourses[tempCourseID.ToUpper()] = new Course(tempCourseID.ToUpper(), tempDesc, tempGrade, tempAppDate, tempCredits);

                    LblCrsAddSuccessFailMsg.ForeColor = ConstantsClass.successColor;
                    LblCrsAddSuccessFailMsg.Text = courseAddedMsg + tempCourseID;
                }
                else
                {
                    TxtAddCrsEmpId.BackColor = ConstantsClass.missingDataBackgroundColor;
                }                
            }

        }//end BtnAddCourse_Click(...)
        

        /// <summary>
        /// Finds and displays all courses for a given employee
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLookupCourses_Click(object sender, EventArgs e)
        {
            String invalidIdMsg = "Invalid ID";
            String credits = "Credits: ";
            String grade = "Grade: ";
            String approvalDate = "Approval Date: ";
            String[] approvedDateArr;
            String approvedDate = "Not Approved";
            const int dateIdx = 0;
            uint empId = 0;
            Course course;

            LBxLookupCourses.Items.Clear();

            if (TxtLookupCoursesEmpId.Text == String.Empty)
            {
                TxtLookupCoursesEmpId.Text = invalidIdMsg;
                return;
            }
            
            if (uint.TryParse(TxtLookupCoursesEmpId.Text, out empId))
            {
                if (businessRules.empDictionary.Contains(empId))
                {
                    foreach (KeyValuePair<string, Course> entry in businessRules.empDictionary.GetValue(empId).employeeCourses)
                    {
                        course = entry.Value;

                        if (course.approvedDate != null)
                        {
                            approvedDateArr = course.approvedDate.Value.ToString().Split();
                            approvedDate = approvedDateArr[dateIdx];
                        }                        

                        LBxLookupCourses.Items.Add(course.courseID + ": " + course.courseDesc + " ("
                            + credits + course.courseCredits + ",  " + grade + course.courseGrade + ",  " +
                            approvalDate + approvedDate + ")");
                    }
                }
                else
                {
                    TxtLookupCoursesEmpId.Text = invalidIdMsg;
                    return;
                }
            }
            else
            {
                LBxLookupCourses.Items.Add(ConstantsClass.NO_COURSES_MSG);
            }

        }//end BtnLookupCourses_Click(...)


        /// <summary>
        /// Ensures only the Full Time checkbox is checked
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void ChBxFullTime_CheckedChanged(object sender, EventArgs e)
        {
            ChBxPartTime.Checked = false;
            
        }//end ChBxFullTime_CheckedChanged(...)


        /// <summary>
        /// Ensures only the Part Time checkbox is checked
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void ChBxPartTime_CheckedChanged(object sender, EventArgs e)
        {
            ChBxFullTime.Checked = false;

        }//end ChBxPartTime_CheckedChanged(...)

    }//end FrmAddEmp class

}//end Lab_03 namespace
