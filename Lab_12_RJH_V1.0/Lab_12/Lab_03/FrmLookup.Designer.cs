﻿namespace Lab_03
{
    partial class FrmLookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtLkupContractWage = new System.Windows.Forms.TextBox();
            this.TxtLkupHourlyRate = new System.Windows.Forms.TextBox();
            this.TxtLkupMonthlySalary = new System.Windows.Forms.TextBox();
            this.TxtLkupLastName = new System.Windows.Forms.TextBox();
            this.TxtLkupFirstName = new System.Windows.Forms.TextBox();
            this.TxtLkupEmpId = new System.Windows.Forms.TextBox();
            this.LblLkupEmpType = new System.Windows.Forms.Label();
            this.LblLkupContractWage = new System.Windows.Forms.Label();
            this.LblLkupHourlyRate = new System.Windows.Forms.Label();
            this.LblLkupMonthlySalary = new System.Windows.Forms.Label();
            this.LblLkupLastName = new System.Windows.Forms.Label();
            this.LblLkupFirstName = new System.Windows.Forms.Label();
            this.LblLkupEmpId = new System.Windows.Forms.Label();
            this.LblDblClkInstruct = new System.Windows.Forms.Label();
            this.LbxMatchingRecords = new System.Windows.Forms.ListBox();
            this.GBxMatchingRecords = new System.Windows.Forms.GroupBox();
            this.BtnFindByLastName = new System.Windows.Forms.Button();
            this.BtnFindByID = new System.Windows.Forms.Button();
            this.TxtFindByLastName = new System.Windows.Forms.TextBox();
            this.TxtFindByID = new System.Windows.Forms.TextBox();
            this.LblFindByLastName = new System.Windows.Forms.Label();
            this.LblFindById = new System.Windows.Forms.Label();
            this.GBxEmpInfo = new System.Windows.Forms.GroupBox();
            this.BtnLkupDeleteEmp = new System.Windows.Forms.Button();
            this.CBxLkupEmpType = new System.Windows.Forms.ComboBox();
            this.CBxLkupMarritalStatus = new System.Windows.Forms.ComboBox();
            this.DTpLkupStartDate = new System.Windows.Forms.DateTimePicker();
            this.CBxLkupIsCurrent = new System.Windows.Forms.ComboBox();
            this.CBxLkupFullPartTime = new System.Windows.Forms.ComboBox();
            this.LblLkupFullPartTime = new System.Windows.Forms.Label();
            this.BtnLkupSaveChanges = new System.Windows.Forms.Button();
            this.LblLkupIsCurrent = new System.Windows.Forms.Label();
            this.LblLkupStartDate = new System.Windows.Forms.Label();
            this.TxtLkupTitle = new System.Windows.Forms.TextBox();
            this.LblLkupTitle = new System.Windows.Forms.Label();
            this.TxtLkupDepartment = new System.Windows.Forms.TextBox();
            this.LblLkupDepartment = new System.Windows.Forms.Label();
            this.LblLkupIsMarried = new System.Windows.Forms.Label();
            this.TxtLkupMiddleInitial = new System.Windows.Forms.TextBox();
            this.LblLkupMiddleInitial = new System.Windows.Forms.Label();
            this.GBxLookupEmp = new System.Windows.Forms.GroupBox();
            this.GBxLkupAllEmps = new System.Windows.Forms.GroupBox();
            this.BtnLkupRefresh = new System.Windows.Forms.Button();
            this.LBxLkupAllEmps = new System.Windows.Forms.ListBox();
            this.TxtLkupAgency = new System.Windows.Forms.TextBox();
            this.LblLkupAgency = new System.Windows.Forms.Label();
            this.GBxMatchingRecords.SuspendLayout();
            this.GBxEmpInfo.SuspendLayout();
            this.GBxLookupEmp.SuspendLayout();
            this.GBxLkupAllEmps.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtLkupContractWage
            // 
            this.TxtLkupContractWage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupContractWage.Location = new System.Drawing.Point(130, 427);
            this.TxtLkupContractWage.Name = "TxtLkupContractWage";
            this.TxtLkupContractWage.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupContractWage.TabIndex = 18;
            // 
            // TxtLkupHourlyRate
            // 
            this.TxtLkupHourlyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupHourlyRate.Location = new System.Drawing.Point(130, 398);
            this.TxtLkupHourlyRate.Name = "TxtLkupHourlyRate";
            this.TxtLkupHourlyRate.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupHourlyRate.TabIndex = 17;
            // 
            // TxtLkupMonthlySalary
            // 
            this.TxtLkupMonthlySalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupMonthlySalary.Location = new System.Drawing.Point(130, 369);
            this.TxtLkupMonthlySalary.Name = "TxtLkupMonthlySalary";
            this.TxtLkupMonthlySalary.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupMonthlySalary.TabIndex = 16;
            // 
            // TxtLkupLastName
            // 
            this.TxtLkupLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupLastName.Location = new System.Drawing.Point(130, 115);
            this.TxtLkupLastName.Name = "TxtLkupLastName";
            this.TxtLkupLastName.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupLastName.TabIndex = 8;
            // 
            // TxtLkupFirstName
            // 
            this.TxtLkupFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupFirstName.Location = new System.Drawing.Point(130, 86);
            this.TxtLkupFirstName.Name = "TxtLkupFirstName";
            this.TxtLkupFirstName.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupFirstName.TabIndex = 7;
            // 
            // TxtLkupEmpId
            // 
            this.TxtLkupEmpId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupEmpId.Location = new System.Drawing.Point(130, 57);
            this.TxtLkupEmpId.Name = "TxtLkupEmpId";
            this.TxtLkupEmpId.ReadOnly = true;
            this.TxtLkupEmpId.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupEmpId.TabIndex = 6;
            // 
            // LblLkupEmpType
            // 
            this.LblLkupEmpType.AutoSize = true;
            this.LblLkupEmpType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupEmpType.Location = new System.Drawing.Point(6, 32);
            this.LblLkupEmpType.Name = "LblLkupEmpType";
            this.LblLkupEmpType.Size = new System.Drawing.Size(105, 16);
            this.LblLkupEmpType.TabIndex = 0;
            this.LblLkupEmpType.Text = "Employee Type";
            // 
            // LblLkupContractWage
            // 
            this.LblLkupContractWage.AutoSize = true;
            this.LblLkupContractWage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupContractWage.Location = new System.Drawing.Point(6, 430);
            this.LblLkupContractWage.Name = "LblLkupContractWage";
            this.LblLkupContractWage.Size = new System.Drawing.Size(97, 16);
            this.LblLkupContractWage.TabIndex = 0;
            this.LblLkupContractWage.Text = "Contract Wage";
            // 
            // LblLkupHourlyRate
            // 
            this.LblLkupHourlyRate.AutoSize = true;
            this.LblLkupHourlyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupHourlyRate.Location = new System.Drawing.Point(6, 401);
            this.LblLkupHourlyRate.Name = "LblLkupHourlyRate";
            this.LblLkupHourlyRate.Size = new System.Drawing.Size(79, 16);
            this.LblLkupHourlyRate.TabIndex = 0;
            this.LblLkupHourlyRate.Text = "Hourly Rate";
            // 
            // LblLkupMonthlySalary
            // 
            this.LblLkupMonthlySalary.AutoSize = true;
            this.LblLkupMonthlySalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupMonthlySalary.Location = new System.Drawing.Point(6, 372);
            this.LblLkupMonthlySalary.Name = "LblLkupMonthlySalary";
            this.LblLkupMonthlySalary.Size = new System.Drawing.Size(96, 16);
            this.LblLkupMonthlySalary.TabIndex = 0;
            this.LblLkupMonthlySalary.Text = "Monthly Salary";
            // 
            // LblLkupLastName
            // 
            this.LblLkupLastName.AutoSize = true;
            this.LblLkupLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupLastName.Location = new System.Drawing.Point(6, 118);
            this.LblLkupLastName.Name = "LblLkupLastName";
            this.LblLkupLastName.Size = new System.Drawing.Size(73, 16);
            this.LblLkupLastName.TabIndex = 0;
            this.LblLkupLastName.Text = "Last Name";
            // 
            // LblLkupFirstName
            // 
            this.LblLkupFirstName.AutoSize = true;
            this.LblLkupFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupFirstName.Location = new System.Drawing.Point(6, 89);
            this.LblLkupFirstName.Name = "LblLkupFirstName";
            this.LblLkupFirstName.Size = new System.Drawing.Size(73, 16);
            this.LblLkupFirstName.TabIndex = 0;
            this.LblLkupFirstName.Text = "First Name";
            // 
            // LblLkupEmpId
            // 
            this.LblLkupEmpId.AutoSize = true;
            this.LblLkupEmpId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupEmpId.Location = new System.Drawing.Point(6, 60);
            this.LblLkupEmpId.Name = "LblLkupEmpId";
            this.LblLkupEmpId.Size = new System.Drawing.Size(86, 16);
            this.LblLkupEmpId.TabIndex = 0;
            this.LblLkupEmpId.Text = "Employee ID";
            // 
            // LblDblClkInstruct
            // 
            this.LblDblClkInstruct.AutoSize = true;
            this.LblDblClkInstruct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDblClkInstruct.Location = new System.Drawing.Point(3, 348);
            this.LblDblClkInstruct.Name = "LblDblClkInstruct";
            this.LblDblClkInstruct.Size = new System.Drawing.Size(240, 13);
            this.LblDblClkInstruct.TabIndex = 1;
            this.LblDblClkInstruct.Text = "Double-click record to view Employee Information";
            // 
            // LbxMatchingRecords
            // 
            this.LbxMatchingRecords.FormattingEnabled = true;
            this.LbxMatchingRecords.ItemHeight = 16;
            this.LbxMatchingRecords.Location = new System.Drawing.Point(9, 21);
            this.LbxMatchingRecords.Name = "LbxMatchingRecords";
            this.LbxMatchingRecords.Size = new System.Drawing.Size(364, 324);
            this.LbxMatchingRecords.TabIndex = 0;
            this.LbxMatchingRecords.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LbxMatchingRecords_MouseDoubleClick);
            // 
            // GBxMatchingRecords
            // 
            this.GBxMatchingRecords.Controls.Add(this.LblDblClkInstruct);
            this.GBxMatchingRecords.Controls.Add(this.LbxMatchingRecords);
            this.GBxMatchingRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxMatchingRecords.Location = new System.Drawing.Point(343, 111);
            this.GBxMatchingRecords.Name = "GBxMatchingRecords";
            this.GBxMatchingRecords.Size = new System.Drawing.Size(383, 401);
            this.GBxMatchingRecords.TabIndex = 5;
            this.GBxMatchingRecords.TabStop = false;
            this.GBxMatchingRecords.Text = "Matching Records";
            // 
            // BtnFindByLastName
            // 
            this.BtnFindByLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFindByLastName.Location = new System.Drawing.Point(249, 54);
            this.BtnFindByLastName.Name = "BtnFindByLastName";
            this.BtnFindByLastName.Size = new System.Drawing.Size(124, 23);
            this.BtnFindByLastName.TabIndex = 4;
            this.BtnFindByLastName.Text = "Find by Last Name";
            this.BtnFindByLastName.UseVisualStyleBackColor = true;
            this.BtnFindByLastName.Click += new System.EventHandler(this.BtnFindByLastName_Click);
            // 
            // BtnFindByID
            // 
            this.BtnFindByID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFindByID.Location = new System.Drawing.Point(249, 22);
            this.BtnFindByID.Name = "BtnFindByID";
            this.BtnFindByID.Size = new System.Drawing.Size(124, 23);
            this.BtnFindByID.TabIndex = 2;
            this.BtnFindByID.Text = "Find by ID";
            this.BtnFindByID.UseVisualStyleBackColor = true;
            this.BtnFindByID.Click += new System.EventHandler(this.BtnFindByID_Click);
            // 
            // TxtFindByLastName
            // 
            this.TxtFindByLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFindByLastName.Location = new System.Drawing.Point(98, 54);
            this.TxtFindByLastName.Name = "TxtFindByLastName";
            this.TxtFindByLastName.Size = new System.Drawing.Size(145, 22);
            this.TxtFindByLastName.TabIndex = 3;
            // 
            // TxtFindByID
            // 
            this.TxtFindByID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFindByID.Location = new System.Drawing.Point(98, 22);
            this.TxtFindByID.Name = "TxtFindByID";
            this.TxtFindByID.Size = new System.Drawing.Size(145, 22);
            this.TxtFindByID.TabIndex = 1;
            // 
            // LblFindByLastName
            // 
            this.LblFindByLastName.AutoSize = true;
            this.LblFindByLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFindByLastName.Location = new System.Drawing.Point(6, 57);
            this.LblFindByLastName.Name = "LblFindByLastName";
            this.LblFindByLastName.Size = new System.Drawing.Size(73, 16);
            this.LblFindByLastName.TabIndex = 1;
            this.LblFindByLastName.Text = "Last Name";
            // 
            // LblFindById
            // 
            this.LblFindById.AutoSize = true;
            this.LblFindById.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFindById.Location = new System.Drawing.Point(6, 25);
            this.LblFindById.Name = "LblFindById";
            this.LblFindById.Size = new System.Drawing.Size(86, 16);
            this.LblFindById.TabIndex = 0;
            this.LblFindById.Text = "Employee ID";
            // 
            // GBxEmpInfo
            // 
            this.GBxEmpInfo.Controls.Add(this.TxtLkupAgency);
            this.GBxEmpInfo.Controls.Add(this.LblLkupAgency);
            this.GBxEmpInfo.Controls.Add(this.BtnLkupDeleteEmp);
            this.GBxEmpInfo.Controls.Add(this.CBxLkupEmpType);
            this.GBxEmpInfo.Controls.Add(this.CBxLkupMarritalStatus);
            this.GBxEmpInfo.Controls.Add(this.DTpLkupStartDate);
            this.GBxEmpInfo.Controls.Add(this.CBxLkupIsCurrent);
            this.GBxEmpInfo.Controls.Add(this.CBxLkupFullPartTime);
            this.GBxEmpInfo.Controls.Add(this.LblLkupFullPartTime);
            this.GBxEmpInfo.Controls.Add(this.BtnLkupSaveChanges);
            this.GBxEmpInfo.Controls.Add(this.LblLkupIsCurrent);
            this.GBxEmpInfo.Controls.Add(this.LblLkupStartDate);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupTitle);
            this.GBxEmpInfo.Controls.Add(this.LblLkupTitle);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupDepartment);
            this.GBxEmpInfo.Controls.Add(this.LblLkupDepartment);
            this.GBxEmpInfo.Controls.Add(this.LblLkupIsMarried);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupMiddleInitial);
            this.GBxEmpInfo.Controls.Add(this.LblLkupMiddleInitial);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupContractWage);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupHourlyRate);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupMonthlySalary);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupLastName);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupFirstName);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupEmpId);
            this.GBxEmpInfo.Controls.Add(this.LblLkupEmpType);
            this.GBxEmpInfo.Controls.Add(this.LblLkupContractWage);
            this.GBxEmpInfo.Controls.Add(this.LblLkupHourlyRate);
            this.GBxEmpInfo.Controls.Add(this.LblLkupMonthlySalary);
            this.GBxEmpInfo.Controls.Add(this.LblLkupLastName);
            this.GBxEmpInfo.Controls.Add(this.LblLkupFirstName);
            this.GBxEmpInfo.Controls.Add(this.LblLkupEmpId);
            this.GBxEmpInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEmpInfo.Location = new System.Drawing.Point(734, 14);
            this.GBxEmpInfo.Name = "GBxEmpInfo";
            this.GBxEmpInfo.Size = new System.Drawing.Size(360, 498);
            this.GBxEmpInfo.TabIndex = 4;
            this.GBxEmpInfo.TabStop = false;
            this.GBxEmpInfo.Text = "Employee Information";
            // 
            // BtnLkupDeleteEmp
            // 
            this.BtnLkupDeleteEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLkupDeleteEmp.ForeColor = System.Drawing.Color.DarkRed;
            this.BtnLkupDeleteEmp.Location = new System.Drawing.Point(241, 469);
            this.BtnLkupDeleteEmp.Name = "BtnLkupDeleteEmp";
            this.BtnLkupDeleteEmp.Size = new System.Drawing.Size(105, 23);
            this.BtnLkupDeleteEmp.TabIndex = 0;
            this.BtnLkupDeleteEmp.Text = "Delete";
            this.BtnLkupDeleteEmp.UseVisualStyleBackColor = true;
            this.BtnLkupDeleteEmp.Click += new System.EventHandler(this.BtnLkupDeleteEmp_Click);
            // 
            // CBxLkupEmpType
            // 
            this.CBxLkupEmpType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBxLkupEmpType.FormattingEnabled = true;
            this.CBxLkupEmpType.Items.AddRange(new object[] {
            "Salary",
            "Sales",
            "Hourly",
            "Contract"});
            this.CBxLkupEmpType.Location = new System.Drawing.Point(130, 29);
            this.CBxLkupEmpType.Name = "CBxLkupEmpType";
            this.CBxLkupEmpType.Size = new System.Drawing.Size(216, 24);
            this.CBxLkupEmpType.TabIndex = 5;
            // 
            // CBxLkupMarritalStatus
            // 
            this.CBxLkupMarritalStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBxLkupMarritalStatus.FormattingEnabled = true;
            this.CBxLkupMarritalStatus.Items.AddRange(new object[] {
            "Single",
            "Married"});
            this.CBxLkupMarritalStatus.Location = new System.Drawing.Point(130, 171);
            this.CBxLkupMarritalStatus.Name = "CBxLkupMarritalStatus";
            this.CBxLkupMarritalStatus.Size = new System.Drawing.Size(216, 24);
            this.CBxLkupMarritalStatus.TabIndex = 10;
            // 
            // DTpLkupStartDate
            // 
            this.DTpLkupStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTpLkupStartDate.Location = new System.Drawing.Point(130, 255);
            this.DTpLkupStartDate.Name = "DTpLkupStartDate";
            this.DTpLkupStartDate.Size = new System.Drawing.Size(216, 20);
            this.DTpLkupStartDate.TabIndex = 13;
            // 
            // CBxLkupIsCurrent
            // 
            this.CBxLkupIsCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBxLkupIsCurrent.FormattingEnabled = true;
            this.CBxLkupIsCurrent.Items.AddRange(new object[] {
            "TRUE",
            "FALSE"});
            this.CBxLkupIsCurrent.Location = new System.Drawing.Point(130, 281);
            this.CBxLkupIsCurrent.Name = "CBxLkupIsCurrent";
            this.CBxLkupIsCurrent.Size = new System.Drawing.Size(121, 24);
            this.CBxLkupIsCurrent.TabIndex = 14;
            // 
            // CBxLkupFullPartTime
            // 
            this.CBxLkupFullPartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBxLkupFullPartTime.FormattingEnabled = true;
            this.CBxLkupFullPartTime.Items.AddRange(new object[] {
            "Full Time",
            "Part Time"});
            this.CBxLkupFullPartTime.Location = new System.Drawing.Point(130, 311);
            this.CBxLkupFullPartTime.Name = "CBxLkupFullPartTime";
            this.CBxLkupFullPartTime.Size = new System.Drawing.Size(121, 24);
            this.CBxLkupFullPartTime.TabIndex = 15;
            // 
            // LblLkupFullPartTime
            // 
            this.LblLkupFullPartTime.AutoSize = true;
            this.LblLkupFullPartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupFullPartTime.Location = new System.Drawing.Point(6, 314);
            this.LblLkupFullPartTime.Name = "LblLkupFullPartTime";
            this.LblLkupFullPartTime.Size = new System.Drawing.Size(110, 16);
            this.LblLkupFullPartTime.TabIndex = 0;
            this.LblLkupFullPartTime.Text = "Employee Status";
            // 
            // BtnLkupSaveChanges
            // 
            this.BtnLkupSaveChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLkupSaveChanges.ForeColor = System.Drawing.Color.Green;
            this.BtnLkupSaveChanges.Location = new System.Drawing.Point(9, 469);
            this.BtnLkupSaveChanges.Name = "BtnLkupSaveChanges";
            this.BtnLkupSaveChanges.Size = new System.Drawing.Size(209, 23);
            this.BtnLkupSaveChanges.TabIndex = 0;
            this.BtnLkupSaveChanges.Text = "Save Changes";
            this.BtnLkupSaveChanges.UseVisualStyleBackColor = true;
            this.BtnLkupSaveChanges.Click += new System.EventHandler(this.BtnLkupSaveChanges_Click);
            // 
            // LblLkupIsCurrent
            // 
            this.LblLkupIsCurrent.AutoSize = true;
            this.LblLkupIsCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupIsCurrent.Location = new System.Drawing.Point(6, 286);
            this.LblLkupIsCurrent.Name = "LblLkupIsCurrent";
            this.LblLkupIsCurrent.Size = new System.Drawing.Size(115, 16);
            this.LblLkupIsCurrent.TabIndex = 0;
            this.LblLkupIsCurrent.Text = "Current Employee";
            // 
            // LblLkupStartDate
            // 
            this.LblLkupStartDate.AutoSize = true;
            this.LblLkupStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupStartDate.Location = new System.Drawing.Point(6, 258);
            this.LblLkupStartDate.Name = "LblLkupStartDate";
            this.LblLkupStartDate.Size = new System.Drawing.Size(67, 16);
            this.LblLkupStartDate.TabIndex = 0;
            this.LblLkupStartDate.Text = "Start Date";
            // 
            // TxtLkupTitle
            // 
            this.TxtLkupTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupTitle.Location = new System.Drawing.Point(130, 227);
            this.TxtLkupTitle.Name = "TxtLkupTitle";
            this.TxtLkupTitle.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupTitle.TabIndex = 12;
            // 
            // LblLkupTitle
            // 
            this.LblLkupTitle.AutoSize = true;
            this.LblLkupTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupTitle.Location = new System.Drawing.Point(6, 230);
            this.LblLkupTitle.Name = "LblLkupTitle";
            this.LblLkupTitle.Size = new System.Drawing.Size(34, 16);
            this.LblLkupTitle.TabIndex = 0;
            this.LblLkupTitle.Text = "Title";
            // 
            // TxtLkupDepartment
            // 
            this.TxtLkupDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupDepartment.Location = new System.Drawing.Point(130, 199);
            this.TxtLkupDepartment.Name = "TxtLkupDepartment";
            this.TxtLkupDepartment.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupDepartment.TabIndex = 11;
            // 
            // LblLkupDepartment
            // 
            this.LblLkupDepartment.AutoSize = true;
            this.LblLkupDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupDepartment.Location = new System.Drawing.Point(6, 202);
            this.LblLkupDepartment.Name = "LblLkupDepartment";
            this.LblLkupDepartment.Size = new System.Drawing.Size(78, 16);
            this.LblLkupDepartment.TabIndex = 0;
            this.LblLkupDepartment.Text = "Department";
            // 
            // LblLkupIsMarried
            // 
            this.LblLkupIsMarried.AutoSize = true;
            this.LblLkupIsMarried.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupIsMarried.Location = new System.Drawing.Point(6, 174);
            this.LblLkupIsMarried.Name = "LblLkupIsMarried";
            this.LblLkupIsMarried.Size = new System.Drawing.Size(92, 16);
            this.LblLkupIsMarried.TabIndex = 0;
            this.LblLkupIsMarried.Text = "Marrital Status";
            // 
            // TxtLkupMiddleInitial
            // 
            this.TxtLkupMiddleInitial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupMiddleInitial.Location = new System.Drawing.Point(130, 143);
            this.TxtLkupMiddleInitial.Name = "TxtLkupMiddleInitial";
            this.TxtLkupMiddleInitial.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupMiddleInitial.TabIndex = 9;
            // 
            // LblLkupMiddleInitial
            // 
            this.LblLkupMiddleInitial.AutoSize = true;
            this.LblLkupMiddleInitial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupMiddleInitial.Location = new System.Drawing.Point(6, 146);
            this.LblLkupMiddleInitial.Name = "LblLkupMiddleInitial";
            this.LblLkupMiddleInitial.Size = new System.Drawing.Size(82, 16);
            this.LblLkupMiddleInitial.TabIndex = 0;
            this.LblLkupMiddleInitial.Text = "Middle Initial";
            // 
            // GBxLookupEmp
            // 
            this.GBxLookupEmp.Controls.Add(this.BtnFindByLastName);
            this.GBxLookupEmp.Controls.Add(this.BtnFindByID);
            this.GBxLookupEmp.Controls.Add(this.TxtFindByLastName);
            this.GBxLookupEmp.Controls.Add(this.TxtFindByID);
            this.GBxLookupEmp.Controls.Add(this.LblFindByLastName);
            this.GBxLookupEmp.Controls.Add(this.LblFindById);
            this.GBxLookupEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxLookupEmp.Location = new System.Drawing.Point(343, 7);
            this.GBxLookupEmp.Name = "GBxLookupEmp";
            this.GBxLookupEmp.Size = new System.Drawing.Size(383, 98);
            this.GBxLookupEmp.TabIndex = 3;
            this.GBxLookupEmp.TabStop = false;
            this.GBxLookupEmp.Text = "Lookup Employee";
            // 
            // GBxLkupAllEmps
            // 
            this.GBxLkupAllEmps.Controls.Add(this.BtnLkupRefresh);
            this.GBxLkupAllEmps.Controls.Add(this.LBxLkupAllEmps);
            this.GBxLkupAllEmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxLkupAllEmps.Location = new System.Drawing.Point(12, 12);
            this.GBxLkupAllEmps.Name = "GBxLkupAllEmps";
            this.GBxLkupAllEmps.Size = new System.Drawing.Size(323, 500);
            this.GBxLkupAllEmps.TabIndex = 6;
            this.GBxLkupAllEmps.TabStop = false;
            this.GBxLkupAllEmps.Text = "All Employees";
            // 
            // BtnLkupRefresh
            // 
            this.BtnLkupRefresh.Location = new System.Drawing.Point(6, 443);
            this.BtnLkupRefresh.Name = "BtnLkupRefresh";
            this.BtnLkupRefresh.Size = new System.Drawing.Size(85, 23);
            this.BtnLkupRefresh.TabIndex = 1;
            this.BtnLkupRefresh.Text = "Refresh";
            this.BtnLkupRefresh.UseVisualStyleBackColor = true;
            this.BtnLkupRefresh.Click += new System.EventHandler(this.BtnLkupRefresh_Click);
            // 
            // LBxLkupAllEmps
            // 
            this.LBxLkupAllEmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBxLkupAllEmps.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.LBxLkupAllEmps.FormattingEnabled = true;
            this.LBxLkupAllEmps.Location = new System.Drawing.Point(6, 17);
            this.LBxLkupAllEmps.Name = "LBxLkupAllEmps";
            this.LBxLkupAllEmps.Size = new System.Drawing.Size(311, 420);
            this.LBxLkupAllEmps.TabIndex = 0;
            // 
            // TxtLkupAgency
            // 
            this.TxtLkupAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLkupAgency.Location = new System.Drawing.Point(130, 341);
            this.TxtLkupAgency.Name = "TxtLkupAgency";
            this.TxtLkupAgency.Size = new System.Drawing.Size(216, 22);
            this.TxtLkupAgency.TabIndex = 21;
            // 
            // LblLkupAgency
            // 
            this.LblLkupAgency.AutoSize = true;
            this.LblLkupAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupAgency.Location = new System.Drawing.Point(6, 344);
            this.LblLkupAgency.Name = "LblLkupAgency";
            this.LblLkupAgency.Size = new System.Drawing.Size(54, 16);
            this.LblLkupAgency.TabIndex = 20;
            this.LblLkupAgency.Text = "Agency";
            // 
            // FrmLookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1106, 524);
            this.Controls.Add(this.GBxLkupAllEmps);
            this.Controls.Add(this.GBxMatchingRecords);
            this.Controls.Add(this.GBxEmpInfo);
            this.Controls.Add(this.GBxLookupEmp);
            this.Name = "FrmLookup";
            this.Text = "FrmLookup";
            this.Load += new System.EventHandler(this.FrmLookup_Load);
            this.GBxMatchingRecords.ResumeLayout(false);
            this.GBxMatchingRecords.PerformLayout();
            this.GBxEmpInfo.ResumeLayout(false);
            this.GBxEmpInfo.PerformLayout();
            this.GBxLookupEmp.ResumeLayout(false);
            this.GBxLookupEmp.PerformLayout();
            this.GBxLkupAllEmps.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TxtLkupContractWage;
        private System.Windows.Forms.TextBox TxtLkupHourlyRate;
        private System.Windows.Forms.TextBox TxtLkupMonthlySalary;
        private System.Windows.Forms.TextBox TxtLkupLastName;
        private System.Windows.Forms.TextBox TxtLkupFirstName;
        private System.Windows.Forms.TextBox TxtLkupEmpId;
        private System.Windows.Forms.Label LblLkupEmpType;
        private System.Windows.Forms.Label LblLkupContractWage;
        private System.Windows.Forms.Label LblLkupHourlyRate;
        private System.Windows.Forms.Label LblLkupMonthlySalary;
        private System.Windows.Forms.Label LblLkupLastName;
        private System.Windows.Forms.Label LblLkupFirstName;
        private System.Windows.Forms.Label LblLkupEmpId;
        private System.Windows.Forms.Label LblDblClkInstruct;
        private System.Windows.Forms.ListBox LbxMatchingRecords;
        private System.Windows.Forms.GroupBox GBxMatchingRecords;
        private System.Windows.Forms.Button BtnFindByLastName;
        private System.Windows.Forms.Button BtnFindByID;
        private System.Windows.Forms.TextBox TxtFindByLastName;
        private System.Windows.Forms.TextBox TxtFindByID;
        private System.Windows.Forms.Label LblFindByLastName;
        private System.Windows.Forms.Label LblFindById;
        private System.Windows.Forms.GroupBox GBxEmpInfo;
        private System.Windows.Forms.GroupBox GBxLookupEmp;
        private System.Windows.Forms.Label LblLkupStartDate;
        private System.Windows.Forms.TextBox TxtLkupTitle;
        private System.Windows.Forms.Label LblLkupTitle;
        private System.Windows.Forms.TextBox TxtLkupDepartment;
        private System.Windows.Forms.Label LblLkupDepartment;
        private System.Windows.Forms.Label LblLkupIsMarried;
        private System.Windows.Forms.TextBox TxtLkupMiddleInitial;
        private System.Windows.Forms.Label LblLkupMiddleInitial;
        private System.Windows.Forms.Label LblLkupIsCurrent;
        private System.Windows.Forms.GroupBox GBxLkupAllEmps;
        private System.Windows.Forms.ListBox LBxLkupAllEmps;
        private System.Windows.Forms.Button BtnLkupSaveChanges;
        private System.Windows.Forms.Button BtnLkupRefresh;
        private System.Windows.Forms.Label LblLkupFullPartTime;
        private System.Windows.Forms.ComboBox CBxLkupFullPartTime;
        private System.Windows.Forms.DateTimePicker DTpLkupStartDate;
        private System.Windows.Forms.ComboBox CBxLkupIsCurrent;
        private System.Windows.Forms.ComboBox CBxLkupEmpType;
        private System.Windows.Forms.ComboBox CBxLkupMarritalStatus;
        private System.Windows.Forms.Button BtnLkupDeleteEmp;
        private System.Windows.Forms.TextBox TxtLkupAgency;
        private System.Windows.Forms.Label LblLkupAgency;
    }
}