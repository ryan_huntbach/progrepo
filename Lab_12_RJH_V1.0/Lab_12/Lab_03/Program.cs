﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Windows.Forms;

namespace Lab_03
{
    /// <summary>
    /// The class that contains the main method.  Handles high level control flow for the program.
    /// </summary>
    static class Program
    {
        public static FrmEmp addEmp;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            addEmp = new FrmEmp();
            Application.Run(addEmp);
        }//end Main()

    }//end Program class

}//end Lab_03 namespace
