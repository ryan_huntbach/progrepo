﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/28/2016 15:02:02 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;

namespace Lab_03
{
    /// <summary>
    /// Provides indexable storage for employee object references.
    /// <remarks>This class is a Singleton.</remarks>
    /// </summary>
    public sealed class BusinessRules
    {
        //Members
        public SortedDictionary empDictionary = SortedDictionary.GetInstance();

        private static volatile BusinessRules instance = null;
        private static Object syncInstanceLock = new object();
        

        /// <summary>
        /// Constructor.  BusinessRules is a Singleton, so the constructor is inaccessable from outside. 
        /// </summary>
        private BusinessRules(){}

        
        /// <summary>
        /// Retrieves the instance of BusinessRules.  
        /// <remarks>
        /// - This method implements the Singleton pattern.  
        /// - Method is thread safe. 
        /// </remarks>
        /// </summary>
        /// <returns>If no instance of BusinessRules has been created, a new instance is returned, else the 
        /// pre-existing single instance of BusinessRules is returned. </returns>
        public static BusinessRules GetInstance()
        {
            if (instance == null)
            {
                lock (syncInstanceLock)
                {
                    if (instance == null)
                    {
                        instance = new BusinessRules();
                    }
                }
            }

            return instance;

        }//end GetInstance()

    }//end BusinessRules class

}//end Lab_03 namespace
