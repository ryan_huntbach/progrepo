﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections.Generic;

namespace Lab_03
{
    /// <summary>
    /// This class provides functionality for running unit tests on FrmAddEmp, and FrmAddEmp's 
    /// related classes, methods, members, etc...
    /// </summary>
    public class TestData
    {
        //Constant members
        private const string TEST_DATA_FILE = "testData.txt";

        //Members 
        public string RadioBtnTypeSelection { get; set; }
        public string EmpID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MonthlySalary { get; set; }
        public string HourlyRate { get; set; }
        public string ContractWage { get; set; }
        public string TestInfo { get; set; }
        private static Stack<TestData> testDataObjStack = new Stack<TestData>();

        /// <summary>
        /// The TestData class constructor
        /// </summary>
        /// <param name="radBtnTypeSelection">any string</param>
        /// <param name="empID">any string</param>
        /// <param name="firstName">any string</param>
        /// <param name="lastName">any string</param>
        /// <param name="monthlySalary">any string</param>
        /// <param name="hourlyRate">any string</param>
        /// <param name="contractWage">any string</param>
        /// <param name="testInfo">any string</param>
        public TestData (string radBtnTypeSelection, string empID, string firstName, string lastName, 
            string monthlySalary, string hourlyRate, string contractWage, string testInfo)
        {
            this.RadioBtnTypeSelection = radBtnTypeSelection;
            this.EmpID = empID;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.MonthlySalary = monthlySalary;
            this.HourlyRate = hourlyRate;
            this.ContractWage = contractWage;
            this.TestInfo = testInfo;
        }//end TestData(...)

        /// <summary>
        /// Reads data from a file, and uses the data to create TestData objects using the TestData constructor.
        /// TestData objects are pushed onto a stack (stored at the TestData class level) as they are created.
        /// <remarks>File must be located in the current working directory, and file formatting 
        /// must be [data;data;...data;].  The order of the data elements must exactly match the 
        /// TestData constructor.</remarks>
        /// </summary>
        /// <returns>true, if all TestData objects were read from the file, else returns false.</returns>
        public static bool LoadTestData()
        {
            const char DELIMITER = ';';
            const int NUM_PARMS_REQ = 8;
            int RBtnPos = 0, empIdPos = 1, firstNamePos = 2, lastNamePos = 3, salaryPos = 4, hourlyPos = 5, contractPos = 6, testInfoPos = 7;
            string[] lines = null;
            string[] parms = null;

            try
            {
                lines = System.IO.File.ReadAllLines(TEST_DATA_FILE);
            }
            catch (Exception e)
            {
                return false;//file read error
            }

            if (lines.Length == 0) { return false; }//nothing read from file
            
            foreach (string individualLine in lines)
            {
                parms = individualLine.Split(DELIMITER);

                if (parms.Length < NUM_PARMS_REQ)
                {
                    return false;//line didn't contain valid info.  file may be corrupted.
                }
                else
                {
                    testDataObjStack.Push(new TestData(parms[RBtnPos], parms[empIdPos], parms[firstNamePos], 
                        parms[lastNamePos], parms[salaryPos], parms[hourlyPos], parms[contractPos], parms[testInfoPos]));
                }
            }

            return true;
        }//end LoadTestData()

        /// <summary>
        /// Pops TestData objects off of the testDataObjStack stack.
        /// </summary>
        /// <returns>The TestData item from the top of testDataObjStack, or null if the stack is empty</returns>
        public static TestData GetNextTestDataObj()
        {
            TestData tempTestDataObj = null;

            try
            {
                tempTestDataObj = testDataObjStack.Pop();
            }
            catch (Exception e)
            {
                return null;
            }

            return tempTestDataObj;
        }//end GetNextTestDataObj()

    }//end TestData class

}//end Lab_03 namespace