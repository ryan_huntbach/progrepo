﻿namespace Lab_03
{
    partial class FrmLookupEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtLkupHourlyRate = new System.Windows.Forms.TextBox();
            this.TxtLkupLastName = new System.Windows.Forms.TextBox();
            this.LblLkupMonthlySalary = new System.Windows.Forms.Label();
            this.LblLkupLastName = new System.Windows.Forms.Label();
            this.LblLkupFirstName = new System.Windows.Forms.Label();
            this.GBxEmpInfo = new System.Windows.Forms.GroupBox();
            this.TxtLkupContractWage = new System.Windows.Forms.TextBox();
            this.TxtLkupMonthlySalary = new System.Windows.Forms.TextBox();
            this.TxtLkupFirstName = new System.Windows.Forms.TextBox();
            this.TxtLkupEmpId = new System.Windows.Forms.TextBox();
            this.TxtLkupEmpType = new System.Windows.Forms.TextBox();
            this.LblLkupEmpType = new System.Windows.Forms.Label();
            this.LblLkupContractWage = new System.Windows.Forms.Label();
            this.LblLkupHourlyRate = new System.Windows.Forms.Label();
            this.LblLkupEmpId = new System.Windows.Forms.Label();
            this.GBxMatchingRecords = new System.Windows.Forms.GroupBox();
            this.LblDblClkInstruct = new System.Windows.Forms.Label();
            this.LbxMatchingRecords = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GBxEmpInfo.SuspendLayout();
            this.GBxMatchingRecords.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtLkupHourlyRate
            // 
            this.TxtLkupHourlyRate.Location = new System.Drawing.Point(130, 193);
            this.TxtLkupHourlyRate.Name = "TxtLkupHourlyRate";
            this.TxtLkupHourlyRate.Size = new System.Drawing.Size(267, 22);
            this.TxtLkupHourlyRate.TabIndex = 12;
            // 
            // TxtLkupLastName
            // 
            this.TxtLkupLastName.Location = new System.Drawing.Point(130, 127);
            this.TxtLkupLastName.Name = "TxtLkupLastName";
            this.TxtLkupLastName.Size = new System.Drawing.Size(267, 22);
            this.TxtLkupLastName.TabIndex = 10;
            // 
            // LblLkupMonthlySalary
            // 
            this.LblLkupMonthlySalary.AutoSize = true;
            this.LblLkupMonthlySalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupMonthlySalary.Location = new System.Drawing.Point(6, 163);
            this.LblLkupMonthlySalary.Name = "LblLkupMonthlySalary";
            this.LblLkupMonthlySalary.Size = new System.Drawing.Size(96, 16);
            this.LblLkupMonthlySalary.TabIndex = 3;
            this.LblLkupMonthlySalary.Text = "Monthly Salary";
            // 
            // LblLkupLastName
            // 
            this.LblLkupLastName.AutoSize = true;
            this.LblLkupLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupLastName.Location = new System.Drawing.Point(6, 130);
            this.LblLkupLastName.Name = "LblLkupLastName";
            this.LblLkupLastName.Size = new System.Drawing.Size(73, 16);
            this.LblLkupLastName.TabIndex = 2;
            this.LblLkupLastName.Text = "Last Name";
            // 
            // LblLkupFirstName
            // 
            this.LblLkupFirstName.AutoSize = true;
            this.LblLkupFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupFirstName.Location = new System.Drawing.Point(6, 97);
            this.LblLkupFirstName.Name = "LblLkupFirstName";
            this.LblLkupFirstName.Size = new System.Drawing.Size(73, 16);
            this.LblLkupFirstName.TabIndex = 1;
            this.LblLkupFirstName.Text = "First Name";
            // 
            // GBxEmpInfo
            // 
            this.GBxEmpInfo.Controls.Add(this.TxtLkupContractWage);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupHourlyRate);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupMonthlySalary);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupLastName);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupFirstName);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupEmpId);
            this.GBxEmpInfo.Controls.Add(this.TxtLkupEmpType);
            this.GBxEmpInfo.Controls.Add(this.LblLkupEmpType);
            this.GBxEmpInfo.Controls.Add(this.LblLkupContractWage);
            this.GBxEmpInfo.Controls.Add(this.LblLkupHourlyRate);
            this.GBxEmpInfo.Controls.Add(this.LblLkupMonthlySalary);
            this.GBxEmpInfo.Controls.Add(this.LblLkupLastName);
            this.GBxEmpInfo.Controls.Add(this.LblLkupFirstName);
            this.GBxEmpInfo.Controls.Add(this.LblLkupEmpId);
            this.GBxEmpInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEmpInfo.Location = new System.Drawing.Point(14, 319);
            this.GBxEmpInfo.Name = "GBxEmpInfo";
            this.GBxEmpInfo.Size = new System.Drawing.Size(403, 266);
            this.GBxEmpInfo.TabIndex = 4;
            this.GBxEmpInfo.TabStop = false;
            this.GBxEmpInfo.Text = "Employee Information";
            // 
            // TxtLkupContractWage
            // 
            this.TxtLkupContractWage.Location = new System.Drawing.Point(130, 226);
            this.TxtLkupContractWage.Name = "TxtLkupContractWage";
            this.TxtLkupContractWage.Size = new System.Drawing.Size(267, 22);
            this.TxtLkupContractWage.TabIndex = 13;
            // 
            // TxtLkupMonthlySalary
            // 
            this.TxtLkupMonthlySalary.Location = new System.Drawing.Point(130, 160);
            this.TxtLkupMonthlySalary.Name = "TxtLkupMonthlySalary";
            this.TxtLkupMonthlySalary.Size = new System.Drawing.Size(267, 22);
            this.TxtLkupMonthlySalary.TabIndex = 11;
            // 
            // TxtLkupFirstName
            // 
            this.TxtLkupFirstName.Location = new System.Drawing.Point(130, 94);
            this.TxtLkupFirstName.Name = "TxtLkupFirstName";
            this.TxtLkupFirstName.Size = new System.Drawing.Size(267, 22);
            this.TxtLkupFirstName.TabIndex = 9;
            // 
            // TxtLkupEmpId
            // 
            this.TxtLkupEmpId.Location = new System.Drawing.Point(130, 61);
            this.TxtLkupEmpId.Name = "TxtLkupEmpId";
            this.TxtLkupEmpId.Size = new System.Drawing.Size(267, 22);
            this.TxtLkupEmpId.TabIndex = 8;
            // 
            // TxtLkupEmpType
            // 
            this.TxtLkupEmpType.Location = new System.Drawing.Point(130, 29);
            this.TxtLkupEmpType.Name = "TxtLkupEmpType";
            this.TxtLkupEmpType.Size = new System.Drawing.Size(267, 22);
            this.TxtLkupEmpType.TabIndex = 7;
            // 
            // LblLkupEmpType
            // 
            this.LblLkupEmpType.AutoSize = true;
            this.LblLkupEmpType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupEmpType.Location = new System.Drawing.Point(6, 32);
            this.LblLkupEmpType.Name = "LblLkupEmpType";
            this.LblLkupEmpType.Size = new System.Drawing.Size(105, 16);
            this.LblLkupEmpType.TabIndex = 6;
            this.LblLkupEmpType.Text = "Employee Type";
            // 
            // LblLkupContractWage
            // 
            this.LblLkupContractWage.AutoSize = true;
            this.LblLkupContractWage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupContractWage.Location = new System.Drawing.Point(6, 229);
            this.LblLkupContractWage.Name = "LblLkupContractWage";
            this.LblLkupContractWage.Size = new System.Drawing.Size(97, 16);
            this.LblLkupContractWage.TabIndex = 5;
            this.LblLkupContractWage.Text = "Contract Wage";
            // 
            // LblLkupHourlyRate
            // 
            this.LblLkupHourlyRate.AutoSize = true;
            this.LblLkupHourlyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupHourlyRate.Location = new System.Drawing.Point(6, 196);
            this.LblLkupHourlyRate.Name = "LblLkupHourlyRate";
            this.LblLkupHourlyRate.Size = new System.Drawing.Size(79, 16);
            this.LblLkupHourlyRate.TabIndex = 4;
            this.LblLkupHourlyRate.Text = "Hourly Rate";
            // 
            // LblLkupEmpId
            // 
            this.LblLkupEmpId.AutoSize = true;
            this.LblLkupEmpId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLkupEmpId.Location = new System.Drawing.Point(6, 64);
            this.LblLkupEmpId.Name = "LblLkupEmpId";
            this.LblLkupEmpId.Size = new System.Drawing.Size(86, 16);
            this.LblLkupEmpId.TabIndex = 0;
            this.LblLkupEmpId.Text = "Employee ID";
            // 
            // GBxMatchingRecords
            // 
            this.GBxMatchingRecords.Controls.Add(this.LblDblClkInstruct);
            this.GBxMatchingRecords.Controls.Add(this.LbxMatchingRecords);
            this.GBxMatchingRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxMatchingRecords.Location = new System.Drawing.Point(14, 116);
            this.GBxMatchingRecords.Name = "GBxMatchingRecords";
            this.GBxMatchingRecords.Size = new System.Drawing.Size(403, 197);
            this.GBxMatchingRecords.TabIndex = 5;
            this.GBxMatchingRecords.TabStop = false;
            this.GBxMatchingRecords.Text = "Matching Records";
            // 
            // LblDblClkInstruct
            // 
            this.LblDblClkInstruct.AutoSize = true;
            this.LblDblClkInstruct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDblClkInstruct.Location = new System.Drawing.Point(3, 170);
            this.LblDblClkInstruct.Name = "LblDblClkInstruct";
            this.LblDblClkInstruct.Size = new System.Drawing.Size(240, 13);
            this.LblDblClkInstruct.TabIndex = 1;
            this.LblDblClkInstruct.Text = "Double-click record to view Employee Information";
            // 
            // LbxMatchingRecords
            // 
            this.LbxMatchingRecords.FormattingEnabled = true;
            this.LbxMatchingRecords.ItemHeight = 16;
            this.LbxMatchingRecords.Location = new System.Drawing.Point(6, 21);
            this.LbxMatchingRecords.Name = "LbxMatchingRecords";
            this.LbxMatchingRecords.Size = new System.Drawing.Size(391, 148);
            this.LbxMatchingRecords.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Employee ID";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(98, 54);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(169, 22);
            this.textBox1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(273, 21);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Find by ID";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(14, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 98);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lookup Employee";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(273, 53);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Find by Last Name";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(98, 22);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(169, 22);
            this.textBox2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Last Name";
            // 
            // FrmLookupEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 602);
            this.Controls.Add(this.GBxEmpInfo);
            this.Controls.Add(this.GBxMatchingRecords);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmLookupEmp";
            this.Text = "Lookup Employee";
            this.GBxEmpInfo.ResumeLayout(false);
            this.GBxEmpInfo.PerformLayout();
            this.GBxMatchingRecords.ResumeLayout(false);
            this.GBxMatchingRecords.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox LbxMatchingRecords;
        private System.Windows.Forms.Label LblDblClkInstruct;
        private System.Windows.Forms.GroupBox GBxMatchingRecords;
        private System.Windows.Forms.Label LblLkupEmpId;
        private System.Windows.Forms.Label LblLkupHourlyRate;
        private System.Windows.Forms.Label LblLkupContractWage;
        private System.Windows.Forms.Label LblLkupEmpType;
        private System.Windows.Forms.TextBox TxtLkupEmpType;
        private System.Windows.Forms.TextBox TxtLkupEmpId;
        private System.Windows.Forms.TextBox TxtLkupFirstName;
        private System.Windows.Forms.TextBox TxtLkupMonthlySalary;
        private System.Windows.Forms.TextBox TxtLkupContractWage;
        private System.Windows.Forms.GroupBox GBxEmpInfo;
        private System.Windows.Forms.Label LblLkupFirstName;
        private System.Windows.Forms.Label LblLkupLastName;
        private System.Windows.Forms.Label LblLkupMonthlySalary;
        private System.Windows.Forms.TextBox TxtLkupLastName;
        private System.Windows.Forms.TextBox TxtLkupHourlyRate;
    }
}