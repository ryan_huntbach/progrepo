﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 2/04/2016 14:09:10 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------



using System;
using System.Windows.Forms;

namespace Lab_03
{
    /// <summary>
    /// This class is for a form that displays test information
    /// </summary>
    public partial class FrmTestOutput : Form
    {
        //Members
        private Tests tests;

        /// <summary>
        /// The FrmTestOutput initializer
        /// </summary>
        public FrmTestOutput()
        {
            InitializeComponent();

        }//end FrmTestOutput()

        
        /// <summary>
        /// Appends text to the textbox
        /// </summary>
        /// <param name="toAppend">any valid string.</param>
        public void TxtTestData_Append(string toAppend)
        {
            TxtTestData.AppendText(toAppend);

        }//end TxtTestData_Append(...)

        /// <summary>
        /// Disables the next button
        /// </summary>
        public void BtnNext_Disable()
        {
            BtnNext.Enabled = false;

        }//end BtnNext_Disable()


        /// <summary>
        /// The next button--runs next test in the Tests class
        /// </summary>
        /// <param name="sender">the sending object</param>
        /// <param name="e">event information</param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            tests.RunNextTest();

        }// end BtnNext_Blick(...)


        /// <summary>
        /// Sets the Tests object member
        /// </summary>
        /// <param name="testParm">a valid and initialized Tests object</param>
        public void SetTestsObj(Tests testParm)
        {
            tests = testParm;

        }//end SetTestsObj(...)

    }//end FrmTestOutput class

}//end Lab_03 namespace
