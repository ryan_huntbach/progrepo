﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------



namespace Lab_03
{
    /// <summary>
    /// This struct models the data required for an employee
    /// </summary>
    public enum EmpDataNames { ID, FIRST_NAME, LAST_NAME, MONTHLY_SALARY, COMMISSION, GROSS_SALES, HOURLY_RATE,
        HOURS_WORKED, CONTRACT_WAGE, MIDDLE_INITIAL, IS_MARRIED, DEPARTMENT, TITLE, START_DATE, AGENCY, EMPLOYEE_STATUS };

}//end Lab_03 namespace
