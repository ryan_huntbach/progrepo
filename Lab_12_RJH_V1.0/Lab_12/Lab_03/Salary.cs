﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_05
// Date: 1/22/2016 19:45:39 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lab_03
{
    /// <summary>
    /// This is a derived class (from Employee) for storing salary employee data
    /// </summary>
    [Serializable()]
    public class Salary : Employee, ISerializable
    {
        //Member constants
        protected const bool OVERTIME = false;
        protected const bool BENEFITS = true;
        protected const bool COMMISSION = false;
        protected const bool EDU_BENEFITS = true;

        //Member properties
        public decimal MonthlySalary { get; set; }

        /// <summary>
        /// The Salary class constructor.  Uses base class constructor for some parameters. 
        /// </summary>
        /// <param name="id">any valid uint</param>
        /// <param name="firstName">and valid string </param>
        /// <param name="lastName">any valid string </param>
        /// <param name="middleInitial">any valid string </param>
        /// <param name="empType">An EType (enum) indicating the employee's compensation status (Salary, Sales, Hourly, Contract) </param>
        /// <param name="isMarried">any valid MarritalStatus (enum) type </param>
        /// <param name="department">any valid string </param>
        /// <param name="title">any valid string </param>
        /// <param name="startDate">any valid DateTime </param>
        /// <param name="employeeStatus">any valid EmployeeStatus (enum) type </param>
        /// <param name="isCurrentEmp">any valid bool (optional) </param>
        /// <param name="courses">any valid SortedDictionary(string, Course) (optional) </param>
        public Salary(uint id, string firstName, string lastName, string middleInitial, EType empType, 
            ConstantsClass.MarritalStatus isMarried, string department, string title, 
            ConstantsClass.EmpStatus employeeStatus, DateTime? startDate, decimal monthlySalary, 
            bool isCurrentEmp = true, SortedDictionary<string, Course> courses = null)
            : base (id, firstName, lastName, middleInitial, empType, isMarried, department, title, employeeStatus, startDate, isCurrentEmp, courses)
        {
            this.MonthlySalary = monthlySalary;

        }//end Salary(...)


        /// <summary>
        /// Maps ISerializable BinaryFormatter deserialization to Employee class object
        /// </summary>
        /// <param name="info">info about serialization.</param>
        /// <param name="context">context of the serialization.</param>
        public Salary(SerializationInfo info, StreamingContext context) 
            : base(info.GetUInt32(ConstantsClass.EMP_ID_STR), info.GetString(ConstantsClass.FIRST_NAME_STR),info.GetString(ConstantsClass.LAST_NAME_STR),
                   info.GetString(ConstantsClass.MIDDLE_INITIAL_STR), (EType)info.GetValue(ConstantsClass.EMP_TYPE_STR, typeof(EType)),
                   (ConstantsClass.MarritalStatus)info.GetValue(ConstantsClass.IS_MARRIED_STR, typeof(ConstantsClass.MarritalStatus)), 
                   info.GetString(ConstantsClass.DEPARTMENT_STR), info.GetString(ConstantsClass.TITLE_STR),
                   (ConstantsClass.EmpStatus)info.GetValue(ConstantsClass.EMPLOYEE_STATUS_STR, typeof(ConstantsClass.EmpStatus)), 
                   info.GetDateTime(ConstantsClass.START_DATE_STR), info.GetBoolean(ConstantsClass.IS_CURRENT_EMP_STR),
                   (SortedDictionary<string, Course>)info.GetValue(ConstantsClass.EMPLOYEE_COURSES_STR, typeof(SortedDictionary<string, Course>)))

        {
            MonthlySalary = info.GetDecimal(ConstantsClass.MONTHLY_SALARY_STR);

        }//end Salary(...)


        /// <summary>
        /// Provides the data ISerializable needs for BinaryFormatter serialization
        /// </summary>
        /// <param name="info">info about serialization</param>
        /// <param name="context">context of the serialization to be performed</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(ConstantsClass.EMP_ID_STR, EmpID);
            info.AddValue(ConstantsClass.FIRST_NAME_STR, FirstName);
            info.AddValue(ConstantsClass.LAST_NAME_STR, LastName);
            info.AddValue(ConstantsClass.MIDDLE_INITIAL_STR, MiddleInitial);
            info.AddValue(ConstantsClass.EMP_TYPE_STR, EmpType);
            info.AddValue(ConstantsClass.IS_MARRIED_STR, IsMarried);
            info.AddValue(ConstantsClass.DEPARTMENT_STR, Department);
            info.AddValue(ConstantsClass.TITLE_STR, Title);
            info.AddValue(ConstantsClass.EMPLOYEE_STATUS_STR, EmployeeStatus);
            info.AddValue(ConstantsClass.START_DATE_STR, StartDate);
            info.AddValue(ConstantsClass.MONTHLY_SALARY_STR, MonthlySalary);
            info.AddValue(ConstantsClass.IS_CURRENT_EMP_STR, IsCurrentEmp);
            info.AddValue(ConstantsClass.EMPLOYEE_COURSES_STR, employeeCourses);

        }//end GetObjectData(...)

    }//end Salary class

}//end Lab_03 namespace
