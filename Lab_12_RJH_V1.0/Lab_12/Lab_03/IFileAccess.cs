﻿// Project Prolog
// Name: Ryan Huntbach
// CS3260 Section 001
// Project: Lab_07
// Date: 2/18/2016 12:41:59 PM
// Purpose: This is an employee management software system.  Currently, the system
// provides functionality to add and store a new employee, and a unit test harness.
// 
// I declare that the following code was written by me or provided 
// by the instructor for this project. I understand that copying source
// code from any other source constitutes cheating, and that I will receive
// a zero on this project if I am found in violation of this policy.
// ---------------------------------------------------------------------------




namespace Lab_03
{
    /// <summary>
    /// Interface for serialized file IO operations
    /// </summary>
    interface IFileAccess
    {
        //Member properties
        SortedDictionary DB { get; set; }

        /// <summary>
        /// Serializes employee objects contained in BusinessRules storage and writes it to client specified file
        /// <remarks>Clears and overwrites file, if it existed previously.</remarks>
        /// </summary>
        void WriteDB();

        /// <summary>
        /// Reads file, deserializes it, and converts to employee objects;
        /// </summary>
        void ReadDB();

        /// <summary>
        /// Opens file and initializes readStream, if file can be read.
        /// </summary>
        void OpenDB();

        /// <summary>
        /// Opens and/or creates file to write DB to, and writes information contained in DB to the file
        /// </summary>
        void CloseDB();

    }//end IFileAccess

}//end Lab_03 Namespace
