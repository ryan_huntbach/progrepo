﻿namespace Lab_03
{
    partial class FrmEditEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GBxEditEmpType = new System.Windows.Forms.GroupBox();
            this.GBxEditBasicInfo = new System.Windows.Forms.GroupBox();
            this.GBxEditCompensation = new System.Windows.Forms.GroupBox();
            this.GBxEditFullPartTime = new System.Windows.Forms.GroupBox();
            this.BtnEditSaveChanges = new System.Windows.Forms.Button();
            this.BtnEditCancel = new System.Windows.Forms.Button();
            this.RBtnEditSalary = new System.Windows.Forms.RadioButton();
            this.RBtnEditSales = new System.Windows.Forms.RadioButton();
            this.RBtnEditHourly = new System.Windows.Forms.RadioButton();
            this.RBtnEditContract = new System.Windows.Forms.RadioButton();
            this.LblEditEmpID = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.LblEditFirstName = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.LblEditLastName = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.LblEditMiddleInitial = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.LblEditMaritalStatus = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.LblEditDepartment = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.LblEditTitle = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.LblEditStartDate = new System.Windows.Forms.Label();
            this.GBxEditEmpType.SuspendLayout();
            this.GBxEditBasicInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBxEditEmpType
            // 
            this.GBxEditEmpType.Controls.Add(this.RBtnEditContract);
            this.GBxEditEmpType.Controls.Add(this.RBtnEditHourly);
            this.GBxEditEmpType.Controls.Add(this.RBtnEditSales);
            this.GBxEditEmpType.Controls.Add(this.RBtnEditSalary);
            this.GBxEditEmpType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEditEmpType.Location = new System.Drawing.Point(12, 12);
            this.GBxEditEmpType.Name = "GBxEditEmpType";
            this.GBxEditEmpType.Size = new System.Drawing.Size(373, 132);
            this.GBxEditEmpType.TabIndex = 0;
            this.GBxEditEmpType.TabStop = false;
            this.GBxEditEmpType.Text = "Employee Type";
            // 
            // GBxEditBasicInfo
            // 
            this.GBxEditBasicInfo.Controls.Add(this.textBox8);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditStartDate);
            this.GBxEditBasicInfo.Controls.Add(this.textBox7);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditTitle);
            this.GBxEditBasicInfo.Controls.Add(this.textBox6);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditDepartment);
            this.GBxEditBasicInfo.Controls.Add(this.textBox5);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditMaritalStatus);
            this.GBxEditBasicInfo.Controls.Add(this.textBox4);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditMiddleInitial);
            this.GBxEditBasicInfo.Controls.Add(this.textBox3);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditLastName);
            this.GBxEditBasicInfo.Controls.Add(this.textBox2);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditFirstName);
            this.GBxEditBasicInfo.Controls.Add(this.textBox1);
            this.GBxEditBasicInfo.Controls.Add(this.LblEditEmpID);
            this.GBxEditBasicInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEditBasicInfo.Location = new System.Drawing.Point(12, 150);
            this.GBxEditBasicInfo.Name = "GBxEditBasicInfo";
            this.GBxEditBasicInfo.Size = new System.Drawing.Size(373, 258);
            this.GBxEditBasicInfo.TabIndex = 1;
            this.GBxEditBasicInfo.TabStop = false;
            this.GBxEditBasicInfo.Text = "Basic Info";
            // 
            // GBxEditCompensation
            // 
            this.GBxEditCompensation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEditCompensation.Location = new System.Drawing.Point(408, 12);
            this.GBxEditCompensation.Name = "GBxEditCompensation";
            this.GBxEditCompensation.Size = new System.Drawing.Size(400, 186);
            this.GBxEditCompensation.TabIndex = 2;
            this.GBxEditCompensation.TabStop = false;
            this.GBxEditCompensation.Text = "Compensation";
            // 
            // GBxEditFullPartTime
            // 
            this.GBxEditFullPartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxEditFullPartTime.Location = new System.Drawing.Point(408, 204);
            this.GBxEditFullPartTime.Name = "GBxEditFullPartTime";
            this.GBxEditFullPartTime.Size = new System.Drawing.Size(400, 103);
            this.GBxEditFullPartTime.TabIndex = 3;
            this.GBxEditFullPartTime.TabStop = false;
            this.GBxEditFullPartTime.Text = "Full Time / Part Time";
            // 
            // BtnEditSaveChanges
            // 
            this.BtnEditSaveChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEditSaveChanges.Location = new System.Drawing.Point(408, 361);
            this.BtnEditSaveChanges.Name = "BtnEditSaveChanges";
            this.BtnEditSaveChanges.Size = new System.Drawing.Size(180, 23);
            this.BtnEditSaveChanges.TabIndex = 4;
            this.BtnEditSaveChanges.Text = "Save Changes";
            this.BtnEditSaveChanges.UseVisualStyleBackColor = true;
            // 
            // BtnEditCancel
            // 
            this.BtnEditCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEditCancel.Location = new System.Drawing.Point(633, 361);
            this.BtnEditCancel.Name = "BtnEditCancel";
            this.BtnEditCancel.Size = new System.Drawing.Size(175, 23);
            this.BtnEditCancel.TabIndex = 5;
            this.BtnEditCancel.Text = "Cancel";
            this.BtnEditCancel.UseVisualStyleBackColor = true;
            // 
            // RBtnEditSalary
            // 
            this.RBtnEditSalary.AutoSize = true;
            this.RBtnEditSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnEditSalary.Location = new System.Drawing.Point(6, 21);
            this.RBtnEditSalary.Name = "RBtnEditSalary";
            this.RBtnEditSalary.Size = new System.Drawing.Size(65, 20);
            this.RBtnEditSalary.TabIndex = 0;
            this.RBtnEditSalary.TabStop = true;
            this.RBtnEditSalary.Text = "Salary";
            this.RBtnEditSalary.UseVisualStyleBackColor = true;
            // 
            // RBtnEditSales
            // 
            this.RBtnEditSales.AutoSize = true;
            this.RBtnEditSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnEditSales.Location = new System.Drawing.Point(6, 47);
            this.RBtnEditSales.Name = "RBtnEditSales";
            this.RBtnEditSales.Size = new System.Drawing.Size(61, 20);
            this.RBtnEditSales.TabIndex = 1;
            this.RBtnEditSales.TabStop = true;
            this.RBtnEditSales.Text = "Sales";
            this.RBtnEditSales.UseVisualStyleBackColor = true;
            // 
            // RBtnEditHourly
            // 
            this.RBtnEditHourly.AutoSize = true;
            this.RBtnEditHourly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnEditHourly.Location = new System.Drawing.Point(6, 73);
            this.RBtnEditHourly.Name = "RBtnEditHourly";
            this.RBtnEditHourly.Size = new System.Drawing.Size(65, 20);
            this.RBtnEditHourly.TabIndex = 2;
            this.RBtnEditHourly.TabStop = true;
            this.RBtnEditHourly.Text = "Hourly";
            this.RBtnEditHourly.UseVisualStyleBackColor = true;
            // 
            // RBtnEditContract
            // 
            this.RBtnEditContract.AutoSize = true;
            this.RBtnEditContract.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBtnEditContract.Location = new System.Drawing.Point(6, 99);
            this.RBtnEditContract.Name = "RBtnEditContract";
            this.RBtnEditContract.Size = new System.Drawing.Size(75, 20);
            this.RBtnEditContract.TabIndex = 3;
            this.RBtnEditContract.TabStop = true;
            this.RBtnEditContract.Text = "Contract";
            this.RBtnEditContract.UseVisualStyleBackColor = true;
            // 
            // LblEditEmpID
            // 
            this.LblEditEmpID.AutoSize = true;
            this.LblEditEmpID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditEmpID.Location = new System.Drawing.Point(6, 24);
            this.LblEditEmpID.Name = "LblEditEmpID";
            this.LblEditEmpID.Size = new System.Drawing.Size(86, 16);
            this.LblEditEmpID.TabIndex = 0;
            this.LblEditEmpID.Text = "Employee ID";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(146, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(208, 22);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(146, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(208, 22);
            this.textBox2.TabIndex = 3;
            // 
            // LblEditFirstName
            // 
            this.LblEditFirstName.AutoSize = true;
            this.LblEditFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditFirstName.Location = new System.Drawing.Point(6, 52);
            this.LblEditFirstName.Name = "LblEditFirstName";
            this.LblEditFirstName.Size = new System.Drawing.Size(73, 16);
            this.LblEditFirstName.TabIndex = 2;
            this.LblEditFirstName.Text = "First Name";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(146, 77);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(208, 22);
            this.textBox3.TabIndex = 5;
            // 
            // LblEditLastName
            // 
            this.LblEditLastName.AutoSize = true;
            this.LblEditLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditLastName.Location = new System.Drawing.Point(6, 80);
            this.LblEditLastName.Name = "LblEditLastName";
            this.LblEditLastName.Size = new System.Drawing.Size(73, 16);
            this.LblEditLastName.TabIndex = 4;
            this.LblEditLastName.Text = "Last Name";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(146, 105);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(208, 22);
            this.textBox4.TabIndex = 7;
            // 
            // LblEditMiddleInitial
            // 
            this.LblEditMiddleInitial.AutoSize = true;
            this.LblEditMiddleInitial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditMiddleInitial.Location = new System.Drawing.Point(6, 108);
            this.LblEditMiddleInitial.Name = "LblEditMiddleInitial";
            this.LblEditMiddleInitial.Size = new System.Drawing.Size(82, 16);
            this.LblEditMiddleInitial.TabIndex = 6;
            this.LblEditMiddleInitial.Text = "Middle Initial";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(146, 133);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(208, 22);
            this.textBox5.TabIndex = 9;
            // 
            // LblEditMaritalStatus
            // 
            this.LblEditMaritalStatus.AutoSize = true;
            this.LblEditMaritalStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditMaritalStatus.Location = new System.Drawing.Point(6, 136);
            this.LblEditMaritalStatus.Name = "LblEditMaritalStatus";
            this.LblEditMaritalStatus.Size = new System.Drawing.Size(88, 16);
            this.LblEditMaritalStatus.TabIndex = 8;
            this.LblEditMaritalStatus.Text = "Marital Status";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(146, 161);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(208, 22);
            this.textBox6.TabIndex = 11;
            // 
            // LblEditDepartment
            // 
            this.LblEditDepartment.AutoSize = true;
            this.LblEditDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditDepartment.Location = new System.Drawing.Point(6, 164);
            this.LblEditDepartment.Name = "LblEditDepartment";
            this.LblEditDepartment.Size = new System.Drawing.Size(78, 16);
            this.LblEditDepartment.TabIndex = 10;
            this.LblEditDepartment.Text = "Department";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(146, 189);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(208, 22);
            this.textBox7.TabIndex = 13;
            // 
            // LblEditTitle
            // 
            this.LblEditTitle.AutoSize = true;
            this.LblEditTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditTitle.Location = new System.Drawing.Point(6, 192);
            this.LblEditTitle.Name = "LblEditTitle";
            this.LblEditTitle.Size = new System.Drawing.Size(34, 16);
            this.LblEditTitle.TabIndex = 12;
            this.LblEditTitle.Text = "Title";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(146, 217);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(208, 22);
            this.textBox8.TabIndex = 15;
            // 
            // LblEditStartDate
            // 
            this.LblEditStartDate.AutoSize = true;
            this.LblEditStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEditStartDate.Location = new System.Drawing.Point(6, 220);
            this.LblEditStartDate.Name = "LblEditStartDate";
            this.LblEditStartDate.Size = new System.Drawing.Size(67, 16);
            this.LblEditStartDate.TabIndex = 14;
            this.LblEditStartDate.Text = "Start Date";
            // 
            // FrmEditEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 425);
            this.Controls.Add(this.BtnEditCancel);
            this.Controls.Add(this.BtnEditSaveChanges);
            this.Controls.Add(this.GBxEditFullPartTime);
            this.Controls.Add(this.GBxEditCompensation);
            this.Controls.Add(this.GBxEditBasicInfo);
            this.Controls.Add(this.GBxEditEmpType);
            this.Name = "FrmEditEmp";
            this.Text = "Edit Employee Record";
            this.GBxEditEmpType.ResumeLayout(false);
            this.GBxEditEmpType.PerformLayout();
            this.GBxEditBasicInfo.ResumeLayout(false);
            this.GBxEditBasicInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBxEditEmpType;
        private System.Windows.Forms.GroupBox GBxEditBasicInfo;
        private System.Windows.Forms.GroupBox GBxEditCompensation;
        private System.Windows.Forms.GroupBox GBxEditFullPartTime;
        private System.Windows.Forms.Button BtnEditSaveChanges;
        private System.Windows.Forms.Button BtnEditCancel;
        private System.Windows.Forms.RadioButton RBtnEditContract;
        private System.Windows.Forms.RadioButton RBtnEditHourly;
        private System.Windows.Forms.RadioButton RBtnEditSales;
        private System.Windows.Forms.RadioButton RBtnEditSalary;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label LblEditEmpID;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label LblEditStartDate;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label LblEditTitle;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label LblEditDepartment;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label LblEditMaritalStatus;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label LblEditMiddleInitial;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label LblEditLastName;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label LblEditFirstName;
    }
}