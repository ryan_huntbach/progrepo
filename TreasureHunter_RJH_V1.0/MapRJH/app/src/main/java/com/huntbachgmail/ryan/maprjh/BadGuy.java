package com.huntbachgmail.ryan.maprjh;

import android.os.AsyncTask;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Dell on 4/8/2016.
 */
public class BadGuy {

    public int mBadGuyCol;
    public int mBadGuyRow;
    private int mNumCols;
    private int mNumRows;
    private Random mRandom;


    public BadGuy(int col, int row, int numCols, int numRows, int seed) {
        mBadGuyCol = col;
        mBadGuyRow = row;
        mNumCols = numCols;
        mNumRows = numRows;
        mRandom = new Random(seed);

    }


    public void update(int playerCol, int playerRow) {
        Random random = new Random(mRandom.nextInt());

        //add random movements
        if ((mBadGuyCol + 2 != playerCol && mBadGuyRow + 2 != playerRow) &&
            (mBadGuyCol + 2 != playerCol && mBadGuyRow - 2 != playerRow) &&
            (mBadGuyCol - 2 != playerCol && mBadGuyRow + 2 != playerRow) &&
            (mBadGuyCol - 2 != playerCol && mBadGuyRow - 2 != playerRow)) {

            if ((random.nextInt() % 7) == 0 && (mBadGuyCol - 1 >= 1)) {
                mBadGuyCol--;
            }
            if ((random.nextInt() % 7) == 0 && mBadGuyCol + 1 <= mNumCols - 2) {
                mBadGuyCol++;
            }
            if ((random.nextInt() % 7) == 0 && (mBadGuyRow - 1 >= 1)) {
                mBadGuyRow--;
            }
            if ((random.nextInt() % 7) == 0 && mBadGuyRow + 1 <= mNumRows - 2) {
                mBadGuyRow++;
            }

        }

        //move bad guy toward player
        if (playerCol == mBadGuyCol && playerRow == mBadGuyRow){
            return;
        } else if (Math.abs(playerCol - mBadGuyCol) > Math.abs(playerRow - mBadGuyRow)) {
            if (playerCol > mBadGuyCol  && mBadGuyCol + 1 <= mNumCols - 2) {
                mBadGuyCol++;
            } else if (mBadGuyCol - 1 >= 1) {
                mBadGuyCol--;
            }
        } else {
            if (playerRow > mBadGuyRow && mBadGuyRow + 1 <= mNumRows - 2) {
                mBadGuyRow++;
            } else if (mBadGuyRow - 1 >= 1) {
                mBadGuyRow--;
            }
        }
    }
}
