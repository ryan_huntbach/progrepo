package com.huntbachgmail.ryan.maprjh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    public static final String SELECTED_DIFFICULTY_LEVEL = "SELECTED_DIFFICULTY_LEVEL";
    public static final String MAX_DIFFICULTY_LEVEL = "MAX_DIFFICULTY_LEVEL";
    Button mStartButton;
    SeekBar mSeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStartButton = (Button)findViewById(R.id.BtnStartGame);
        mSeekBar = (SeekBar)findViewById(R.id.SkBarDifficultyLevel);

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMapView();
            }
        });
    }

    private void openMapView(){
        Intent i = new Intent(getApplicationContext(), MapActivity.class);
        i.putExtra(SELECTED_DIFFICULTY_LEVEL, mSeekBar.getProgress());
        i.putExtra(MAX_DIFFICULTY_LEVEL, mSeekBar.getMax());
        startActivity(i);
    }
}
