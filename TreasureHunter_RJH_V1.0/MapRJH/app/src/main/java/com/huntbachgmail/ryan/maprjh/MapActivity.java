package com.huntbachgmail.ryan.maprjh;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

/**
 * Created by Dell on 4/9/2016.
 */
public class MapActivity extends AppCompatActivity {

    private final int MAX_BAD_GUY_MAX_INTERVAL = 2000;
    private final int DIFFICULTY_LEVEL_REDUCER = 180;
    MediaPlayer mWonSoundMP;
    MediaPlayer mLostSoundMP;
    private int mSelectedDifficulty;
    private int mMoveBadGuyCalculatedInterval;
    private DisplayMetrics mDisplayMetrics;
    private int mWidth;
    private int mHeight;
    private float mAspectRatio1;
    private float mAspectRatio2;
    private MapView mMapView;
    private Handler mMoveBadGuysHandler;
    private boolean mWonOrLost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mMapView = (MapView)findViewById(R.id.mv);
        mDisplayMetrics = getResources().getDisplayMetrics();
        mWidth = mDisplayMetrics.widthPixels;
        mHeight = mDisplayMetrics.heightPixels;
        mAspectRatio1 = (float)mHeight / (float)mWidth;
        mAspectRatio2 = (float)mWidth / (float)mHeight;
        mWonSoundMP = MediaPlayer.create(this, R.raw.winsound);
        mLostSoundMP = MediaPlayer.create(this, R.raw.lostsound);
        mMoveBadGuysHandler = new Handler();
        mWonOrLost = false;
        mSelectedDifficulty = getIntent().getExtras().getInt(MainActivity.SELECTED_DIFFICULTY_LEVEL);
        mMoveBadGuyCalculatedInterval = MAX_BAD_GUY_MAX_INTERVAL - (mSelectedDifficulty * DIFFICULTY_LEVEL_REDUCER);
        mMoveBadGuys.run();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN){

            int x = (int)event.getX();
            int y = (int)event.getY();

            if (y < x * mAspectRatio1){
                if (x < mWidth - (y * mAspectRatio2)){
                    mMapView.movePlayerUp();
                } else {
                    mMapView.movePlayerRight();
                }
            } else if (y > x * mAspectRatio1) {
                if (x < mWidth - (y * mAspectRatio2)) {
                    mMapView.movePlayerLeft();
                } else {
                    mMapView.movePlayerDown();
                }
            }
        }

        mMapView.invalidate();
        checkIfWonOrLost();
        return false;
    }

    Runnable mMoveBadGuys = new Runnable(){
        @Override
        public void run(){
            if (mWonOrLost == true) {
                finish();
                return;
            }
            mMapView.notifyBadGuys();
            mMapView.invalidate();
            checkIfWonOrLost();
            mMoveBadGuysHandler.postDelayed(mMoveBadGuys, mMoveBadGuyCalculatedInterval);
        }
    };

    public void checkIfWonOrLost(){
        if (mMapView.PlayerWon()) {
            mWonOrLost = true;
            mMoveBadGuysHandler.removeCallbacks(mMoveBadGuys);
            mWonSoundMP.start();
            Intent i = new Intent(getApplicationContext(), WonActivity.class);
            startActivity(i);
        }else if (mMapView.BadGuysWon()){
            mWonOrLost = true;
            mMoveBadGuysHandler.removeCallbacks(mMoveBadGuys);
            mLostSoundMP.start();
            Intent i = new Intent(getApplicationContext(), LostActivity.class);
            startActivity(i);
        }
    }
}
