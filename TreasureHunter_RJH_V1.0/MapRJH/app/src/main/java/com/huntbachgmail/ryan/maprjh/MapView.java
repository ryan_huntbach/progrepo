package com.huntbachgmail.ryan.maprjh;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Dell on 3/30/2016.
 */
public class MapView extends View {

    private final int NUM_IMAGES_ACROSS = 5;
    private final int NUM_IMAGES_HIGH = 7;
    private final int NUM_ROWS = 15;
    private final int NUM_COLS = 14;
    private Bitmap mForest;
    private Bitmap mMountain;
    private Bitmap mOut;
    private Bitmap mPerson;
    private Bitmap mPlain;
    private Bitmap mTreasure;
    private Bitmap mWater;
    private Bitmap mBadGuy;
    DisplayMetrics mDisplayMetrics;
    private int mWidth;
    private int mHeight;
    private int mapCurRow;
    private int mapCurCol;
    private int playerCurRow;
    private int playerCurCol;
    private char[][] tileArr;
    private static ArrayList<BadGuy> mBadGuyList;
    private int mTreasureCol;
    private int mTreasureRow;
    Random random;


    public MapView(Context context) {
        super(context);
        init (context , null, 0);
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public MapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr)
    {
        mForest = BitmapFactory.decodeResource(getResources(), R.drawable.forest);
        mDisplayMetrics = getResources().getDisplayMetrics();
        mWidth = mDisplayMetrics.widthPixels / NUM_IMAGES_ACROSS;
        mHeight = mDisplayMetrics.heightPixels / NUM_IMAGES_HIGH;
        mForest = Bitmap.createScaledBitmap(mForest, mWidth, mHeight, false);

        mMountain = BitmapFactory.decodeResource(getResources(), R.drawable.mountain);
        mMountain = Bitmap.createScaledBitmap(mMountain, mWidth, mHeight, false);

        mOut = BitmapFactory.decodeResource(getResources(), R.drawable.out);
        mOut = Bitmap.createScaledBitmap(mOut, mWidth, mHeight, false);

        mPlain = BitmapFactory.decodeResource(getResources(), R.drawable.plain);
        mPlain = Bitmap.createScaledBitmap(mPlain, mWidth, mHeight, false);

        mTreasure = BitmapFactory.decodeResource(getResources(), R.drawable.treasure);
        mTreasure = Bitmap.createScaledBitmap(mTreasure, mWidth, mHeight, false);

        mWater = BitmapFactory.decodeResource(getResources(), R.drawable.water);
        mWater = Bitmap.createScaledBitmap(mWater, mWidth, mHeight, false);

        mPerson = BitmapFactory.decodeResource(getResources(), R.drawable.person);
        mPerson = Bitmap.createScaledBitmap(mPerson, mWidth, mHeight, false);

        mBadGuy = BitmapFactory.decodeResource(getResources(), R.drawable.bad_guy);
        mBadGuy = Bitmap.createScaledBitmap(mBadGuy, mWidth, mHeight, false);

        mapCurRow = 5;
        mapCurCol = 5;

        playerCurRow = 7;
        playerCurCol = 7;

        mBadGuyList = new ArrayList<BadGuy>();

        registerBadGuy(new BadGuy(1, 1, NUM_COLS, NUM_ROWS, new Random(139).nextInt()));
        registerBadGuy(new BadGuy(NUM_COLS - 2, 1, NUM_COLS, NUM_ROWS, new Random(149).nextInt()));
        registerBadGuy(new BadGuy(1, NUM_ROWS - 2, NUM_COLS, NUM_ROWS, new Random(151).nextInt()));
        registerBadGuy(new BadGuy(NUM_COLS - 2, NUM_ROWS - 2, NUM_COLS, NUM_ROWS, new Random(157).nextInt()));

        tileArr = new char[NUM_ROWS][];
        //o = out, t = treasure, m = mountain, f = forest, p = plain, w = water
        tileArr[0] = new char[] { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o' };
        tileArr[1] = new char[] { 'o', 'f', 'f', 'f', 'f', 'f', 'f', 'w', 'w', 'w', 'f', 'f', 'f', 'o' };
        tileArr[2] = new char[] { 'o', 'f', 'f', 'f', 'f', 'f', 'w', 'w', 'w', 'f', 'f', 'f', 'f', 'o' };
        tileArr[3] = new char[] { 'o', 'f', 'f', 'f', 'f', 'w', 'w', 'w', 'f', 'f', 'f', 'f', 'p', 'o' };
        tileArr[4] = new char[] { 'o', 'f', 'f', 'f', 'w', 'w', 'w', 'f', 'f', 'f', 'f', 'p', 'p', 'o' };
        tileArr[5] = new char[] { 'o', 'f', 'f', 'w', 'w', 'w', 'f', 'f', 'f', 'm', 'p', 'p', 'p', 'o' };
        tileArr[6] = new char[] { 'o', 'f', 'w', 'w', 'w', 'f', 'f', 'f', 'f', 'm', 'm', 'p', 'p', 'o' };
        tileArr[7] = new char[] { 'o', 'w', 'w', 'w', 'f', 'f', 'f', 'm', 'm', 'm', 'm', 'p', 'p', 'o' };
        tileArr[8] = new char[] { 'o', 'w', 'w', 'f', 'f', 'f', 'm', 'm', 'm', 'm', 'm', 'p', 'p', 'o' };
        tileArr[9] = new char[] { 'o', 'w', 'w', 'f', 'f', 'f', 'w', 'm', 'm', 'm', 'm', 'p', 'p', 'o' };
        tileArr[10] = new char[] { 'o', 'w', 'w', 'w', 'f', 'w', 'w', 'w', 'm', 'm', 'm', 'p', 'p', 'o' };
        tileArr[11] = new char[] { 'o', 'f', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'm', 'm', 'p', 'p', 'o' };
        tileArr[12] = new char[] { 'o', 'f', 'f', 'w', 'w', 'w', 'w', 'w', 'w', 'm', 'm', 'p', 'p', 'o' };
        tileArr[13] = new char[] { 'o', 'f', 'f', 'f', 'w', 'w', 'w', 'w', 'w', 'm', 'm', 'p', 'p', 'o' };
        tileArr[14] = new char[] { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o' };

        mTreasureCol = new Random(System.currentTimeMillis()).nextInt(NUM_COLS - 2) + 1;
        mTreasureRow = new Random(System.currentTimeMillis()).nextInt(NUM_ROWS - 2) + 1;
        tileArr[mTreasureRow][mTreasureCol] = 't';

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int rowOffset = 0;
        int colOffset = 0;

        for (int row = mapCurRow; row < mapCurRow + NUM_IMAGES_HIGH - 1; row++) {

            for (int col = mapCurCol; col < mapCurCol + NUM_IMAGES_ACROSS; col++) {

                switch (tileArr[row][col]) {

                    case ('f'):
                        canvas.drawBitmap(mForest, mWidth * colOffset++, mHeight * rowOffset, null);
                        break;
                    case ('m'):
                        canvas.drawBitmap(mMountain, mWidth * colOffset++, mHeight * rowOffset, null);
                        break;
                    case ('o'):
                        canvas.drawBitmap(mOut, mWidth * colOffset++, mHeight * rowOffset, null);
                        break;
                    case ('p'):
                        canvas.drawBitmap(mPlain, mWidth * colOffset++, mHeight * rowOffset, null);
                        break;
                    case ('t'):
                        canvas.drawBitmap(mTreasure, mWidth * colOffset++, mHeight * rowOffset, null);
                        break;
                    case ('w'):
                        canvas.drawBitmap(mWater, mWidth * colOffset++, mHeight * rowOffset, null);
                        break;
                }
            }

            rowOffset++;
            colOffset = 0;
        }

        canvas.drawBitmap(mPerson, mWidth * (playerCurCol - mapCurCol), mHeight * (playerCurRow - mapCurRow), null);

        for (BadGuy badGuy : mBadGuyList){
            canvas.drawBitmap(mBadGuy, mWidth * (badGuy.mBadGuyCol - mapCurCol), mHeight * (badGuy.mBadGuyRow - mapCurRow), null);
        }
    }

    protected void movePlayerRight()
    {
        if (tileArr[playerCurRow][playerCurCol + 1] == 'o') {
            return;
        }

        playerCurCol++;

        if (playerCurCol >= mapCurCol + NUM_IMAGES_ACROSS - 1){
            mapCurCol++;
        }
    }

    protected void movePlayerLeft()
    {
        if (tileArr[playerCurRow][playerCurCol -1] == 'o') {
            return;
        }

        playerCurCol--;

        if (playerCurCol <= mapCurCol){
            mapCurCol--;
        }
    }

    protected void movePlayerUp()
    {
        if (tileArr[playerCurRow - 1][playerCurCol] == 'o') {
            return;
        }

        playerCurRow--;

        if (playerCurRow <= mapCurRow){
            mapCurRow--;
        }
    }

    protected void movePlayerDown()
    {
        if (tileArr[playerCurRow + 1][playerCurCol] == 'o') {
            return;
        }

        playerCurRow++;

        if (playerCurRow >= mapCurRow + NUM_IMAGES_HIGH - 2){
            mapCurRow++;
        }
    }

    protected static void registerBadGuy(BadGuy badGuy){
        mBadGuyList.add(badGuy);
    }

    protected void notifyBadGuys(){
        for (BadGuy badGuy : mBadGuyList){
            badGuy.update(playerCurCol, playerCurRow);
        }
    }

    protected boolean PlayerWon(){
        return (playerCurCol == mTreasureCol && playerCurRow == mTreasureRow);
    }

    protected boolean BadGuysWon(){
        boolean badGuysWonOrNot = false;

        for (BadGuy badGuy : mBadGuyList){
            if (playerCurCol == badGuy.mBadGuyCol && playerCurRow == badGuy.mBadGuyRow){
                badGuysWonOrNot = true;
            }
        }

        return badGuysWonOrNot;
    }
}
